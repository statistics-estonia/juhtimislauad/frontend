const qs = require('qs');

const stringifyQuery = query => qs.stringify(query, { allowDots: true, sort: (a, b) => a.localeCompare(b) });
const parseQuery = query => qs.parse(query, { allowDots: true, decoder: booleanDecoder });

const booleanDecoder = (string, decoder) => {
  if (string === 'true') return true;
  else if (string === 'false') return false;

  return decoder(string);
};

const delay = timeout => new Promise(resolve => setTimeout(resolve, timeout));
const log = (...args) => console.log(new Date().toISOString(), ...args);
const logError = (...args) => console.log(new Date().toISOString(), ...args);

module.exports = {
  stringifyQuery,
  parseQuery,
  delay,
  log,
  logError
};
