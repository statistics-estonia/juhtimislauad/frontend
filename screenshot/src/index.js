const url = require('url');
const express = require('express');
const puppeteer = require('puppeteer');
const { parseQuery, stringifyQuery, delay, log, logError } = require('./utils');
const { createCacheDir, writeCache, readCache } = require('./cache');

const {
  SCREENSHOT_API_PORT = 8080,
  CACHE_MAX_AGE_IN_SECONDS = 60 * 60,
  FRONTEND_BASE_URL = 'https://arendus.juhtimislauad.stat.ee/branches/develop',
  ALLOWED_ORIGINS = 'https://arendus.juhtimislauad.stat.ee, https://demo.juhtimislauad.stat.ee'
} = process.env;

const PUPPETEER_OPTIONS = {
  headless: true,
  args: ['--disable-dev-shm-usage'],
  executablePath: process.env.PUPPETEER_SKIP_CHROMIUM_DOWNLOAD ? '/usr/bin/chromium-browser' : undefined
};

const app = express();

const start = async () => {
  log('Launching puppeteer with the following options', PUPPETEER_OPTIONS);
  const browser = await puppeteer.launch(PUPPETEER_OPTIONS);

  log('Starting Express on port', Number(SCREENSHOT_API_PORT));
  const server = app.listen(Number(SCREENSHOT_API_PORT));

  app.locals.browser = browser;

  const exitGracefully = async () => {
    log('Exiting..');
    await browser.close();
    server.close();
  };

  process.on('SIGINT', exitGracefully);
  process.on('SIGTERM', exitGracefully);
};

app.get(
  '/api/screenshot',

  (req, res, next) => {
    const { download = true, language = 'et', width = 1024, height = 768, ...preferences } = parseQuery(
      url.parse(req.url).query
    );

    res.locals.download = download;
    res.locals.preferences = { language, width, height, ...preferences };

    next();
  },

  async (req, res, next) => {
    const { preferences } = res.locals;

    try {
      const { title, data, createdAt } = await readCache(preferences);

      const ageInMilliseconds = new Date().getTime() - createdAt;

      if (ageInMilliseconds < Number(CACHE_MAX_AGE_IN_SECONDS) * 1000) {
        res.locals.title = title;
        res.locals.screenshot = data;
      }
    } catch (e) {
      if (e.code !== 'ENOENT') logError(e);
    }

    next();
  },

  async (req, res, next) => {
    if (res.locals.screenshot && res.locals.title) {
      next();
      return;
    }

    const { width, height, ...preferences } = res.locals.preferences;
    const finalPreferences = {
      disableAnimation: true,
      disableControls: true,
      ...preferences
    };

    const page = await req.app.locals.browser.newPage();

    try {
      await page.setViewport({
        width: Math.min(width, 3000),
        height: Math.min(height, 3000),
        deviceScaleFactor: 2
      });

      await Promise.race([
        waitForWidget(page, finalPreferences),
        delay(5000).then(() => Promise.reject(new Error('There was a problem screenshotting the widget')))
      ]);

      const screenshot = await page.screenshot({ encoding: 'binary' });
      const title = await page.title();

      res.locals.title = title;
      res.locals.screenshot = screenshot;

      await createCacheDir();
      await writeCache(res.locals.preferences, { title, data: screenshot });
    } catch (e) {
      if (e.message === 'There was a problem screenshotting the widget') res.status(502);
      else res.status(500);

      res.end(e.message);
    }

    await page.close();

    next();
  },

  (req, res) => {
    const { title, screenshot, download } = res.locals;

    if (download) res.header('content-disposition', `attachment; filename="${title}.png"`);
    res.header('content-type', 'image/png');
    res.end(screenshot, 'binary');
  }
);

const waitForWidget = (page, { baseUrl = FRONTEND_BASE_URL, ...preferences }) =>
  new Promise(async (resolve, reject) => {
    if (baseUrl !== FRONTEND_BASE_URL && !parsedAllowedOrigins.find(origin => baseUrl.startsWith(origin))) {
      reject(new Error(`Origin "${baseUrl}" is not allowed!`));
      return;
    }

    const url = `${baseUrl}/embed?${stringifyQuery(preferences)}`;

    log('Screenshotting', url);

    try {
      await page.exposeFunction('onWidgetReady', async () => {
        resolve();
      });

      await page.evaluateOnNewDocument(() => {
        window.addEventListener('widgetready', event => {
          window.onWidgetReady(event);
        });
      });

      await page.goto(url);
    } catch (e) {
      reject(e);
    }
  });

const parsedAllowedOrigins = ALLOWED_ORIGINS.split(',').map(item => item.trim());

start();
