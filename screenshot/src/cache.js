const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const crypto = require('crypto');
const { stringifyQuery } = require('./utils');

const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const exists = promisify(fs.exists);
const mkdir = promisify(fs.mkdir);

const { CACHE_DIR = path.join(__dirname, '..', 'cache') } = process.env;

const createCacheDir = async () => !(await exists(CACHE_DIR)) && (await mkdir(CACHE_DIR));

const writeCache = (preferences, { title, data }) => {
  const key = md5(stringifyQuery(preferences));
  return Promise.all([writeCacheMeta(key, { title, createdAt: new Date().getTime() }), writeCacheImage(key, data)]);
};
const writeCacheMeta = (key, meta) => writeFile(path.join(CACHE_DIR, `${key}.json`), JSON.stringify(meta));
const writeCacheImage = (key, data) => writeFile(path.join(CACHE_DIR, `${key}.png`), data);

const readCache = async preferences => {
  const key = md5(stringifyQuery(preferences));
  const [meta, data] = await Promise.all([readCacheMeta(key), readCacheImage(key)]);
  return { ...meta, data };
};
const readCacheMeta = async key => JSON.parse(await readFile(path.join(CACHE_DIR, `${key}.json`)));
const readCacheImage = key => readFile(path.join(CACHE_DIR, `${key}.png`));

const md5 = query =>
  crypto
    .createHash('md5')
    .update(query)
    .digest('hex');

module.exports = {
  createCacheDir,
  writeCache,
  readCache
};
