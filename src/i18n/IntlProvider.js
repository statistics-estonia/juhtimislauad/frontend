import React from 'react';
import { injectIntl } from 'react-intl';

export const IntlContext = React.createContext();

const IntlProvider = ({ intl, children }) => <IntlContext.Provider value={intl}>{children}</IntlContext.Provider>;

export default injectIntl(IntlProvider);
