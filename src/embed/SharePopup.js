import { mdiContentCopy } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { Button, Form, Heading } from 'react-bulma-components';
import Popup from 'reactjs-popup';
import { Translate, useTranslate } from '../i18n/LanguageProvider';
import { useEmbedScript, useEmbedUrl, useWidgetUrl } from './hooks';

const SharePopup = ({ children }) => {
  const url = useEmbedUrl();
  const script = useEmbedScript();
  const widgetUrl = useWidgetUrl();

  return (
    <Popup
      trigger={children}
      contentStyle={{ width: 250, padding: '1rem' }}
      position="bottom right"
      closeOnDocumentClick
    >
      <React.Fragment>
        <Heading size={4}>
          <Translate>stat.share</Translate>
        </Heading>
        <Form.Label>
          <Translate>stat.share.link</Translate>
        </Form.Label>
        <CopyButtonInput value={widgetUrl} title="stat.share.link.tooltip" />
        <Form.Label>
          <Translate>stat.share.embed</Translate>
        </Form.Label>
        <CopyButtonInput value={script} title="stat.share.embed.tooltip" />
        <Form.Label>
          <Translate>stat.share.graph</Translate>
        </Form.Label>
        <CopyButtonInput value={url} title="stat.share.graph.tooltip" />
      </React.Fragment>
    </Popup>
  );
};

const CopyButtonInput = ({ title, ...props }) => {
  const inputRef = React.useRef();
  const translate = useTranslate();

  const onClick = () => {
    inputRef.current.select();
    document.execCommand('copy');
  };

  return (
    <>
      <Form.Field kind="addons">
        <Form.Control>
          <AutoSelectingInput readOnly ref={inputRef} {...props} />
        </Form.Control>
        <Form.Control>
          <Button className="is-small copy-btn" onClick={onClick} title={translate(title)}>
            <Icon path={mdiContentCopy} size={1} />
          </Button>
        </Form.Control>
      </Form.Field>
    </>
  );
};

const AutoSelectingInput = React.forwardRef((props, ref) => (
  <Form.Input ref={ref} onFocus={event => event.target.select()} {...props} />
));

export default SharePopup;
