import React from 'react';
import DashboardProvider from '../dashboard/DashboardProvider';
import WidgetProvider from '../dashboard/WidgetProvider';
import ConnectedIntlProvider from '../i18n/ConnectedIntlProvider';
import { FixedLanguageProvider, Title } from '../i18n/LanguageProvider';
import { FixedDashboardPreferencesProvider } from '../preferences/dashboard';
import { FixedWidgetPreferencesProvider } from '../preferences/widget';
import EmbeddedWidgetDetails from './EmbeddedWidgetDetails';
import { parsePreferences } from './hooks';

const EmbeddedWidget = ({ location }) => {
  const { widgetId, dashboardId, languageCode, region = 1, preferences = {}, ...additionalProps } = parsePreferences(
    location.search
  );

  useHiddenOverflow();

  return (
    <FixedLanguageProvider languageCode={languageCode}>
      <Title />
      <ConnectedIntlProvider>
        <DashboardProvider dashboardId={dashboardId}>
          <FixedDashboardPreferencesProvider region={region}>
            <FixedWidgetPreferencesProvider widgetId={widgetId} preferences={preferences}>
              <WidgetProvider widgetId={widgetId}>
                <EmbeddedWidgetDetails {...additionalProps} />
              </WidgetProvider>
            </FixedWidgetPreferencesProvider>
          </FixedDashboardPreferencesProvider>
        </DashboardProvider>
      </ConnectedIntlProvider>
    </FixedLanguageProvider>
  );
};

const useHiddenOverflow = () => {
  React.useEffect(() => {
    document.querySelector('html').style.overflowY = 'hidden';

    return () => (document.querySelector('html').style.overflowY = '');
  });
};

export default EmbeddedWidget;
