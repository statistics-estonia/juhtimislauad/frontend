import qs from 'qs';
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { DashboardContext, useCurrentDashboard, useGetRegionSlugById } from '../dashboard/DashboardProvider';
import { useDescriptiveWidgetName, useWidget, WidgetContext } from '../dashboard/WidgetProvider';
import { useActiveLanguageCode, useTranslate } from '../i18n/LanguageProvider';
import { useRegion } from '../preferences/dashboard';
import { useChartType, useWidgetPreferences } from '../preferences/widget';
import { getSlug } from '../shared/utils';

export const useEmbedUrl = additionalPreferences => {
  const preferences = useEmbedPreferences();

  return `${window.location.origin}${process.env.PUBLIC_URL}/embed${stringifyPreferences({
    ...preferences,
    ...additionalPreferences
  })}`;
};

export const useWidgetUrl = () => {
  const { widgetId, dashboardId, languageCode, region } = useEmbedPreferences();
  if (!dashboardId) return '';
  const translate = useTranslate();
  const { name } = useCurrentDashboard();
  const regionSlug = useGetRegionSlugById(region);

  const defaultDashboardName = translate('stat.url.dashboard', { languageCode });
  const graphName = translate('stat.graph', languageCode);

  const widget = useWidget();

  let dashboardSlug = name ? `${getSlug(name)}-${dashboardId}` : `${defaultDashboardName}-${dashboardId}`;

  if (widget.dashboards && widget.dashboards[0]) {
    dashboardSlug = `${getSlug(widget.dashboards[0].name)}-${widget.dashboards[0].id}`;
  }

  const optionalRegionSlug = regionSlug ? `/${regionSlug}` : '';
  const graphSlug = `${graphName}-${widgetId}`;

  return `${window.location.origin}${process.env.PUBLIC_URL}/${languageCode}/${dashboardSlug}${optionalRegionSlug}/${graphSlug}`;
};

export const useScreenshotUrl = additionalPreferences => {
  const preferences = useEmbedPreferences();
  const [chartType] = useChartType();

  return `${process.env.REACT_APP_API_BASE_URL}/screenshot${stringifyPreferences({
    ...preferences,
    disableAnimation: true,
    disableControls: true,
    baseUrl: process.env.PUBLIC_URL ? `${window.location.origin}${process.env.PUBLIC_URL}` : null,
    width: chartType === 'map' ? 1500 : undefined,
    ...additionalPreferences
  })}`;
};

export const useEmbedPreferences = () => {
  const languageCode = useActiveLanguageCode();
  const [region] = useRegion();
  const { dashboardId } = React.useContext(DashboardContext);
  const { widgetId } = React.useContext(WidgetContext);
  const [chartType] = useChartType();
  const [{ graphs = [], ...preferences }] = useWidgetPreferences();

  return {
    region,
    dashboardId,
    widgetId,
    languageCode,
    preferences: {
      ...preferences,
      graphs: graphs.filter(type => type.graphType === chartType)
    }
  };
};

export const useEmbedScript = () => {
  const url = useEmbedUrl();
  const title = useDescriptiveWidgetName()();

  const iframe = <iframe title={title} src={url} width={600} height={400} />;

  return renderToStaticMarkup(iframe);
};

export const stringifyPreferences = preferences =>
  qs.stringify(preferences, {
    allowDots: true,
    addQueryPrefix: true,
    skipNulls: true
  });

export const parsePreferences = queryString =>
  qs.parse(queryString, {
    allowDots: true,
    ignoreQueryPrefix: true,
    decoder: booleanAndIntDecoder,
    depth: 7
  });

const booleanAndIntDecoder = (string, decoder) => {
  if (string === 'true') return true;
  else if (string === 'false') return false;
  else {
    const int = parseInt(string, 10);
    if (!isNaN(int)) return int;
  }

  return decoder(string);
};
