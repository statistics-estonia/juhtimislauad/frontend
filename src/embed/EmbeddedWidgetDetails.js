import { pick } from 'lodash';
import React from 'react';
import { Columns } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import LazyChart from '../container/Dashboard/LazyChart';
import { useDescriptiveWidgetName, useWidget } from '../dashboard/WidgetProvider';
import { Translate, useTranslate } from '../i18n/LanguageProvider';
import './EmbeddedWidgetDetails.scss';
import { useWidgetUrl } from './hooks';

const EmbeddedWidgetDetails = props => {
  useWidgetTitle();

  const widgetProps = pick(props, 'disableControls', 'disableAnimation');
  const widgetUrl = useWidgetUrl();
  const translate = useTranslate();

  return (
    <div style={{ overflowY: 'auto', overflowX: 'hidden' }}>
      <div className="embedded-widget" style={{ height: '100vh', backgroundColor: 'white' }}>
        <Header />
        <LazyChart
          {...widgetProps}
          isEmbedded
          isInDetailView
          onLayoutReady={() => {
            window.dispatchEvent(new CustomEvent('widgetready'));
          }}
        />
        <div className="graph-link is-size-7">{translate('stat.embed.widgetUrl', { widgetUrl })}</div>
      </div>
    </div>
  );
};

const useWidgetTitle = () => {
  const widget = useWidget();

  React.useEffect(() => {
    if (widget) document.title = widget.shortname.replace(/&shy;/g, '');
  }, [widget]);
};

const Header = () => {
  const widget = useWidget();
  const name = useDescriptiveWidgetName()();

  return (
    <Columns>
      <Columns.Column>
        <Heading style={{ fontSize: '18px' }} size={4}>
          {name}
        </Heading>
      </Columns.Column>
      <Columns.Column narrow={true}>
        <p className="is-size-7">
          <Translate>{translate => `${translate('stat.chart.source')}: ${widget.source}`}</Translate>
        </p>
      </Columns.Column>
    </Columns>
  );
};

export default EmbeddedWidgetDetails;
