import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Columns, Container, Hero, Section, Card, Heading } from 'react-bulma-components';
import Header from '../../components/Header';
import Select from '../../components/Select/';
import Footer from '../../components/Footer/Footer';
import { useGetAllRoles, useGetRegions } from '../../dashboard/api';
import { Translate, useActiveLanguageCode } from '../../i18n/LanguageProvider';
import { usePromise, useMedia, useLocalStorage } from '../../shared/hooks';
import { capitalizeRegionName, createDangerousHTML, getSlug } from '../../shared/utils';
import './Home.scss';
import { useFetch } from '../../api/ApiProvider';
import { useIsGuest, useIsLoggedIn } from '../../auth/UserProvider';
import Collapse from '../../components/Collapse/Roles/Collapse';

const Home = () => {
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const getAllRoles = useGetAllRoles();
  const isLoggedIn = useIsLoggedIn();

  const roles = usePromise(getAllRoles, [getAllRoles], []);

  return (
    <>
      <div className="home">
        <Header />
        <Hero>
          <Hero.Body>
            <Container className="home-heading-container">
              <Heading renderAs={'h2'} size={2}>
                <Translate>
                  {isMobile
                    ? 'stat.header.dashboards'
                    : isLoggedIn
                    ? 'stat.domains.allDashboards'
                    : 'stat.header.dashboards'}
                </Translate>
              </Heading>
              <Heading className="subtitle" renderAs="div">
                <Translate>stat.header.dashboards.description</Translate>
              </Heading>
            </Container>
          </Hero.Body>
        </Hero>
        <Section style={{ padding: isMobile ? 0 : '' }}>
          <Container className="home-role-container">
            {isMobile && <MobileLayout roles={roles}></MobileLayout>}
            {!isMobile && <DesktopLayout roles={roles}></DesktopLayout>}
          </Container>
        </Section>
      </div>
      <Footer />
    </>
  );
};

const DesktopLayout = ({ roles }) => {
  return roles.map((role, i) => {
    const iconPath = require(`../../assets/images/icons/${role.mainRole.code}.svg`);

    return (
      <Section key={i}>
        <Collapse
          title={role.mainRole.name}
          description={role.mainRole.description}
          icon={<img src={iconPath} alt={role.mainRole.code} />}
        >
          <Columns breakpoint="mobile" role="main">
            {role.subRoles.map(subRole => (
              <RoleCard key={subRole.id} subRole={subRole} regional={role.dashboard} />
            ))}
          </Columns>
        </Collapse>
      </Section>
    );
  });
};

const MobileLayout = ({ roles = [] }) => {
  return (
    <div className="role-list">
      {roles.map((role, i) => {
        const iconPath = require(`../../assets/images/icons/${role.mainRole.code}.svg`);

        const roleName = role.mainRole.name === 'Valdkonnaülesed teemad' ? 'Valdkonna<br />ülene' : role.mainRole.name;

        return (
          <Collapse
            key={i}
            title={<span dangerouslySetInnerHTML={{ __html: roleName }} />}
            icon={<img src={iconPath} alt={role.mainRole.code} />}
          >
            {role.subRoles.map(subRole => (
              <RoleCard key={subRole.id + Math.random() * 10 + 1} subRole={subRole} regional={role.dashboard} />
            ))}
          </Collapse>
        );
      })}
    </div>
  );
};

const RoleCard = ({ subRole: { id, code, name, dashboards = [] }, regional }) => {
  const [thumbnail, setThumbnail] = React.useState('');
  const history = useHistory();
  const languageCode = useActiveLanguageCode();
  const isTablet = useMedia(['(max-width: 1024px)', '(max-width: 769px)'], [true, false], false);
  const isDisabled = !regional && dashboards.length === 0;

  const loadThumbnail = async () =>
    await import(`../../assets/images/role-thumbnails/${code}.jpg`)
      .catch(err => console.log(err))
      .then(thumb => setThumbnail(thumb ? thumb.default : ''));
  React.useEffect(() => {
    loadThumbnail();
  }, []);

  return (
    <Columns.Column size={isTablet ? 6 : 4} className="custom-column">
      <div className="custom-column-inner">
        <Card className={`has-image${isDisabled ? ' is-disabled' : ''}`}>
          <div className="card-image">
            <img src={thumbnail} alt="" />
          </div>
          <div className="card-content">
            <Card.Header.Title
              dangerouslySetInnerHTML={createDangerousHTML(regional ? capitalizeRegionName(name) : name)}
            />
            {dashboards && !regional && (
              <Select
                id="dashboard"
                aria-label="select dashboard"
                options={dashboards}
                placeholder=""
                onChange={({ name, id }) => history.push(`/${languageCode}/${getSlug(name)}-${id}`)}
                isSearchable
                getOptionLabel={dashboard => dashboard.name}
                getOptionValue={dashboard => dashboard.id}
                noOptionsMessage={() => <Translate>stat.autocomplete.noMessageText</Translate>}
                isDisabled={isDisabled}
              />
            )}
            {regional && <RegionSelector regionId={id} dashboardId={regional.id} dashboardName={regional.name} />}
          </div>
        </Card>
      </div>
    </Columns.Column>
  );
};

const RegionSelector = ({ regionId, dashboardId, dashboardName }) => {
  const fetch = useFetch();
  const history = useHistory();
  const getRegions = useGetRegions();
  const languageCode = useActiveLanguageCode();
  const [regions, setRegions] = useState([]);
  const [optionsLoaded, setOptionsLoaded] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const isGuest = useIsGuest();
  const [, setGuestRegion] = useAsyncRegionChange(dashboardId);

  useEffect(() => {
    if (optionsLoaded) {
      setIsLoading(true);
      handleLoadOptions();
    }
  }, [languageCode]);

  const handleLoadOptions = () => {
    getRegions(regionId)
      .then(opts => {
        setRegions(opts);
        setOptionsLoaded(true);
        setIsLoading(false);
      })
      .catch(err => setIsLoading(false));
  };

  const handleMenuOpen = () => {
    if (!optionsLoaded) {
      setIsLoading(true);
      handleLoadOptions();
    }
  };

  const setRegion = region =>
    fetch(`/v2/dashboard/${dashboardId}/region`, {
      method: 'PATCH',
      body: { language: languageCode, region: parseInt(region.id) },
    }).then(() =>
      history.push(`/${languageCode}/${getSlug(dashboardName)}-${dashboardId}/${getSlug(region.name)}-${region.id}`)
    );

  const handleRegionSelectGuest = region =>
    setGuestRegion(region.mdiChevronRightCircle).then(() =>
      history.push(`/${languageCode}/${getSlug(dashboardName)}-${dashboardId}/${getSlug(region.name)}-${region.id}`)
    );

  return (
    <Select
      id="region"
      aria-label="select region"
      options={regions}
      placeholder=""
      onChange={value => (isGuest ? handleRegionSelectGuest(value) : setRegion(value))}
      isSearchable
      onMenuOpen={handleMenuOpen}
      getOptionLabel={region => region.name}
      getOptionValue={region => region.id}
      noOptionsMessage={() => <Translate>stat.autocomplete.noMessageText</Translate>}
      loadingMessage={() => <Translate>components.Select.Async.loadingMessage</Translate>}
      autoload={false}
      isLoading={isLoading}
    />
  );
};

const useAsyncRegionChange = id => {
  const [guestRegion, setGuestRegion] = useLocalStorage(`v2/dashboard/${id}/region`);
  const setter = x =>
    new Promise(resolve => {
      setGuestRegion(x);
      resolve(x);
    });
  return [guestRegion, setter];
};

export default Home;
