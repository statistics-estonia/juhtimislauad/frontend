import { mdiClose } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { Columns } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import ChartButtons from '../../components/Detail/ChartButtons';
import ShareButtons from '../../components/Detail/ShareButtons';
import IconButton from '../../components/IconButton';
import { useDescriptiveWidgetName, useWidget } from '../../dashboard/WidgetProvider';
import { Translate, useTranslate } from '../../i18n/LanguageProvider';
import { useMedia } from '../../shared/hooks';
import { createDangerousHTML } from '../../shared/utils';

const HeaderDetail = ({ onClose, setIsWidgetInfoOpen, isWidgetInfoOpen }) => {
  const widget = useWidget();
  const name = useDescriptiveWidgetName()();
  const translate = useTranslate();
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const isTablet = useMedia(['(max-width: 1024px)', '(max-width: 769px)'], [true, false], false);

  return (
    <>
      <Columns breakpoint="desktop">
        <Columns.Column>
          <Heading
            size={4}
            style={{ fontSize: '1.375rem' }}
            renderAs="h2"
            dangerouslySetInnerHTML={createDangerousHTML(widget.shortname)}
          />
        </Columns.Column>
        <Columns.Column
          tablet={{
            size: 'one-third'
          }}
        >
          <Columns breakpoint="mobile" className="is-vcentered">
            <Columns.Column
              mobile={{
                size: 'four-fifths'
              }}
            >
              <p className="is-marginless is-size-7 has-text-right">
                <Translate>{translate => `${translate('stat.chart.source')}: ${widget.source}`}</Translate>
              </p>
            </Columns.Column>
            {onClose && (
              <Columns.Column
                narrow={true}
                offset={isMobile ? null : 1}
                className="is-marginless"
                mobile={{
                  size: 'one-fifth'
                }}
              >
                <IconButton
                  title={translate('stat.chart.closeDetail')}
                  onClick={onClose}
                  aria-label="close-detailView"
                  className="is-inline float-right"
                >
                  <Icon path={mdiClose} size={1.2} />
                </IconButton>
              </Columns.Column>
            )}
          </Columns>
        </Columns.Column>
      </Columns>
      <Columns breakpoint="desktop" className={isTablet || isMobile ? '' : 'is-gapless'}>
        <Columns.Column
          tablet={{
            size: 'two-thirds'
          }}
          desktop={{
            size: 'half'
          }}
          style={{ marginBottom: '0.3em' }}
        >
          <Heading subtitle size={6} renderAs="h4">
            {name}
          </Heading>
        </Columns.Column>
        {!isMobile && (
          <Columns.Column
            className="has-text-right"
            desktop={{
              size: 'half'
            }}
          >
            <ChartButtons setIsWidgetInfoOpen={setIsWidgetInfoOpen} isWidgetInfoOpen={isWidgetInfoOpen} />
            <span style={{ marginLeft: '1rem' }}>
              <ShareButtons isWidgetInfoOpen={isWidgetInfoOpen} />
            </span>
          </Columns.Column>
        )}
        {isMobile && (
          <>
            <Columns.Column className="has-text-right">
              <ChartButtons setIsWidgetInfoOpen={setIsWidgetInfoOpen} isWidgetInfoOpen={isWidgetInfoOpen} />
            </Columns.Column>
            <Columns.Column className="has-text-right">
              <ShareButtons isWidgetInfoOpen={isWidgetInfoOpen} />
            </Columns.Column>
          </>
        )}
      </Columns>
    </>
  );
};

export default HeaderDetail;
