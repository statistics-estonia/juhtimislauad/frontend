import React from 'react';
import { Button, Columns, Container } from 'react-bulma-components';
import './Preferences.scss';
import { Translate, useTranslate } from '../../i18n/LanguageProvider';
import { useModal } from '../../dashboard/UIProvider';
import './MyDashboardHelp.scss';
import Icon from '@mdi/react';
import { mdiFilterVariant } from '@mdi/js';
import { useMedia } from '../../shared/hooks';
import { useHistory } from 'react-router-dom';

const MyDashboardHelp = ({ hasNoWidgets }) => {
  const translate = useTranslate();
  const { close, isOpen } = useModal();
  const isTinyScreen = useMedia(['(max-width: 380px)', '(min-width: 381px)'], [true, false], false);
  const history = useHistory();

  return (
    <Container className="dashboard-help" expanded={isOpen('MyDashboardHelp').toString()}>
      <Container className={`dashboard-help-container is-light has-shadow ${hasNoWidgets ? 'column-flex' : ''}`}>
        <Container className="check-list">
          <Columns breakpoint="tablet">{translate('stat.myDashboard.collapseText')}</Columns>
        </Container>
        {!hasNoWidgets && (
          <div className="button-container">
            <Button
              className={`is-primary is-small has-icon ${isOpen('MyDashboardHelp') ? 'is-active' : ''} ${
                isTinyScreen ? 'no-text' : ''
              }`}
              onClick={() => close('MyDashboardHelp')}
            >
              <Icon path={mdiFilterVariant} size={1} />
              {!isTinyScreen && <Translate>stat.myDashboard.readMore</Translate>}
            </Button>
          </div>
        )}

        {hasNoWidgets && (
          <div className="all-dashboards-button-container">
            <Button className="is-primary is-small has-icon" onClick={() => history.push('/')}>
              <Translate>stat.header.allDashboards</Translate>
            </Button>
          </div>
        )}
      </Container>
    </Container>
  );
};

export default MyDashboardHelp;
