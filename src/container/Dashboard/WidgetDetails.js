import React, { useCallback, useEffect } from 'react';
import scrollToComponent from 'react-scroll-to-component';
import { useWidget } from '../../dashboard/WidgetProvider';
import { useChartType } from '../../preferences/widget';
import { useIsMounted, useMedia } from '../../shared/hooks';
import { getFirstSeriesData } from '../../shared/series';
import FilterSection from './FilterSection';
import HeaderDetail from './HeaderDetail';
import LazyChart from './LazyChart';
import './WidgetDetail.scss';
import WidgetInfo from './WidgetInfo';

const WidgetDetails = ({ onClose, scrollToWidget = true, hideBorders = false }) => {
  useEvents();

  const [chartType] = useChartType();
  const scrollTo = useScrollTo();
  const [widgetHeight] = useWidgetHeight();

  const [isWidgetInfoOpen, setIsWidgetInfoOpen] = React.useState(false);

  return (
    <div
      className={`widget ${hideBorders ? 'no-borders' : ''}`}
      id="expanded_chart"
      ref={scrollToWidget ? scrollTo : undefined}
    >
      <HeaderDetail onClose={onClose} setIsWidgetInfoOpen={setIsWidgetInfoOpen} isWidgetInfoOpen={isWidgetInfoOpen} />
      {isWidgetInfoOpen ? (
        <WidgetInfo />
      ) : (
        <>
          <figure className={chartType} style={{ height: widgetHeight, marginBottom: 20 }}>
            <LazyChart isInDetailView />
          </figure>
          <FilterSection />
        </>
      )}
    </div>
  );
};

const useScrollTo = () => {
  const isMounted = useIsMounted();

  return useCallback(node => {
    setTimeout(() => isMounted && scrollToComponent(node, { duration: 500 }), 200);
  }, []);
};

export const useWidgetHeight = () => {
  const [chartType] = useChartType();
  const { diagram } = useWidget();
  const isMobile = useMedia(['(max-width: 812px)', '(min-width: 813px)'], [true, false], false);
  const isTinyScreen = useMedia(['(max-width: 375px)', '(min-width: 376px)'], [true, false], false);
  const contentHeight =
    chartType === 'vertical'
      ? getFirstSeriesData(diagram.series).length * 15
      : isMobile
      ? diagram.series.length * 50 < 300
        ? 300
        : diagram.series.length * 50
      : diagram.series.length * 40;

  const isOverflowingChart = contentHeight > 0.6 * window.innerHeight;

  React.useEffect(() => {
    dispatchResizeEvent();
  }, [contentHeight]);

  return [
    isOverflowingChart ? contentHeight : isMobile ? (isTinyScreen ? '400px' : 0.7 * window.innerHeight) : '60vh',
    isOverflowingChart
  ];
};

const dispatchResizeEvent = () => {
  const event = document.createEvent('Event');
  event.initEvent('resize', true, true);
  window.dispatchEvent(event);
};

const useEvents = () => {
  const widget = useWidget();

  useEffect(() => {
    document.dispatchEvent(new CustomEvent('widgetexpanded', { detail: { widget } }));
  }, []);
};

export default WidgetDetails;
