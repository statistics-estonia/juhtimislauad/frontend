import { chunk, flatMap, groupBy } from 'lodash';
import React, { useMemo } from 'react';
import { Columns, Container, Section } from 'react-bulma-components';
import { useIsGuest } from '../../auth/UserProvider';
import VisibleLazyLoader from '../../components/VisibleLazyLoader';
import { useWidgets } from '../../dashboard/DashboardProvider';
import WidgetProvider from '../../dashboard/WidgetProvider';
import ExportProvider from '../../export/ExportProvider';
import { useAreNoWidgetsActive, useSelectedWidgets } from '../../preferences/dashboard';
import { ApiWidgetPreferencesProvider, LocalStorageWidgetPreferencesProvider } from '../../preferences/widget';
import { useDeferredState, useMedia } from '../../shared/hooks';
import { moveArrayItem } from '../../shared/utils';
import WidgetCacheProvider from '../../widget/WidgetCacheProvider';
import Thumbnail from './Thumbnail';
import WidgetDetails from './WidgetDetails';
import './Widgets.scss';
import scrollToComponent from 'react-scroll-to-component';
import { useDrawer } from '../../dashboard/UIProvider';

const GRID_SIZE = 12;

const Widgets = ({ dashboard }) => {
  const { toggleDrawer } = useDrawer();
  const [expandedWidgetId, setExpandedWidgetId] = React.useState(null);
  const expandedWidgetThumb = React.useRef(null);
  const allWidgets = useWidgets();

  const clearExpandedWidgetId = () => {
    if (expandedWidgetThumb.current) {
      scrollToComponent(expandedWidgetThumb.current, { duration: 500 });
    }
    setExpandedWidgetId(null);
  };

  const [storedWidgetIds, setStoredSelectedWidgets] = useSelectedWidgets();
  const [widgetIds, setSelectedWidgets] = useDeferredState(storedWidgetIds);

  React.useEffect(() => {
    if (!widgetIds.includes(expandedWidgetId)) clearExpandedWidgetId();
  }, [widgetIds]);

  const onMove = (fromIndex, toIndex) => setSelectedWidgets(ids => moveArrayItem(ids, fromIndex, toIndex));
  const onDrop = () => setStoredSelectedWidgets(widgetIds);

  const deactivateWidget = widgetId => setStoredSelectedWidgets(widgetIds.filter(id => id !== widgetId));

  const filteredWidgets = useMemo(() => {
    const allWidgetsById = groupBy(allWidgets, 'id');
    return widgetIds.filter(id => allWidgetsById[id]);
  }, [widgetIds, allWidgets]);

  return (
    <WidgetCacheProvider>
      <Layout
        title={dashboard.name}
        dashboard={dashboard}
        detailView={
          <Providers widgetId={expandedWidgetId}>
            <WidgetDetails onClose={clearExpandedWidgetId} />
          </Providers>
        }
        expandedWidgetId={expandedWidgetId}
        toggleDrawer={toggleDrawer}
        onClose={clearExpandedWidgetId}
      >
        {filteredWidgets.map((widgetId, i) => (
          <VisibleLazyLoader key={widgetId} widgetId={widgetId}>
            {(waypoint, isVisible) => (
              <div ref={expandedWidgetId === widgetId ? expandedWidgetThumb : undefined} style={{ minHeight: 350 }}>
                {waypoint}
                {isVisible && (
                  <Providers widgetId={widgetId}>
                    <Thumbnail
                      widgetId={widgetId}
                      arrayIndex={i}
                      onClick={() => setExpandedWidgetId(widgetId)}
                      onDelete={() => deactivateWidget(widgetId)}
                      onMove={onMove}
                      onDrop={onDrop}
                    />
                  </Providers>
                )}
              </div>
            )}
          </VisibleLazyLoader>
        ))}
      </Layout>
    </WidgetCacheProvider>
  );
};

const Providers = ({ widgetId, children }) => {
  const isGuest = useIsGuest();

  const exportProvider = <ExportProvider>{children}</ExportProvider>;

  const widgetProvider = (
    <WidgetProvider widgetId={widgetId}>
      {isGuest ? exportProvider : <ApiWidgetPreferencesProvider>{exportProvider}</ApiWidgetPreferencesProvider>}
    </WidgetProvider>
  );

  return isGuest ? (
    <LocalStorageWidgetPreferencesProvider widgetId={widgetId}>{widgetProvider}</LocalStorageWidgetPreferencesProvider>
  ) : (
    widgetProvider
  );
};

const Layout = ({ detailView, expandedWidgetId, children, title, dashboard }) => {
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const isTablet = useMedia(['(max-width: 1023px)', '(max-width: 769px)'], [true, false], false);
  const ROW_SIZE = isTablet ? 2 : 3;
  const allWidgetsDeactivated = useAreNoWidgetsActive();
  const isTinyScreen = useMedia(['(max-width: 380px)', '(min-width: 381px)'], [true, false], false);

  const body = flatMap(chunk(React.Children.toArray(children), ROW_SIZE), children => [
    ...children.map(child => (
      <Columns.Column className="thumbnail" key={child.props.widgetId} size={GRID_SIZE / ROW_SIZE}>
        {child}
      </Columns.Column>
    )),
    children.some(child => expandedWidgetId === child.props.widgetId) ? (
      <Columns.Column className="detail-view" key={expandedWidgetId + 'expanded'} size={GRID_SIZE}>
        {detailView}
      </Columns.Column>
    ) : null,
  ]);

  return (
    <Section className="is-small main-widget-section" style={allWidgetsDeactivated ? { height: '100vh' } : {}}>
      <Container className="main-widget-container">
        <Container className="widget-container">
          <Columns className="thumbnails">{body}</Columns>
        </Container>
      </Container>
    </Section>
  );
};

export default Widgets;
