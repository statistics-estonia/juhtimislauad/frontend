import React from 'react';
import { Columns } from 'react-bulma-components';
import { useChartType } from '../../preferences/widget';
import Filters from './Filters';

const FilterSection = () => {
  const [chartType] = useChartType();

  return (
    <Columns breakpoint="tablet" className={`filters${chartType === 'map' ? ' map' : ''}`}>
      <Filters />
    </Columns>
  );
};

export default FilterSection;
