import React, { lazy, Suspense } from 'react';

const Chart = lazy(() => import('./Chart'));

const LazyChart = props => (
  <Suspense fallback={null}>
    <Chart {...props} />
  </Suspense>
);

export default LazyChart;
