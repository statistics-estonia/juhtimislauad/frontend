import React from 'react';
import { Columns, Container, Section, Heading } from 'react-bulma-components';
import { useIsGuest } from '../../auth/UserProvider';
import WidgetProvider from '../../dashboard/WidgetProvider';
import ExportProvider from '../../export/ExportProvider';
import { ApiWidgetPreferencesProvider, LocalStorageWidgetPreferencesProvider } from '../../preferences/widget';
import { useMedia } from '../../shared/hooks';
import WidgetDetails from './WidgetDetails';
import '../Dashboard/Widgets.scss';

const SingleWidget = ({ dashboard, graphId }) => (
  <Layout title={dashboard.name}>
    <Providers widgetId={graphId}>
      <WidgetDetails />
    </Providers>
  </Layout>
);

const Providers = ({ widgetId, children }) => {
  const isGuest = useIsGuest();

  const exportProvider = <ExportProvider>{children}</ExportProvider>;

  const widgetProvider = (
    <WidgetProvider widgetId={widgetId}>
      {isGuest ? exportProvider : <ApiWidgetPreferencesProvider>{exportProvider}</ApiWidgetPreferencesProvider>}
    </WidgetProvider>
  );

  return isGuest ? (
    <LocalStorageWidgetPreferencesProvider widgetId={widgetId}>{widgetProvider}</LocalStorageWidgetPreferencesProvider>
  ) : (
    widgetProvider
  );
};

const Layout = ({ children, title }) => {
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const isTablet = useMedia(['(max-width: 1023px)', '(max-width: 769px)'], [true, false], false);

  return (
    <Section className="is-small">
      <Container>
        {(isMobile || isTablet) && <Heading size={4}>{title}</Heading>}
        <Columns>
          <Columns.Column className="detail-view" size={12}>
            {children}
          </Columns.Column>
        </Columns>
      </Container>
    </Section>
  );
};

export default SingleWidget;
