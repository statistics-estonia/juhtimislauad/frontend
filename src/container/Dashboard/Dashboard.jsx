import React, { useEffect } from 'react';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header';
import DashboardProvider, {
  useCurrentDashboard,
  useGetRegionSlugById,
  useIsPersonalDashboard,
  useSetMyDashboardVisited,
  useWidgets,
} from '../../dashboard/DashboardProvider';
import { Button, Container, Heading } from 'react-bulma-components';
import Icon from '@mdi/react';
import { mdiFilterVariant } from '@mdi/js';
import { Translate, useTranslate } from '../../i18n/LanguageProvider';
import { useSelectedWidgets, useIsFirstVisit, useRegion } from '../../preferences/dashboard';
import './Dashboard.scss';
import Preferences from './Preferences';
import MyDashboardHelp from './MyDashboardHelp';
import Widgets from './Widgets';
import { useModal } from '../../dashboard/UIProvider';
import { useMedia, useSlugIds } from '../../shared/hooks';
import { generatePath, useHistory, useRouteMatch } from 'react-router';
import { getSlug } from '../../shared/utils';
import { SLUG } from '../../shared/constants';
import SingleWidget from './SingleWidget';
import Breadcrumb from '../../components/Header/NavPrimary/Breadcrumb';

const DashboardRoute = ({ personal }) => {
  const { dashboardId, graphId } = useSlugIds();
  return (
    <DashboardProvider dashboardId={personal ? 'me' : dashboardId}>
      <Dashboard graphId={graphId} />
    </DashboardProvider>
  );
};

const Dashboard = ({ graphId }) => {
  useEvents();

  const { open, isOpen } = useModal();
  const [selectedWidgets] = useSelectedWidgets();
  const allWidgets = useWidgets();
  const dashboard = useCurrentDashboard();
  const isPersonalDashboard = useIsPersonalDashboard();
  const setMyDashboardVisited = useSetMyDashboardVisited();
  const isFirstVisit = useIsFirstVisit();
  const isTinyScreen = useMedia(['(max-width: 380px)', '(min-width: 381px)'], [true, false], false);
  const history = useHistory();
  const { params, path } = useRouteMatch();
  const [persistedRegionId, setPersistedRegion] = useRegion();
  const translate = useTranslate();
  const { regionId } = useSlugIds();
  const regionSlug = useGetRegionSlugById(regionId);
  const isRegional = dashboard.type === 'REGIONAL';
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const iconPath = require(`../../assets/images/icons/${isPersonalDashboard ? 'GOV' : dashboard.mainRoleCode}.svg`);

  if (!regionSlug && isRegional && !persistedRegionId) {
    const id = dashboard.regions[0].id;
    setPersistedRegion(id);
  }

  if (regionSlug && isRegional && regionId !== persistedRegionId) {
    setPersistedRegion(regionId);
  }

  const hasNoWidgets = isPersonalDashboard
    ? !allWidgets || !allWidgets.length
    : !selectedWidgets || !selectedWidgets.length;

  useEffect(() => {
    if (isFirstVisit && isPersonalDashboard) setMyDashboardVisited();
    if ((isPersonalDashboard && isFirstVisit) || hasNoWidgets) open('MyDashboardHelp');
  }, []);

  useEffect(() => {
    if (isPersonalDashboard) return;
    const { pathname } = history.location;
    const { graphSlug } = params;
    const nextGraphSlug = graphSlug ? `${translate('stat.graph')}-${graphId}` : null;

    if (isRegional && !regionSlug) {
      const currentRegion = persistedRegionId
        ? dashboard.regions.find(r => r.id === persistedRegionId)
        : dashboard.regions[0];
      const regionSlug = getSlug(`${currentRegion.name}-${currentRegion.id}`);
      const optionalGraphSlug = nextGraphSlug ? `/${SLUG.GRAPH}` : '';

      const path = `/:lang/${SLUG.DASHBOARD}/${SLUG.REGION}${optionalGraphSlug}`;
      const nextPath = generatePath(path, { ...params, regionSlug, graphSlug: nextGraphSlug });
      if (nextPath !== pathname) {
        return history.replace(nextPath);
      }
    }

    const nextPath = generatePath(path, {
      ...params,
      dashboardSlug: dashboard.slug,
      regionSlug,
      graphSlug: nextGraphSlug,
    });
    if (nextPath !== pathname) {
      history.replace(nextPath);
    }
  }, [dashboard.lang]);

  if (!dashboard) return null;

  if (graphId) {
    return (
      <>
        <div className="Dashboard">
          <Header dashboard={dashboard} />
          <SingleWidget dashboard={dashboard} graphId={graphId} />
        </div>
        <Footer />
      </>
    );
  }

  return (
    <>
      <div className="Dashboard">
        <Header dashboard={dashboard} />
        <div className="stat-header-dashboards">
          <Container
            className={`dashboard-headings-container ${
              isOpen('MyDashboardHelp') && isPersonalDashboard && !hasNoWidgets ? 'hidden' : ''
            }`}
          >
            <div className="heading-container">
              <Heading renderAs={'h2'}>
                <Translate>
                  {isPersonalDashboard ? 'stat.domains.myDashboards' : 'stat.domains.allDashboards'}
                </Translate>
              </Heading>
              <Heading className="subtitle" renderAs="div">
                <Translate>
                  {isPersonalDashboard ? 'stat.myDashboard.newFeatureHeader' : 'stat.header.dashboards.description'}
                </Translate>
              </Heading>
            </div>

            {isPersonalDashboard && !hasNoWidgets && (
              <div className="button-container" style={{ marginRight: isMobile ? '20px' : '0' }}>
                <Button
                  className={`is-primary is-small has-icon ${isOpen('MyDashboardHelp') ? 'is-active' : ''} ${
                    isTinyScreen ? 'no-text' : ''
                  }`}
                  onClick={() => open('MyDashboardHelp')}
                >
                  <Icon path={mdiFilterVariant} size={1} />
                  {!isTinyScreen && <Translate>stat.myDashboard.readMore</Translate>}
                </Button>
              </div>
            )}
          </Container>

          {isPersonalDashboard && isOpen('MyDashboardHelp') && (
            <div className="my-dashboard-help">
              <MyDashboardHelp hasNoWidgets={hasNoWidgets} />
            </div>
          )}
        </div>

        <>
          {!(isPersonalDashboard && (isMobile || hasNoWidgets)) && (
            <Container className="dashboard-main-container">
              <Breadcrumb dashboard={dashboard} mainRoleName={dashboard.mainRoleName} dashboardName={dashboard.name} />
              <Container className="role-container">
                <img src={iconPath} alt={`${dashboard.mainRoleCode} icon`} className="role-icon" />

                {isPersonalDashboard ? (
                  <Heading renderAs={isMobile ? 'h4' : 'h2'} size={isMobile ? 4 : 2}>
                    <Translate>stat.domains.myDashboards</Translate>
                  </Heading>
                ) : (
                  <span>{dashboard.mainRoleName}</span>
                )}

                {!isPersonalDashboard && (
                  <button className="is-primary" onClick={() => open('Preferences')}>
                    <Translate>stat.header.options</Translate>
                  </button>
                )}

                {isOpen('Preferences') && (
                  <div className="preferences-modal">
                    <Preferences />
                  </div>
                )}
              </Container>
            </Container>
          )}

          {!hasNoWidgets && <Widgets dashboard={dashboard} />}
        </>
      </div>
      <Footer />
    </>
  );
};

const useEvents = () => {
  const dashboard = useCurrentDashboard();

  useEffect(() => {
    document.dispatchEvent(new CustomEvent('dashboardopened', { detail: { dashboard } }));
  }, []);
};

export default DashboardRoute;
