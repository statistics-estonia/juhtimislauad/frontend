import React, { useEffect, useMemo } from 'react';
import { Button, Container, Form, Heading } from 'react-bulma-components';
import Checkbox from '../../components/Checkbox';
import Collapse from '../../components/Collapse';
import { useDomains, useWidgets } from '../../dashboard/DashboardProvider';
import { Translate } from '../../i18n/LanguageProvider';
import {
  useIsDomainActive,
  useIsFirstVisit,
  useIsWidgetActive,
  useSelectedWidgets,
  useToggleDomain,
  useToggleWidget,
} from '../../preferences/dashboard';
import { useModal } from '../../dashboard/UIProvider';
import { useMedia } from '../../shared/hooks';
import './Preferences.scss';
import Toggle from '../../components/Toggle/Toggle';
import Icon from '@mdi/react';
import { mdiClose } from '@mdi/js';
import IconButton from '../../components/IconButton';

const Preferences = () => {
  const isWidgetActive = useIsWidgetActive();
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const domains = useDomains();
  const widgets = useWidgets();
  const areAllWidgetsActive = useMemo(
    () => widgets.map(widget => widget.id).every(isWidgetActive),
    [widgets, isWidgetActive]
  );

  const activateAllWidgets = () => setSelectedWidgets(widgets.map(widget => widget.id));
  const deactivateAllWidgets = () => setSelectedWidgets([]);
  const setSelectedWidgets = useSelectedWidgets()[1];

  const isFirstVisit = useIsFirstVisit();

  useEffect(() => {
    if (isFirstVisit) activateAllWidgets();
  }, [isFirstVisit, domains]);

  const preferences = (
    <div className="domains-wrapper">
      <div className="domain-label-container">
        <label className="domain-label">
          <Translate>stat.domains.domain</Translate>
        </label>
      </div>

      {isMobile ? (
        <div className="domain-list">
          {domains.map(domain => (
            <div key={domain.id} className="domain-block">
              <Domain domain={domain} showDivider={true} isMobile={isMobile} />
            </div>
          ))}
        </div>
      ) : (
        <div className="domain-grid">
          {domains.map((domain, index) => (
            <Domain key={domain.id} domain={domain} showDivider={index < domains.length - 1} />
          ))}
        </div>
      )}
    </div>
  );

  return (
    <Layout
      preferences={preferences}
      areAllWidgetsActive={areAllWidgetsActive}
      deactivateAllWidgets={deactivateAllWidgets}
      activateAllWidgets={activateAllWidgets}
      isMobile={isMobile}
    />
  );
};

const Domain = ({ domain, showDivider, isMobile }) => {
  const isDomainActive = useIsDomainActive();
  const toggleDomain = useToggleDomain();
  const isWidgetActive = useIsWidgetActive();
  const toggleWidget = useToggleWidget();
  return (
    <div className={`domain-content-container ${isMobile ? 'mobile-domain' : ''}`}>
      <div className="widget-preferences-container">
        <ToggleControl
          label={domain.name}
          checked={isDomainActive(domain.id)}
          onChange={() => toggleDomain(domain.id)}
        />
      </div>

      {isMobile ? (
        <div className="mobile-widgets-container">
          {domain.widgets.map(widget => (
            <div key={widget.id} className="mobile-widget-item">
              <CheckboxControl
                label={widget.shortname.replace(/&shy;/g, '')}
                checked={isWidgetActive(widget.id)}
                onChange={() => toggleWidget(widget.id)}
              />
            </div>
          ))}
        </div>
      ) : (
        <div className="widgets-container">
          {domain.widgets.map(widget => (
            <div key={widget.id} className="widget-item">
              <Form.Field className="check">
                <CheckboxControl
                  label={widget.shortname.replace(/&shy;/g, '')}
                  checked={isWidgetActive(widget.id)}
                  onChange={() => toggleWidget(widget.id)}
                />
              </Form.Field>
            </div>
          ))}
        </div>
      )}

      {showDivider && <hr className="domain-divider" />}
    </div>
  );
};

const CheckboxControl = ({ label, checked, onChange }) => (
  <Form.Control>
    <Checkbox checked={checked} onChange={onChange}>
      {label}
    </Checkbox>
  </Form.Control>
);

const ToggleControl = ({ label, checked, onChange, ...props }) => (
  <Form.Control>
    <Toggle label={label} checked={checked} onChange={onChange} {...props} />
  </Form.Control>
);

const Layout = ({ preferences, deactivateAllWidgets, activateAllWidgets }) => {
  const { close, isOpen } = useModal();
  return (
    <Collapse className="collapse-container" expanded={isOpen('Preferences')}>
      <Container className="check-list">
        <Container className="headings-container">
          <div className="headings-content">
            <Heading size={5} renderAs="h4" className="styled-heading">
              <Translate>stat.domains.chooseFromDash</Translate>
            </Heading>

            <Heading size={5} className="styled-heading">
              <FakeLink onClick={activateAllWidgets} aria-label="activate-all-widgets">
                <Translate>stat.domains.activateAll</Translate>
              </FakeLink>
              {', '}
              <FakeLink onClick={deactivateAllWidgets} aria-label="deactivate-all-widgets">
                <Translate>stat.domains.deactivateAll</Translate>
              </FakeLink>
            </Heading>
          </div>
          <div className="icon-button-container">
            <IconButton onClick={() => close('Preferences')} aria-label="close-detailView" className="close-button">
              <Icon path={mdiClose} size={1.2} />
            </IconButton>
          </div>
        </Container>
        {preferences}
        <Container className="btn-confirm-selection">
          <Button outlined={true} color="primary" onClick={() => close('Preferences')} aria-label="confirm-selection">
            <Translate>stat.domains.confirm</Translate>
          </Button>
        </Container>
      </Container>
    </Collapse>
  );
};

const FakeLink = props => <span className="has-text-link" style={{ cursor: 'pointer' }} {...props} />;

export default Preferences;
