import React from 'react';
import { Columns } from 'react-bulma-components';
import { Content } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { useWidget } from '../../dashboard/WidgetProvider';
import { Translate } from '../../i18n/LanguageProvider';
import { useLocalizedDate } from '../../i18n/hooks';

const WidgetInfo = () => {
  const widget = useWidget();
  const { description, note, methodsLinks, statCubes, dataUpdatedAt, statisticianJobLinks } = widget;
  const dateUpdated = useLocalizedDate(dataUpdatedAt);

  return (
    <Columns>
      {description && (
        <Columns.Column size={6}>
          <Heading size={5} renderAs="h3">
            <Translate>stat.widgetInfo.explanation</Translate>
          </Heading>
          <p style={{ whiteSpace: 'pre-wrap' }}>{description}</p>
        </Columns.Column>
      )}
      <Columns.Column size={6}>
        {note && (
          <Heading size={5} renderAs="h3">
            <Translate>stat.widgetInfo.notes</Translate>
          </Heading>
        )}
        <Content>
          {note && <p style={{ whiteSpace: 'pre-wrap' }}>{note}</p>}
          {statCubes && statCubes.length > 0 && (
            <StatCubeLinks title="stat.widgetInfo.baseData" statCubes={statCubes} />
          )}
          {dateUpdated && (
            <p>
              <span className="has-text-weight-semibold">
                <Translate>stat.widgetInfo.lastUpdate</Translate>: {''}
              </span>
              {dateUpdated}
            </p>
          )}
          {methodsLinks && methodsLinks.length > 0 && (
            <FooterFieldWithLinks title="stat.widgetInfo.methods" links={methodsLinks} />
          )}
          {statisticianJobLinks && statisticianJobLinks.length > 0 && (
            <FooterFieldWithLinks title="stat.widgetInfo.statJob" links={statisticianJobLinks} />
          )}
        </Content>
      </Columns.Column>
    </Columns>
  );
};

const FooterFieldWithLinks = ({ title, links }) => (
  <p>
    <span className="has-text-weight-semibold">
      <Translate>{title}</Translate>: {''}
    </span>
    {links
      .map(link => (
        <a href={link} className="has-text-link has-link-break" target="_blank" rel="noopener noreferrer">
          {link}
        </a>
      ))
      .reduce((prev, curr) => [prev, ', ', curr])}
  </p>
);

const StatCubeLinks = ({ title, statCubes }) => (
  <p>
    <span className="has-text-weight-semibold">
      <Translate>{title}</Translate>: {''}
    </span>
    {statCubes
      .map(({ cube, url }) => (
        <a href={url} className="has-text-link has-link-break" target="_blank" rel="noopener noreferrer">
          {cube}
        </a>
      ))
      .reduce((prev, curr) => [prev, ', ', curr])}
  </p>
);

export default WidgetInfo;
