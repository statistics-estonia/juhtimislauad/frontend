import { mdiArrowAll, mdiClose, mdiPin } from '@mdi/js';
import Icon from '@mdi/react';
import React, { useRef } from 'react';
import { Card } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { useDrag, useDrop } from 'react-dnd';
import IconButton from '../../components/IconButton';
import { useDescriptiveWidgetName, useDomainNames, useWidget } from '../../dashboard/WidgetProvider';
import { useTranslate } from '../../i18n/LanguageProvider';
import LazyChart from './LazyChart';
import './Thumbnail.scss';
import { useIsGuest } from '../../auth/UserProvider';
import { useWidgetPinned } from '../../preferences/widget';
import { useIsPersonalDashboard } from '../../dashboard/DashboardProvider';
import { useMedia } from '../../shared/hooks';
import { createDangerousHTML } from '../../shared/utils';

const Thumbnail = ({ widgetId, arrayIndex, onClick, onDelete, onMove, onDrop }) => {
  const widget = useWidget();
  const descriptiveName = useDescriptiveWidgetName()();
  const footerText = useDomainNames();
  const translate = useTranslate();
  const isGuest = useIsGuest();
  const isPersonalDashboard = useIsPersonalDashboard();
  const [pinned, setPinned] = useWidgetPinned();
  const handlePin = () => setPinned(widget.id, { pinned: !pinned });
  const isTabletOrBigger = useMedia(['(min-width: 769px)', '(max-width: 768px)'], [true, false], false);

  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: TYPE, widgetId, arrayIndex },
    collect: monitor => ({
      isDragging: monitor.isDragging()
    })
  });

  const [, drop] = useDrop({
    accept: TYPE,
    hover: item => {
      const { arrayIndex: draggedIndex } = item;
      const hoveredIndex = arrayIndex;

      if (draggedIndex === hoveredIndex) return;

      onMove(draggedIndex, hoveredIndex);
      item.arrayIndex = hoveredIndex;
    },
    drop: () => onDrop()
  });

  const thumbnailRef = useRef();
  const dragRef = useRef();

  drag(dragRef);
  preview(thumbnailRef);
  drop(thumbnailRef);

  const PinButton = () => (
    <HeaderIconButton
      title={
        isGuest
          ? translate('stat.share.pinGuest')
          : pinned
          ? translate('stat.share.unpin')
          : translate('stat.share.pin')
      }
      className={`icon--button${pinned ? ' active' : ''}`}
      icon={mdiPin}
      aria-label="pin"
      disabled={isGuest}
      onClick={handlePin}
    />
  );

  return (
    <Card className="chart-thumbnail" ref={thumbnailRef} onClick={onClick} style={{ opacity: isDragging ? 0 : 1 }}>
      <Card.Header>
        <Card.Header.Title>
          <Heading
            size={5}
            renderAs="h5"
            className="is-spaced"
            dangerouslySetInnerHTML={createDangerousHTML(widget.shortname)}
          />
        </Card.Header.Title>
        <Card.Header.Icon>
          {!isPersonalDashboard && !(isGuest && !isTabletOrBigger) && <PinButton />}
          {isTabletOrBigger && (
            <HeaderIconButton
              title={translate('stat.chart.moveThumb')}
              className="icon--button"
              icon={mdiArrowAll}
              aria-label="drag"
              renderAs="span"
              ref={dragRef}
            />
          )}

          {isPersonalDashboard && !(isGuest && !isTabletOrBigger) ? (
            <PinButton />
          ) : (
            <HeaderIconButton
              title={translate('stat.chart.closeThumb')}
              className="icon--button"
              icon={mdiClose}
              onClick={onDelete}
              aria-label="thumbnail-close"
            />
          )}
        </Card.Header.Icon>
      </Card.Header>
      <Card.Header>
        <Card.Header.Title className="is-fading">
          <Heading subtitle size={6} renderAs="p" className="has-text-grey-dark is-spaced">
            {descriptiveName}
          </Heading>
        </Card.Header.Title>
      </Card.Header>
      <Card.Content>
        <LazyChart />
      </Card.Content>
      <Card.Footer>
        <Card.Footer.Item>
          <span className="is-size-7 has-text-right has-text-grey-dark is-uppercase is-spaced">{footerText}</span>
        </Card.Footer.Item>
      </Card.Footer>
    </Card>
  );
};

const HeaderIconButton = React.forwardRef(({ icon, onClick, ...props }, ref) => (
  <IconButton
    ref={ref}
    onClick={event => {
      event.stopPropagation();
      if (onClick) onClick(event);
    }}
    {...props}
  >
    <Icon path={icon} size={1} />
  </IconButton>
));

const TYPE = 'thumbnail';

export default Thumbnail;
