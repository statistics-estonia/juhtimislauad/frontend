import React from 'react';
import AreaChart from '../../components/Charts/AreaChart';
import BarChart, { StackedBarChart } from '../../components/Charts/BarChart';
import LineChart from '../../components/Charts/LineChart';
import PieChart from '../../components/Charts/PieChart';
import PopulationPyramid from '../../components/Charts/PopulationPyramid';
import RadarChart from '../../components/Charts/RadarChart';
import Treemap from '../../components/Charts/Treemap';
import LazyMap from '../../components/Map/LazyMap';
import VerticalBarChart from '../../components/VerticalBarChart';
import WidgetError from '../../components/WidgetError';
import { useWidget } from '../../dashboard/WidgetProvider';
import { Translate } from '../../i18n/LanguageProvider';
import { useChartType } from '../../preferences/widget';

export const getLegendFilter = filters => filters.filter(filter => filter.type === 'LEGEND').shift();

export const getDefaultFilters = filters =>
  filters.reduce((result, filter) => {
    if (filter.defaultOptions) result[filter.name] = filter.defaultOptions[0];
    return result;
  }, {});

const chartTypeToComponent = chartType => {
  switch (chartType) {
    case 'bar':
      return BarChart;
    case 'stacked':
      return StackedBarChart;
    case 'vertical':
      return VerticalBarChart;
    case 'map':
      return LazyMap;
    case 'area':
      return AreaChart;
    case 'pie':
      return PieChart;
    case 'treemap':
      return Treemap;
    case 'pyramid':
      return PopulationPyramid;
    case 'radar':
      return RadarChart;
    default:
      return LineChart;
  }
};

const Chart = ({ isInDetailView = false, ...props }) => {
  const widget = useWidget();
  const [chartType] = useChartType();

  const { diagram: { series = [] } = {} } = widget;

  if (widget.graphType !== chartType) return null;
  if (series.length === 0) return <NoData />;

  return React.createElement(chartTypeToComponent(chartType), {
    widget,
    isExpanded: isInDetailView,
    ...props
  });
};

const NoData = () => (
  <WidgetError>
    <Translate>stat.widgetProvider.noData</Translate>
  </WidgetError>
);

export default Chart;
