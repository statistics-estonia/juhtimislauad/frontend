import React from 'react';
import { Columns } from 'react-bulma-components';
import Select from '../../components/Select/';
import { useWidget } from '../../dashboard/WidgetProvider';
import { useFormatDate } from '../../i18n/hooks';
import { Translate, useTranslate } from '../../i18n/LanguageProvider';
import { useFilterValue } from '../../preferences/widget';
import { wrapToArray } from '../../shared/utils';

const Filters = () => {
  const [getFilterValue, setFilterValue] = useFilterValue();
  const formatDate = useFormatDate();
  const translate = useTranslate();

  const { diagram = {} } = useWidget();
  const { filters = [] } = diagram;

  return filters
    .filter(filter => filter.type === 'MENU')
    .map(({ id, values, name, numberOfOptions, time: isTimeFilter, region }) => {
      const isMulti = numberOfOptions > 1;
      const storedValue = getFilterValue(id);
      const selectedValues = values.filter(option => wrapToArray(storedValue).includes(option.id));
      const value =
        selectedValues.length === 0 && storedValue === undefined ? getDefaultOptions(values) : selectedValues;

      const onChange = value =>
        setFilterValue(
          id,
          wrapToArray(value)
            .map(option => option.id)
            .slice(0, numberOfOptions),
          { isRegional: region }
        );

      const getOptionLabel = option => (isTimeFilter ? formatDate(option.option) : option.option);
      const getOptionValue = option => option.id;

      return (
        <Columns.Column key={name} size={4} mobile={{ size: 12 }} style={{ position: 'relative' }}>
          <div className="filters-label">{`${translate('components.Select.Placeholder.choose')}${name}`}</div>
          <Select
            aria-label="filter"
            menuPlacement="top"
            options={values}
            value={isMulti ? value : value[0]}
            onChange={onChange}
            isMulti={isMulti}
            getOptionLabel={getOptionLabel}
            getOptionValue={getOptionValue}
            noOptionsMessage={() => <Translate>stat.autocomplete.noMessageText</Translate>}
            selectAllBtn={false}
            components={{}}
          />
          <span className="filters-message">
            <Translate>{translate => translate('stat.filters.maximumOptionsMessage', { numberOfOptions })}</Translate>
          </span>
        </Columns.Column>
      );
    });
};

const getDefaultOptions = values => values.filter(option => option.selected);

export default Filters;
