import { mdiDelete, mdiMagnify } from '@mdi/js';
import Icon from '@mdi/react';
import { flatMap, debounce } from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';
import { Button } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { Columns } from 'react-bulma-components';
import { Form } from 'react-bulma-components';
import { Link } from 'react-router-dom';
import IconButton from '../components/IconButton';
import Table from '../components/Table';
import { useActiveLanguageCode } from '../i18n/LanguageProvider';
import { useIsMounted } from '../shared/hooks';
import { useDeleteDashboard, useGetDashboards } from './DashboardProvider';

const Dashboards = () => {
  const deleteDashboard = useDeleteDashboard();
  const [dashboards, refreshDashboards] = useDashboards();
  const lang = useActiveLanguageCode();
  const flattenedDashboards = flatMap(dashboards, dashboard => [dashboard, ...(dashboard.subDashboards || [])]);

  const onDelete = async dashboard => {
    if (!confirmDeletion(dashboard.nameEt)) return;

    await deleteDashboard(dashboard.id);
    refreshDashboards();
  };

  return (
    <div>
      <Columns>
        <Columns.Column size="two-thirds">
          <Heading size={2}>
            Juhtimislauad
            <Button
              id="add-new-dashboard-btn"
              to={`/${lang}/admin/dashboards/new`}
              renderAs={Link}
              className="btn-custom-margin is-primary is-outlined"
              style={{ marginLeft: '30px' }}
            >
              Lisa uus
            </Button>
          </Heading>
        </Columns.Column>
        <Columns.Column>
          <Search refreshDashboards={refreshDashboards} />
        </Columns.Column>
      </Columns>
      <Table
        data={flattenedDashboards}
        columns={[
          {
            Header: 'Juhtimislaua nimi',
            accessor: 'nameEt',
            Cell: props => <NameCell {...props} lang={lang} />
          },
          {
            Header: 'Olek',
            accessor: 'status',
            Cell: StatusCell
          },
          {
            Cell: props => <ActionsCell {...props} onDelete={onDelete} />
          }
        ]}
        showPagination={false}
        pageSize={flattenedDashboards && flattenedDashboards.length > 0 ? flattenedDashboards.length : 3}
        loading={flattenedDashboards.length === 0}
        loadingText="Laadin andmeid"
        noDataText="Kirjed puuduvad"
      />
    </div>
  );
};

const useDashboards = () => {
  const getDashboards = useGetDashboards();
  const [dashboards, setDashboards] = useState([]);

  const isMounted = useIsMounted();
  const refreshDashboards = useCallback(
    async query => isMounted() && setDashboards((await getDashboards({ size: 1000, name: query })).content),
    [getDashboards]
  );

  return [dashboards, refreshDashboards];
};

const Search = ({ refreshDashboards }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const refreshDebounced = useCallback(debounce(refreshDashboards, 300), [refreshDashboards]);

  useEffect(() => {
    const trimmedQuery = searchQuery.trim();
    if (trimmedQuery.length >= 3 || trimmedQuery === '') refreshDebounced(trimmedQuery);
    return refreshDebounced.cancel;
  }, [searchQuery]);

  return (
    <Form.Control iconRight>
      <Form.Input autoComplete="off" value={searchQuery} onChange={e => setSearchQuery(e.target.value)} />
      <span className="icon is-right">
        <Icon path={mdiMagnify} size={1.5} color="#dbdbdb" />
      </span>
    </Form.Control>
  );
};

const confirmDeletion = name => window.confirm(`Oled kindel, et soovid kustutada "${name}"?`);

const NameCell = ({ value, original: dashboard, lang }) => {
  const isChild = typeof dashboard.parentId === 'number';
  return (
    <Link
      to={`/${lang}/admin/dashboards/${dashboard.id}`}
      style={{ display: 'block', paddingLeft: isChild ? '1rem' : 0 }}
    >
      {isChild && '- '}
      {value}
    </Link>
  );
};

const StatusCell = ({ value }) => <span>{value === 'VISIBLE' ? 'avaldatud' : 'peidetud'}</span>;

const ActionsCell = ({ original: dashboard, onDelete }) =>
  isEmpty(dashboard.subDashboards) ? (
    <IconButton id="delete-dashboard-btn" onClick={() => onDelete(dashboard)}>
      <Icon path={mdiDelete} size={1} />
    </IconButton>
  ) : null;

const isEmpty = array => !array || (array instanceof Array && array.length === 0);

export default Dashboards;
