import { useCallback } from 'react';
import { useFetch } from '../api/ApiProvider';

export const useGetProcessLog = () => {
  const fetch = useFetch();
  return useCallback(
    () =>
      fetch('/admin/processlog?size=9999', {
        errorNotification: 'Logide pärimine ebaõnnestus'
      }),
    [fetch]
  );
};

export const useGetProcess = () => {
  const fetch = useFetch();
  return useCallback(
    processId =>
      fetch(`/admin/processlog/${processId}/logs?size=9999`, {
        errorNotification: 'Protsessi logide pärimine ebaõnnestus'
      }),
    [fetch]
  );
};

export const intlConfig = {
  day: '2-digit',
  month: '2-digit',
  year: 'numeric',
  hour: '2-digit',
  minute: '2-digit',
  second: '2-digit'
};
