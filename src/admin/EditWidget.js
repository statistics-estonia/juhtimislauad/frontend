import { FORM_ERROR } from 'final-form';
import React, { useState, useEffect } from 'react';
import { Heading } from 'react-bulma-components';
import WidgetForm from './WidgetForm';
import { mapErrorCodeToMessage } from './WidgetForm/schema';
import { useGetWidget, useUpdateWidget } from './WidgetProvider';
import { toastError } from '../notifications';

const EditWidget = ({ match }) => {
  const getWidget = useGetWidget();
  const updateWidget = useUpdateWidget();
  const [widget, setWidget] = useState(null);
  const [widgetKey, setWidgetKey] = useState(0);

  useEffect(() => {
    handleGetWidget();
  }, [match.params.id]);

  const handleGetWidget = () => {
    getWidget(match.params.id).then(result => {
      setWidget(result);
      setWidgetKey(value => value + 1);
    });
  };

  const onSubmit = updatedWidget => {
    updateWidget(updatedWidget)
      .then(result => {
        setWidget(result);
        setWidgetKey(value => value + 1);

        return {
          [FORM_ERROR]: result.calculationProblem && [
            'Kuvamooduli metaandmed on salvestunud, aga graafiku andmete ümberarvutamisel tekkis viga.',
          ],
        };
      })
      .catch(response =>
        response.json().then(error => {
          if (error.code === 'widget_admin_week_is_restricted') {
            toastError('Nädalapõhiseid andmeid saab lisada ainult joondiagrammile');
          }
          return {
            [FORM_ERROR]: error.code && [mapErrorCodeToMessage(error.code)],
          };
        })
      );
  };

  if (!widget) return null;

  return (
    <>
      <Heading size={3}>Kuvamooduli muutmine</Heading>
      <WidgetForm key={widgetKey} value={widget} onSubmit={onSubmit} onGetWidget={handleGetWidget} />
    </>
  );
};

export default EditWidget;
