import React, { useState } from 'react';
import { Button, Form, Modal } from 'react-bulma-components';
import WidgetDetails from '../../container/Dashboard/WidgetDetails';
import WidgetProvider from '../../dashboard/WidgetProvider';
import ConnectedIntlProvider from '../../i18n/ConnectedIntlProvider';
import { FixedLanguageProvider, useLanguages } from '../../i18n/LanguageProvider';
import { InMemoryWidgetPreferencesProvider } from '../../preferences/widget';

const PreviewButton = ({ widgetId }) => {
  const languages = useLanguages();
  const [language, setLanguage] = useState(languages[0]);
  const [showPreview, setShowPreview] = useState(false);

  return (
    <>
      {showPreview && <PreviewModal widgetId={widgetId} language={language} onClose={() => setShowPreview(false)} />}
      <Form.Control>
        <Button className="is-secondary" type="button" onClick={() => setShowPreview(true)}>
          Näita eelvaadet
        </Button>
      </Form.Control>
      <Form.Control>
        <Form.Select value={language} onChange={e => setLanguage(e.target.value)}>
          {languages.map(l => (
            <option key={l} value={l}>
              {l.toUpperCase()}
            </option>
          ))}
        </Form.Select>
      </Form.Control>
    </>
  );
};

const PreviewModal = ({ widgetId, language, onClose }) => (
  <FixedLanguageProvider languageCode={language}>
    <ConnectedIntlProvider>
      <Modal show showClose={false} onClose={onClose}>
        <InMemoryWidgetPreferencesProvider>
          <WidgetProvider
            sharingDisabled
            widgetId={widgetId}
            getWidgetUrl={({ widgetId }) => `/admin/widget/${widgetId}/preview`}
          >
            <Modal.Card style={{ width: 'calc(100vw - 100px)' }}>
              <Modal.Card.Body>
                <WidgetDetails hideBorders scrollToWidget={false} onClose={onClose} />
              </Modal.Card.Body>
            </Modal.Card>
          </WidgetProvider>
        </InMemoryWidgetPreferencesProvider>
      </Modal>
    </ConnectedIntlProvider>
  </FixedLanguageProvider>
);

export default PreviewButton;
