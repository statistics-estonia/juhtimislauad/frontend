import arrayMutators from 'final-form-arrays';
import createDecorator from 'final-form-calculate';
import { groupBy } from 'lodash';
import React from 'react';
import { Button, Columns, Content, Form, Heading, Notification, Section } from 'react-bulma-components';
import { Field, Form as FinalForm, FormSpy } from 'react-final-form';
import {
  createYupValidator,
  DatePickerField,
  InputField,
  LabeledControl,
  PrimitiveSelectField,
  RadioField,
  TextareaField,
} from '../../shared/form';
import Elements from './Elements';
import PreviewButton from './PreviewButton';
import schema from './schema';
import WidgetChartTypes from './WidgetChartTypes';
import WidgetDataSource from './WidgetDataSource';

const validate = createYupValidator(schema);

const decorator = createDecorator(
  {
    field: /graphTypes\[\d\]\.defaultType/,
    updates: (value, name, allValues) =>
      allValues.graphTypes.reduce(
        (result, type, index) => ({
          ...result,
          [`graphTypes[${index}].defaultType`]: false,
          [name]: true,
        }),
        {}
      ),
  },
  {
    field: /graphTypes\[\d\]\.filters\[\d\]\.numberOfValues/,
    updates: (value, name, allValues) => {
      const [, typeIndex, filterIndex] = name.match(/graphTypes\[(\d+)\]\.filters\[(\d+)\]/).map(Number);
      const { values } = allValues.graphTypes[typeIndex].filters[filterIndex];
      const groupedByValue = groupBy(values.filter(v => v.selected).slice(0, Number(value)), 'valueEt');

      return {
        [`graphTypes[${typeIndex}].filters[${filterIndex}].values`]: values.map(v => ({
          ...v,
          selected: !!groupedByValue[v.valueEt],
        })),
      };
    },
  }
);

const WidgetForm = ({ value, onSubmit, onGetWidget }) => {
  return (
    <FinalForm
      initialValues={value}
      subscription={{ hasValidationErrors: true, errors: true }}
      onSubmit={onSubmit}
      validate={validate}
      mutators={{ ...arrayMutators }}
      decorators={[decorator]}
      render={({ hasValidationErrors, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <MetaDataSection />
          <DataSourceSection />
          <AdditionalMetaDataSection />
          <Section>
            <WidgetChartTypes
              widgetId={value.id}
              customFiltersInUse={value?.areAnyCustomFilterValuesUsed ?? false}
              widgetHasCustomFilters={value?.doAnyCustomFilterValuesExist ?? false}
              onGetWidget={onGetWidget}
            />
          </Section>
          <Section>
            <FormErrors />
            <Form.Field kind="group">
              <Form.Control>
                <Button className="is-primary" type="submit" disabled={hasValidationErrors}>
                  Salvesta
                </Button>
              </Form.Control>
              {value.id && <PreviewButton widgetId={value.id} />}
            </Form.Field>
          </Section>
        </form>
      )}
    />
  );
};

const MetaDataSection = () => (
  <Section>
    <Columns>
      <Columns.Column size="half">
        <Content>
          <LabeledControl label="Lühinimetus eesti k" name="shortnameEt" component={TextareaField} />
          <LabeledControl label="Täisnimetus eesti k" name="nameEt" component={TextareaField} />
          <LabeledControl label="Ühik eesti k" name="unitEt" component={InputField} />
        </Content>
      </Columns.Column>
      <Columns.Column size="half">
        <Content>
          <LabeledControl label="Lühinimetus inglise k" name="shortnameEn" component={TextareaField} />
          <LabeledControl label="Täisnimetus inglise k" name="nameEn" component={TextareaField} />
          <LabeledControl label="Ühik inglise k" name="unitEn" component={InputField} />
        </Content>
      </Columns.Column>
    </Columns>
    <Content>
      <LabeledControl
        name="timePeriod"
        label="Ajaperiood"
        component={PrimitiveSelectField}
        placeholder="Vali ajaperiood"
        options={timePeriodOptions}
      />
      <LabeledControl name="periods" label="Mitu viimast ajaperioodi" type="number" component={InputField} />
      <Field name="timePeriod">
        {({ input }) =>
          input.value && (
            <>
              <LabeledControl
                name="startDate"
                label="Ajaperioodi algus"
                component={DatePickerField}
                period={input.value}
              />
              <LabeledControl
                name="endDate"
                label="Ajaperioodi lõpp"
                component={DatePickerField}
                period={input.value}
              />
            </>
          )
        }
      </Field>
      <LabeledControl
        name="divisor"
        label="Väärtuse jagaja"
        component={PrimitiveSelectField}
        options={divisorOptions}
        isClearable
      />
      <LabeledControl
        name="precisionScale"
        label="Väärtuse komakohtade arv"
        component={PrimitiveSelectField}
        options={precisionScaleOptions}
        isClearable
      />
      <Form.Label>Olek</Form.Label>
      <Form.Field kind="group" id="widget-visibility">
        <Form.Control>
          <Field type="radio" name="status" value="VISIBLE" component={RadioField}>
            &nbsp;Nähtav
          </Field>
          <Field type="radio" name="status" value="HIDDEN" component={RadioField}>
            &nbsp;Peidetud
          </Field>
        </Form.Control>
      </Form.Field>
    </Content>
  </Section>
);

const DataSourceSection = () => (
  <Section>
    <Columns>
      <Columns.Column>
        <Heading size={5}>Andmeallikad</Heading>
        <WidgetDataSource />
      </Columns.Column>
      <Columns.Column>
        <Heading size={5}>Näidatakse juhtimislaual / -laudadel</Heading>
        <Elements />
      </Columns.Column>
    </Columns>
  </Section>
);

const AdditionalMetaDataSection = () => (
  <Section>
    <Columns>
      <Columns.Column size="half">
        <LabeledControl name="descriptionEt" label="Selgitus eesti k" component={TextareaField} />
        <LabeledControl name="sourceEt" label="Allikas eesti k" component={TextareaField} />
        <LabeledControl name="noteEt" label="Märkused eesti k" component={TextareaField} />
        <LabeledControl name="statisticianJobLinkEt" label="Statistikatöö eesti k" component={TextareaField} />
        <LabeledControl name="methodsLinkEt" label="Mõisted ja meetodika eesti k" component={TextareaField} />
      </Columns.Column>
      <Columns.Column size="half">
        <LabeledControl name="descriptionEn" label="Selgitus inglise k" component={TextareaField} />
        <LabeledControl name="sourceEn" label="Allikas inglise k" component={TextareaField} />
        <LabeledControl name="noteEn" label="Märkused inglise k" component={TextareaField} />
        <LabeledControl name="statisticianJobLinkEn" label="Statistikatöö inglise k" component={TextareaField} />
        <LabeledControl name="methodsLinkEn" label="Mõisted ja metoodika inglise k" component={TextareaField} />
      </Columns.Column>
    </Columns>
  </Section>
);

const FormErrors = () => (
  <FormSpy subscription={{ submitError: true }}>
    {({ submitError }) =>
      submitError
        ? submitError.map(message => (
            <Notification color="danger" key={message}>
              {message}
            </Notification>
          ))
        : null
    }
  </FormSpy>
);

const timePeriodOptions = [
  { label: 'aasta', value: 'YEAR' },
  { label: 'kvartal', value: 'QUARTER' },
  { label: 'kuu', value: 'MONTH' },
  { label: 'nädal', value: 'WEEK' },
];

const divisorOptions = [
  { label: '10', value: 10 },
  { label: '100', value: 100 },
  { label: '1 000', value: 1000 },
  { label: '10 000', value: 10000 },
  { label: '100 000', value: 100000 },
  { label: '1 000 000', value: 1000000 },
];

const precisionScaleOptions = [
  { label: '0', value: 0 },
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
  { label: '5', value: 5 },
  { label: '6', value: 6 },
];

export default WidgetForm;
