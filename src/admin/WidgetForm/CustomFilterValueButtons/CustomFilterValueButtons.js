import React from 'react';
import Icon from '@mdi/react';
import { Button } from 'react-bulma-components';
import { mdiDelete } from '@mdi/js';
import { useActiveLanguageCode } from '../../../i18n/LanguageProvider';
import { DELETE_CUSTOM_FILTERS_CONFIRMATION } from '../../adminConstants';
import { useHistory } from 'react-router-dom';
import { useDeleteAllCustomFilterValues } from '../../WidgetProvider';

const CustomFilterValueButtons = ({
  dimensions,
  widgetId,
  customFiltersInUse,
  widgetHasCustomFilters,
  onGetWidget,
}) => {
  const history = useHistory();
  const lang = useActiveLanguageCode();
  const deleteAllCustomFilters = useDeleteAllCustomFilterValues();

  const navigate = () => {
    history.push({
      pathname: `/${lang}/admin/filtervalue/new`,
      dimensions: dimensions,
      widgetId: widgetId,
    });
  };

  const deleteCustomFilters = () => {
    deleteAllCustomFilters(widgetId).then(() => {
      onGetWidget();
    });
  };

  return (
    <div>
      <div style={{ display: 'flex', gap: 16, marginBottom: 12 }}>
        <Button className="is-secondary" type="button" onClick={navigate}>
          Loo uus filtri väärtus
        </Button>
        {widgetHasCustomFilters && (
          <Button
            style={{ padding: '8px 16px' }}
            className="is-secondary"
            type="button"
            disabled={customFiltersInUse}
            title={customFiltersInUse ? 'Kasutuses olevaid filtri väärtuseid ei saa kustutada' : null}
            onClick={() => window.confirm(DELETE_CUSTOM_FILTERS_CONFIRMATION) && deleteCustomFilters()}
          >
            <Icon path={mdiDelete} size={1} />
            Kustuta loodud filtri väärtused
          </Button>
        )}
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span>
          Käsitsi lisatud filtri väärtused on kuvatud rohelise täpiga (Hiirega peal olles näeb vihjena valemit)
        </span>
        <div
          style={{
            backgroundColor: '#4DC14D',
            borderRadius: '50%',
            content: '""',
            marginLeft: 8,
            height: '8px',
            width: '8px',
          }}
        />
      </div>
    </div>
  );
};

export default CustomFilterValueButtons;
