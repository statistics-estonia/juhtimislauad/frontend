import * as yup from 'yup';
import { ARRAY_ERROR } from 'final-form';

const dimensionSchema = yup.object().shape({
  type: yup
    .string()
    .nullable()
    .required('Dimensiooni tüüp on kohustuslik'),
  values: yup
    .array()
    .label('Filtri väärtused')
    .ensure()
    .when('time', {
      is: value => !value,
      then: yup
        .array()
        .ensure()
        .when('type', {
          is: 'MENU',
          then: yup
            .array()
            .test(ARRAY_ERROR, 'Filtril peab olema vähemalt üks vaikeväärtus', value => value.some(v => v.selected))
        })
        .when('type', {
          is: 'CONSTRAINT',
          then: yup
            .array()
            .test(ARRAY_ERROR, 'Kitsendaja puhul peab filtri väärtusi olema täpselt üks.', value => value.length === 1)
        })
    })
});

const graphTypeSchema = yup.object().shape({
  filters: yup
    .array()
    .label('Dimensioonid')
    .of(dimensionSchema)
    .ensure()
    .test(ARRAY_ERROR, 'Telgesid peab olema täpselt üks.', value => value.filter(d => d.type === 'AXIS').length === 1)
    .test(
      ARRAY_ERROR,
      'Legendi tüüpi dimensioone saab olla maksimaalselt üks.',
      value => value.filter(d => d.type === 'LEGEND').length <= 1
    )
    .test(
      ARRAY_ERROR,
      'Piirkondliku andmetüübiga dimensioone saab olla maksimaalselt üks.',
      value => value.filter(d => d.region).length <= 1
    )
    .test(
      ARRAY_ERROR,
      'Ajalise andmetüübiga dimensioone peab olema täpselt üks.',
      value => value.filter(d => d.time).length === 1
    )
    .test(
      ARRAY_ERROR,
      'Filtreid (sealhulgas 1 legend) saab olla maksimaalselt neli.',
      value => value.filter(d => d.type === 'LEGEND' || d.type === 'MENU').length <= 4
    )
    .when('type', {
      is: 'map',
      then: yup
        .array()
        .test(ARRAY_ERROR, 'Kaardi puhul peab telg olema piirkondlik.', value =>
          value.some(d => d.type === 'AXIS' && d.region)
        )
    })
});

const schema = yup.object().shape({
  nameEt: yup.string().required('Nimi eesti k. on kohustuslik'),
  nameEn: yup.string().required('Nimi inglise k. on kohustuslik'),
  shortnameEt: yup.string().required('Lühinimi eesti k. on kohustuslik'),
  shortnameEn: yup.string().required('Lühinimi inglise k. on kohustuslik'),
  periods: yup
    .number()
    .integer()
    .min(1),
  graphTypes: yup.array().of(graphTypeSchema)
});

export const mapErrorCodeToMessage = code =>
  ({
    excel_filters_do_not_match: 'Exceli dimensioonid ei klapi kuubi omadega.',
    excel_filter_values_do_not_match: 'Exceli dimensiooni väärtused ei klapi kuubi omadega.'
  }[code]);

export default schema;
