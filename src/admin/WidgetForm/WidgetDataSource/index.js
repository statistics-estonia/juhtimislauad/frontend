import { mdiDelete } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { Button, Form } from 'react-bulma-components';
import { Field, useField } from 'react-final-form';
import Checkbox from '../../../components/Checkbox';
import IconButton from '../../../components/IconButton';
import { InputField, LabeledControl } from '../../../shared/form';
import { useUploadExcel, useDownloadExcel } from '../../WidgetProvider';
import './styles.scss';
import { useFieldArray } from 'react-final-form-arrays';
import { EXCEL_DELETE_CONFIRMATION } from '../../adminConstants';

const WidgetDataSource = () => (
  <>
    <APIDataSource />
    <ExcelDataSource />
  </>
);

const APIDataSource = () => {
  const statDb = useField('statDb');
  const chartTypesField = useFieldArray('graphTypes');
  const checked = !!statDb.input.value;
  const chartTypes = chartTypesField.fields.value;
  const hasCreatedChartTypes = chartTypes?.length > 0;

  const StatDataType = ({ input, ...props }) => {
    const onChange = e => {
      if (hasCreatedChartTypes) {
        window.confirm('API vahetamisel kustuvad kõik loodud graafikutüübid. Kas soovite jätkata?') &&
          setValue(e.target.value);
      } else setValue(e.target.value);
    };
    const setValue = value => {
      if (hasCreatedChartTypes) chartTypesField.fields.removeBatch(chartTypes.map((_, i) => i));
      input.onChange(value);
    };
    return <Form.Radio {...input} {...props} onChange={onChange} />;
  };

  return (
    <>
      <Form.Field>
        <Form.Control>
          <Checkbox checked={checked} onChange={event => statDb.input.onChange(event.target.checked ? {} : undefined)}>
            API
          </Checkbox>
        </Form.Control>
      </Form.Field>

      {statDb.input.value && (
        <>
          <Form.Field kind="group" id="widget-statDataType">
            <Form.Control>
              <Field
                type="radio"
                name="statDb.statDataType"
                value="DATA_API"
                defaultValue="DATA_API"
                component={StatDataType}
              >
                &nbsp;PxWeb
              </Field>
              <Field type="radio" name="statDb.statDataType" value="DATA_SQL" component={StatDataType}>
                &nbsp;PxWebSql
              </Field>
            </Form.Control>
          </Form.Field>
          <LabeledControl name="statDb.cube" label="Kuubi kood" autoComplete="off" component={InputField} />
        </>
      )}
    </>
  );
};

const ExcelDataSource = () => {
  const uploadExcel = useUploadExcel();
  const downloadExcel = useDownloadExcel();
  const field = useField('excel');

  const onFilesChange = async event => {
    if (event.target.files.length === 0) return;

    const formData = new FormData();
    formData.append('file', event.target.files[0]);
    const uploadedFile = await uploadExcel(formData);

    field.input.onChange({
      excelType: 'DATA',
      filename: uploadedFile.fileName,
      id: 0,
      location: uploadedFile.fileDownloadUri,
    });
  };

  return (
    <>
      <Form.Field>
        <Form.Control>
          <Checkbox
            checked={!!field.input.value}
            onChange={({ target: { checked } }) =>
              !checked && !window.confirm(EXCEL_DELETE_CONFIRMATION)
                ? null
                : field.input.onChange(checked ? {} : undefined)
            }
          >
            Excel
          </Checkbox>
        </Form.Control>
      </Form.Field>
      {field.input.value ? (
        field.input.value.filename ? (
          <Form.Field kind="group">
            <Button text size="medium" paddingless onClick={() => downloadExcel(field.input.value)}>
              {field.input.value.filename}
            </Button>
            <IconButton onClick={() => window.confirm(EXCEL_DELETE_CONFIRMATION) && field.input.onChange()}>
              <Icon path={mdiDelete} size={1} />
            </IconButton>
          </Form.Field>
        ) : (
          <>
            <Form.Label>Vali fail</Form.Label>
            <Form.Control>
              <input type="file" onChange={onFilesChange} />
            </Form.Control>
          </>
        )
      ) : null}
    </>
  );
};

export default WidgetDataSource;
