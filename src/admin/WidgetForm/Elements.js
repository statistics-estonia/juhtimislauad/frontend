import * as _ from 'lodash';
import React, { useMemo } from 'react';
import { Field } from 'react-final-form';
import { SelectField } from '../../shared/form';
import { usePromise } from '../../shared/hooks';
import { useGetDashboards } from '../DashboardProvider';

const Elements = () => {
  const getDashboards = useGetDashboards();
  const dashboards = usePromise(async () => (await getDashboards({ elements: true })).content, [], []);

  const elements = useMemo(() => _.flatMapDeep(dashboards, flattenElements), [dashboards]);
  const groupedElements = useMemo(
    () =>
      Object.entries(
        _.groupBy(elements, ({ dashboard: d }) => `${d.parent ? `${d.parent.nameEt} > ` : ''}${d.nameEt}`)
      ).map(([label, options]) => ({ label, options })),
    [elements]
  );

  return (
    <Field
      name="elements"
      component={SelectField}
      options={groupedElements}
      getOptionLabel={o => `${o.levelNameEt} "${o.nameEt}"`}
      getOptionValue={o => o.id}
      selectAllBtn={false}
      isMulti
    />
  );
};

const flattenElements = ({ elements = [], subDashboards = [], ...dashboard }) => [
  ...elements.map(e => ({ ...e, dashboard })),
  ...subDashboards.map(d => ({ ...d, parent: dashboard })).map(flattenElements)
];

export default Elements;
