import { debounce, groupBy, mapValues } from 'lodash';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Notification, Tabs } from 'react-bulma-components';
import { useField } from 'react-final-form';
import { FieldArray, useFieldArray } from 'react-final-form-arrays';
import { useIsMounted } from '../../../shared/hooks';
import { useGetDimensions } from '../../WidgetProvider';
import ChartType, { chartTypeOptions } from './ChartType';
import { toastError } from '../../../notifications';
import { useParams } from 'react-router';
import CustomFilterValueButtons from '../CustomFilterValueButtons/CustomFilterValueButtons';

const useFieldValue = (...args) => useField(...args).input.value;

const WidgetChartTypes = ({ widgetId, customFiltersInUse, widgetHasCustomFilters, onGetWidget }) => {
  const chartTypesField = useFieldArray('graphTypes');
  const chartTypes = chartTypesField.fields.value || [];

  const dimensionsField = useField('dimensions');
  const timePeriodField = useField('timePeriod');

  const cube = useFieldValue('statDb.cube');
  const statDataType = useFieldValue('statDb.statDataType');

  const dimensions = useDimensions(cube, statDataType);
  useEffect(() => dimensionsField.input.onChange(dimensions), [dimensions]);
  useEffect(() => {
    if (timePeriodField.input.value === 'WEEK') {
      const indexesToRemove = [];
      chartTypesField.fields.value.forEach((field, i) => {
        if (field.type !== 'line') indexesToRemove.push(i);
      });
      if (indexesToRemove.length > 0) {
        chartTypesField.fields.removeBatch(indexesToRemove);
        toastError('Nädalapõhiseid andmeid saab lisada ainult joondiagrammile');
      }
    }
  }, [timePeriodField.input.value]);

  const [activeChartType, setActiveChartType] = useActiveChartType(chartTypes);

  const onClickAdd = () =>
    chartTypesField.fields.push({
      defaultType: chartTypes.length === 0,
      verticalRule: 'DYNAMIC',
      filters: dimensions.map(({ nameEt, nameEn, time }) => ({
        nameEt,
        nameEn,
        type: time ? 'AXIS' : null,
        values: [],
        numberOfValues: 2,
      })),
    });

  const filteredChartTypeOptions = useMemo(() => {
    const groupedByType = groupBy(chartTypes, 'value');
    return timePeriodField.input.value === 'WEEK'
      ? [chartTypeOptions.find(option => option.value === 'line')]
      : chartTypeOptions.filter(({ value: type }) => !groupedByType[type]);
  }, [timePeriodField.input.value, chartTypes.map(c => c.type).join('-')]);

  if (!cube || !dimensions || dimensions.length === 0)
    return <Notification>Sisesta andmeallikana andmekuubi kood, et graafikutüüpe lisada.</Notification>;

  const pendingNewChartTypeExists = chartTypes.find(chartType => !chartType.type);
  const canAddNewChartType =
    timePeriodField.input.value === 'WEEK'
      ? chartTypes.length < 1
      : chartTypes.length < 4 && !pendingNewChartTypeExists;

  return (
    <>
      {widgetId && (
        <div className="mb-4">
          <CustomFilterValueButtons
            dimensions={dimensions}
            widgetId={widgetId}
            customFiltersInUse={customFiltersInUse}
            widgetHasCustomFilters={widgetHasCustomFilters}
            onGetWidget={onGetWidget}
          />
        </div>
      )}
      <Tabs>
        {chartTypes.map((chartType, index) => (
          <Tabs.Tab
            key={chartType.type || 'new'}
            active={chartType.type === activeChartType}
            onClick={() => setActiveChartType(chartType.type)}
          >
            {(chartType.type && chartTypeOptionsGroupedByValue[chartType.type].label) || 'Uus graafik'}&nbsp;
            <button
              type="button"
              className="delete"
              onClick={() => confirmDeletion(chartType.type) && chartTypesField.fields.remove(index)}
            />
          </Tabs.Tab>
        ))}
        {canAddNewChartType && (
          <Tabs.Tab type="button" onClick={onClickAdd} disabled={filteredChartTypeOptions.length === 0}>
            Lisa graafikutüüp
          </Tabs.Tab>
        )}
      </Tabs>
      <FieldArray name="graphTypes">
        {({ fields }) =>
          fields.length > 0 &&
          fields.map(
            (name, index) =>
              chartTypes[index] &&
              chartTypes[index].type === activeChartType && (
                <ChartType key={name} name={name} filteredChartTypeOptions={filteredChartTypeOptions} />
              )
          )
        }
      </FieldArray>
    </>
  );
};

const confirmDeletion = type =>
  (type &&
    window.confirm(
      `Oled kindel et soovid kustutada selle graafikutüübi (${chartTypeOptionsGroupedByValue[type].label})?`
    )) ||
  !type;

const chartTypeOptionsGroupedByValue = mapValues(groupBy(chartTypeOptions, 'value'), ([o]) => o);

const useDimensions = (cube, statDataType) => {
  const isMounted = useIsMounted();

  const getDimensions = useGetDimensions();
  const [dimensions, setDimensions] = useState([]);

  const { id } = useParams();
  const updateDimensionsDebounced = useCallback(
    debounce(async (cube, statDataType) => setDimensions(cube && (await getDimensions(cube, statDataType, id))), 500),
    [getDimensions, isMounted]
  );

  useEffect(() => {
    updateDimensionsDebounced(cube, statDataType);
    return updateDimensionsDebounced.cancel;
  }, [cube, statDataType, updateDimensionsDebounced]);

  return dimensions;
};

const useActiveChartType = chartTypes => {
  const prevTypes = React.useRef();
  const [activeChartType, setActiveChartType] = useState(chartTypes[0] && chartTypes[0].type);

  const types = chartTypes.map(c => c.type);

  useEffect(() => {
    if (prevTypes.current) {
      if (prevTypes.current.length < types.length) setActiveChartType(types[types.length - 1]);
      else if (!types.includes(activeChartType))
        setActiveChartType(types[prevTypes.current.findIndex(type => type === activeChartType)]);
    }

    prevTypes.current = types;
  }, [types]);

  return [activeChartType, setActiveChartType];
};

export default WidgetChartTypes;
