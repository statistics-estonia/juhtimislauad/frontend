import { groupBy, mapValues } from 'lodash';
import React, { memo, useMemo } from 'react';
import { Columns, Notification } from 'react-bulma-components';
import { useField } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import { CheckboxField, LabeledControl, PrimitiveSelectField, useFieldValue } from '../../../shared/form';
import Dimension from './Dimension';

const ChartType = memo(function ChartType({ name, filteredChartTypeOptions }) {
  const type = useFieldValue(`${name}.type`);
  const dimensions = useFieldValue('dimensions');

  const filtersField = useField(`${name}.filters`);

  const isRegional = filtersField.input.value.some(f => f.region);
  const dimensionsByNameEt = useMemo(() => mapValues(groupBy(dimensions, 'nameEt'), ([d]) => d), [dimensions]);

  return (
    <>
      <Columns>
        <Columns.Column size={3}>
          <LabeledControl
            label="Graafiku tüüp"
            name={`${name}.type`}
            component={PrimitiveSelectField}
            options={filteredChartTypeOptions}
          />
        </Columns.Column>
        <Columns.Column size={3}>
          <LabeledControl label={<>&nbsp;</>} name={`${name}.defaultType`} type="checkbox" component={CheckboxField}>
            Vaikimisi graafik
          </LabeledControl>
        </Columns.Column>
        {isRegional && type === 'map' && (
          <Columns.Column size={3}>
            <LabeledControl
              label="Kaardi andmete näitamine"
              name={`${name}.mapRule`}
              component={PrimitiveSelectField}
              options={mapRuleOptions}
            />
          </Columns.Column>
        )}
        {isRegional && type === 'vertical' && (
          <Columns.Column size={3}>
            <LabeledControl
              label="Rõhttulpdiagrammi andmete näitamine"
              name="verticalRule"
              component={PrimitiveSelectField}
              options={verticalRuleOptions}
            />
          </Columns.Column>
        )}
      </Columns>
      {type && (
        <FieldArray name={`${name}.filters`}>
          {({ fields }) =>
            fields.map((name, index) => (
              <Dimension
                key={index}
                name={name}
                dimension={dimensionsByNameEt[filtersField.input.value[index].nameEt]}
              />
            ))
          }
        </FieldArray>
      )}
      {filtersField.meta.error &&
        filtersField.meta.error
          .filter(Boolean)
          .filter(error => typeof error === 'string')
          .map(message => (
            <Notification key={message} color="danger">
              {message}
            </Notification>
          ))}
    </>
  );
});

export const chartTypeOptions = [
  { value: 'line', label: 'Joondiagramm' },
  { value: 'bar', label: 'Püsttulpdiagramm' },
  { value: 'stacked', label: 'Summapüsttulpdiagramm' },
  { value: 'area', label: 'Pinddiagramm' },
  { value: 'treemap', label: 'Treemap' },
  { value: 'vertical', label: 'Rõhttulpdiagramm' },
  { value: 'map', label: 'Kaart' },
  { value: 'radar', label: 'Võrkdiagramm' },
  { value: 'pyramid', label: 'Rühmitatud rõhttulpdiagramm' },
  { value: 'pie', label: 'Sektordiagramm' }
];

const mapRuleOptions = [
  { value: 'DYNAMIC', label: 'Dünaamiline' },
  { value: 'KOV', label: 'KOVid' },
  { value: 'MK', label: 'Maakonnad' }
];

const verticalRuleOptions = [{ value: 'DYNAMIC', label: 'Dünaamiline' }, { value: 'ALL', label: 'Kõik' }];

export default ChartType;
