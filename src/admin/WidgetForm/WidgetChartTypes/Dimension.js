import { groupBy } from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import { Button, Columns, Heading, Modal } from 'react-bulma-components';
import Select from '../../../components/Select';

import {
  CheckboxField,
  InputField,
  LabeledControl,
  PrimitiveSelectField,
  SelectField,
  useFieldValue,
} from '../../../shared/form';
import { useField } from 'react-final-form';
import customFilterValueStyles from '../../../components/Select/FilterValueStyling';

const Dimension = memo(function Dimension({ name, dimension }) {
  const type = useFieldValue(`${name}.type`);
  const region = useFieldValue(`${name}.region`);
  const nameEt = useFieldValue(`${name}.nameEt`);
  const nameEn = useFieldValue(`${name}.nameEn`);
  const isTime = useFieldValue(`${name}.time`);
  const timePeriod = useFieldValue('timePeriod');
  const timePart = useField(`${name}.timePart`);
  const values = useFieldValue(`${name}.values`) || [];
  const numberOfValues = useFieldValue(`${name}.numberOfValues`);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filteredTimePartOptions, setFilteredTimePartOptions] = useState(
    timePart.input.value === 'WEEK' ? timePartOptions.filter(part => part.value !== 'WEEK') : timePartOptions
  );
  const allValuesAreCustomValues = values.every(val => val.custom);

  useEffect(() => {
    if (timePeriod !== 'WEEK') {
      if (timePart.input.value === 'WEEK') timePart.input.onChange('');
      setFilteredTimePartOptions(timePartOptions.filter(part => part.value !== 'WEEK'));
    } else setFilteredTimePartOptions(timePartOptions);
  }, [timePeriod]);

  return (
    <div style={{ padding: '1rem', marginBottom: '1rem' }} className="has-background-light">
      <Heading size={5}>
        {nameEt} <small>({nameEn})</small>
      </Heading>
      <Columns>
        <Columns.Column size={3}>
          <LabeledControl
            label="Nimi eesti k."
            name={`${name}.displayNameEt`}
            defaultValue={nameEt}
            component={InputField}
          />
        </Columns.Column>
        <Columns.Column size={3}>
          <LabeledControl
            label="Nimi inglise k."
            name={`${name}.displayNameEn`}
            defaultValue={nameEn}
            component={InputField}
          />
        </Columns.Column>
        <Columns.Column size={3}>
          <LabeledControl
            label="Dimensiooni laad"
            name={`${name}.type`}
            component={PrimitiveSelectField}
            options={filterTypeOptions}
            isClearable
          />
          {type === 'TIME_PART' && (
            <LabeledControl
              label="Kuupäeva osa tüüp"
              name={`${name}.timePart`}
              component={PrimitiveSelectField}
              options={filteredTimePartOptions}
            />
          )}
          {type === 'AXIS' && !isTime && (
            <LabeledControl
              label="Telje sorteerimine"
              name={`${name}.axisOrder`}
              component={PrimitiveSelectField}
              options={axisOrderOptions}
            />
          )}
          {type === 'MENU' && (
            <LabeledControl
              label="Lubatud filtriväärtuste arv"
              name={`${name}.numberOfValues`}
              type="number"
              component={InputField}
            />
          )}
        </Columns.Column>
        <Columns.Column size={3}>
          <LabeledControl label="Andmetüüp" name={`${name}.time`} type="checkbox" component={CheckboxField}>
            Ajaline
          </LabeledControl>
          <LabeledControl name={`${name}.region`} type="checkbox" component={CheckboxField}>
            Piirkondlik
          </LabeledControl>
        </Columns.Column>
        <Columns.Column size={12}>
          {type !== 'TIME_PART' && !isTime && (
            <>
              <LabeledControl
                label="Filtri väärtused"
                name={`${name}.values`}
                component={SelectField}
                options={dimension && dimension.values}
                getOptionValue={o => o.valueEt}
                getOptionLabel={o => o.valueEt}
                isMulti
                styles={customFilterValueStyles}
              />
              {!region && (type === 'MENU' || type === 'LEGEND') && !allValuesAreCustomValues && (
                <Button
                  className="is-secondary"
                  type="button"
                  onClick={() => {
                    setIsModalOpen(true);
                  }}
                >
                  Muuda filtri väärtuste nimetusi
                </Button>
              )}
              <Modal show={isModalOpen} onClose={() => setIsModalOpen(false)}>
                <Modal.Card>
                  <Modal.Card.Head onClose={() => setIsModalOpen(false)}>
                    <Modal.Card.Title>Muuda kuvanimesid / Muuda filtri nimesid</Modal.Card.Title>
                  </Modal.Card.Head>
                  <Modal.Card.Body>
                    {values.map(
                      ({ id, valueEt, valueEn, displayValueEt, displayValueEn, custom: isCustomValue }, i) => {
                        if (!isCustomValue) {
                          return (
                            <React.Fragment key={`${valueEt}-${id}`}>
                              <LabeledControl
                                label={`ET: ${valueEt}`}
                                name={`${name}.values[${i}].displayValueEt`}
                                component={InputField}
                                initialValue={displayValueEt}
                                placeholder="Sisesta uus kuvaväärtus"
                              />
                              <LabeledControl
                                label={`EN: ${valueEn}`}
                                name={`${name}.values[${i}].displayValueEn`}
                                initialValue={displayValueEn}
                                component={InputField}
                                placeholder="Enter new display value"
                              />
                            </React.Fragment>
                          );
                        }
                      }
                    )}
                  </Modal.Card.Body>
                  <Modal.Card.Foot>
                    <div className="buttons">
                      <Button onClick={() => setIsModalOpen(false)} className="is-primary">
                        Valmis
                      </Button>
                    </div>
                  </Modal.Card.Foot>
                </Modal.Card>
              </Modal>
            </>
          )}
          {type === 'MENU' && !isTime && (
            <LabeledControl label="Vaikeväärtused" name={`${name}.values`}>
              {({ input }) => (
                <Select
                  {...input}
                  value={(input.value || []).filter(v => v.selected)}
                  options={input.value || []}
                  getOptionValue={o => o.valueEt}
                  getOptionLabel={o => o.valueEt}
                  onChange={values => {
                    const groupedByValue = groupBy(values.slice(0, numberOfValues), 'valueEt');
                    input.onChange(input.value.map(v => ({ ...v, selected: Boolean(groupedByValue[v.valueEt]) })));
                  }}
                  isMulti
                  styles={customFilterValueStyles}
                />
              )}
            </LabeledControl>
          )}
        </Columns.Column>
      </Columns>
    </div>
  );
});

const axisOrderOptions = [
  { value: 'VALUE_ASC', label: 'Väärtuse järgi kasvavalt' },
  { value: 'VALUE_DESC', label: 'Väärtuse järgi kahanevalt' },
  { value: 'KEEP_AXIS', label: 'Sorteerimata' },
];

const filterTypeOptions = [
  { value: 'AXIS', label: 'Telg' },
  { value: 'MENU', label: 'Filter' },
  { value: 'LEGEND', label: 'Legend' },
  { value: 'TIME_PART', label: 'Kuupäeva osa' },
  { value: 'CONSTRAINT', label: 'Kitsendaja' },
];

const timePartOptions = [
  { value: 'MONTH', label: 'Kuu' },
  { value: 'QUARTER', label: 'Kvartal' },
  { value: 'WEEK', label: 'Nädal' },
];

export default Dimension;
