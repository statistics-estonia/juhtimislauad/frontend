import React, { useEffect } from 'react';
import { Heading } from 'react-bulma-components';
import CustomFilterForm from './CustomFilterForm';
import { useCreateNewFilterValue } from '../WidgetProvider';
import { useHistory } from 'react-router-dom';
import './CustomFilter.scss';

const NewCustomFilter = () => {
  const history = useHistory();
  const { widgetId, dimensions } = history.location;
  const createCustomFilterValue = useCreateNewFilterValue();

  useEffect(() => {
    if (!widgetId) {
      history.goBack();
    }
  }, []);

  const handleCancel = () => {
    history.goBack();
  };

  const handleSubmit = customFilter => {
    const requestBody = {
      displayNameEt: customFilter.displayNameEt,
      displayNameEn: customFilter.displayNameEn,
      dimension1NameEt: customFilter.dimensionOne.nameEt,
      dimension1NameEn: customFilter.dimensionOne.nameEn,
      selectionValue1: customFilter.selectionOne.statId,
      dimension2NameEt: customFilter.dimensionTwo?.nameEt ?? null,
      dimension2NameEn: customFilter.dimensionTwo?.nameEn ?? null,
      selectionValue2: customFilter.selectionTwo.statId,
      operation: customFilter.operation,
    };

    createCustomFilterValue({ widgetId: widgetId, newFilter: requestBody }).then(() => {
      history.goBack();
    });
  };

  return (
    <>
      <Heading size={3}>Loo uus filtri väärtus</Heading>
      <CustomFilterForm dimensions={dimensions} onCancel={handleCancel} onSubmit={handleSubmit} />
    </>
  );
};

export default NewCustomFilter;
