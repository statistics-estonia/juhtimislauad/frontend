import React from 'react';
import { Translate } from '../i18n/LanguageProvider';
import Table from '../components/Table';
import { Heading } from 'react-bulma-components';
import { useGetProcess, intlConfig } from './ProcessLogProvider';
import { usePromise } from '../shared/hooks';
import { FormattedDate } from 'react-intl';

const ProcessDetails = ({ match, history }) => {
  const getProcessDetails = useGetProcess();
  const { lang } = match.params;
  const processDetails = usePromise(
    async () => (await getProcessDetails(match.params.id)).content,
    [getProcessDetails],
    []
  );

  const filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    if (row[id] !== null) {
      return row[id] !== undefined
        ? String(row[id])
            .toLowerCase()
            .includes(filter.value.toLowerCase())
        : true;
    }
  };

  return (
    <div>
      <Heading size={2}>Protsessi info</Heading>
      <Table
        data={processDetails}
        columns={[
          {
            Header: <Translate>Aeg</Translate>,
            accessor: 'time',
            Cell: ({ value }) => <FormattedDate value={value} {...intlConfig} />
          },
          {
            Header: <Translate>Nimetus</Translate>,
            accessor: 'widget',
            style: { whiteSpace: 'unset' },
            Cell: ({ value }) => <span>{value.shortnameEt}</span>
          },
          {
            Header: <Translate>Kuubi kood</Translate>,
            accessor: 'cube'
          },
          {
            Header: <Translate>Staatus</Translate>,
            accessor: 'status',
            filterable: true,
            filterMethod: filterCaseInsensitive
          },
          {
            Header: <Translate>Kommentaar</Translate>,
            accessor: 'message',
            style: { whiteSpace: 'unset' }
          }
        ]}
        showPagination={false}
        defaultSortDesc={true}
        pageSize={processDetails.length}
        getTrProps={(state, rowInfo) => ({
          onClick: () => history.push(`/${lang}/admin/widgets/${rowInfo.original.widget.id}`),
          style: { cursor: 'pointer' }
        })}
        loading={processDetails.length === 0}
        loadingText="Laadin andmeid"
        noDataText="Kirjed puuduvad"
      />
    </div>
  );
};

export default ProcessDetails;
