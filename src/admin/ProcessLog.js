import React from 'react';
import { useGetProcessLog, intlConfig } from './ProcessLogProvider';
import { usePromise } from '../shared/hooks';
import Table from '../components/Table';
import { Translate, useActiveLanguageCode } from '../i18n/LanguageProvider';
import { Heading } from 'react-bulma-components';
import { FormattedDate } from 'react-intl';

const ProcessLog = ({ history }) => {
  const getProcessLog = useGetProcessLog();
  const lang = useActiveLanguageCode();
  const processLog = usePromise(async () => (await getProcessLog()).content, [getProcessLog], []);

  return (
    <div>
      <Heading size={2}>Protsesside logi</Heading>
      <Table
        data={processLog}
        columns={[
          {
            Header: <Translate>Protsessi algus - lõpp</Translate>,
            accessor: 'startTime',
            style: { whiteSpace: 'unset' },
            Cell: DateCell
          },
          {
            Header: <Translate>Nimetus</Translate>,
            accessor: 'process',
            style: { whiteSpace: 'unset' }
          },
          {
            Header: <Translate>Staatus</Translate>,
            id: 'status',
            accessor: row => statuses.filter(s => s.code === row.status)[0].status,
            sortable: true
          },
          {
            Header: <Translate>Analüüsitud</Translate>,
            accessor: 'analysed'
          },
          {
            Header: <Translate>Muutmata</Translate>,
            accessor: 'unchanged'
          },
          {
            Header: <Translate>Muudetud</Translate>,
            accessor: 'changed'
          },
          {
            Header: <Translate>Ebaõnnestunud</Translate>,
            accessor: 'failed'
          }
        ]}
        showPagination={false}
        pageSize={processLog.length > 0 ? processLog.length : 3}
        defaultSortDesc={true}
        getTrProps={(state, rowInfo) => ({
          onClick: () => history.push(`/${lang}/admin/processlog/${rowInfo.original.id}`),
          style: { cursor: 'pointer' }
        })}
        loading={processLog.length === 0}
        loadingText="Laadin andmeid"
        noDataText="Kirjed puuduvad"
      />
    </div>
  );
};

const DateCell = ({ original: { startTime, endTime } }) => {
  const dateValue = value => (value ? <FormattedDate value={value} {...intlConfig} /> : '');
  return (
    <span>
      {dateValue(startTime)} - {dateValue(endTime)}
    </span>
  );
};

const statuses = [
  { code: 'FINISHED', status: 'Lõpetatud' },
  { code: 'STARTED', status: 'Alustatud' },
  { code: 'ERROR', status: 'Viga' }
];

export default ProcessLog;
