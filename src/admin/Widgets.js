import { mdiDelete, mdiMagnify } from '@mdi/js';
import Icon from '@mdi/react';
import { debounce } from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';
import { Button } from 'react-bulma-components';
import { Columns } from 'react-bulma-components';
import { Form } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { Link } from 'react-router-dom';
import IconButton from '../components/IconButton';
import Table from '../components/Table';
import Checkbox from '../components/Checkbox';
import { useIsMounted } from '../shared/hooks';
import { useDeleteWidget, useGetWidgets } from './WidgetProvider';
import { SOURCES, QUERY_TYPES } from './adminConstants';
import { useActiveLanguageCode } from '../i18n/LanguageProvider';

const Widgets = () => {
  const deleteWidget = useDeleteWidget();
  const [widgets, refreshWidgets, widgetsLoading] = useWidgets();
  const lang = useActiveLanguageCode();
  const onDelete = async widget => {
    if (!confirmDeletion(widget.nameEt)) return;

    await deleteWidget(widget.id);
    refreshWidgets();
  };

  return (
    <div>
      <Columns>
        <Columns.Column size="half">
          <Heading size={2}>
            Kuvamoodulid
            <Link to={`/${lang}/admin/widgets/new`}>
              <Button className="btn-custom-margin is-primary is-outlined" style={{ marginLeft: '30px' }}>
                Lisa uus
              </Button>
            </Link>
          </Heading>
        </Columns.Column>
        <Columns.Column>
          <Search refreshWidgets={refreshWidgets} />
        </Columns.Column>
      </Columns>
      <Table
        data={widgets}
        columns={[
          {
            Header: 'Kuvamooduli nimi',
            accessor: 'shortnameEt',
            style: { whiteSpace: 'unset' },
            Cell: props => <NameCell {...props} lang={lang} />,
          },
          {
            Header: 'Juhtimislaud',
            id: 'dashboards',
            accessor: row =>
              row.dashboards.map(dashboard => (dashboard.parent ? dashboard.parent.nameEt : dashboard.nameEt)).sort(),
            Cell: DashboardCell,
            sortMethod: compareDashboards,
          },
          {
            Header: 'Kuubikood',
            accessor: 'cube',
          },
          {
            Header: 'Staatus',
            accessor: 'status',
            Cell: StatusCell,
          },
          {
            Header: 'Andmeallikad',
            accessor: 'sources',
            Cell: SourcesCell,
          },
          {
            Cell: props => <ActionsCell {...props} onDelete={onDelete} />,
            sortable: false,
          },
        ]}
        showPagination={false}
        pageSize={widgets && widgets.length > 0 ? widgets.length : 3}
        sortable={true}
        loading={widgetsLoading}
        loadingText="Laadin andmeid"
        noDataText="Kirjed puuduvad"
      />
    </div>
  );
};

const useWidgets = () => {
  const getWidgets = useGetWidgets();
  const [widgets, setWidgets] = useState([]);
  const [widgetsLoading, setWidgetsLoading] = useState(false);

  const isMounted = useIsMounted();
  const refreshWidgets = useCallback(
    async (queries, sources) => {
      setWidgetsLoading(true);
      const { content } = await getWidgets({ size: 1000, ...queries, sources });
      setWidgetsLoading(false);
      return isMounted() && setWidgets(content);
    },
    [getWidgets]
  );

  return [widgets, refreshWidgets, widgetsLoading];
};

const Search = ({ refreshWidgets }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [queryTypes, setQueryTypes] = useState({
    [QUERY_TYPES.WIDGET.DEV]: true,
    [QUERY_TYPES.DASHBOARD.DEV]: true,
    [QUERY_TYPES.CUBE.DEV]: true,
  });
  const [sources, setSources] = useState({
    [SOURCES.NONE.DEV]: true,
    [SOURCES.DATA_API.DEV]: true,
    [SOURCES.DATA_SQL.DEV]: true,
    [SOURCES.EXCEL.DEV]: true,
  });

  const refreshDebounced = useCallback(debounce(refreshWidgets, 300), [refreshWidgets]);

  useEffect(() => {
    if (!searchQuery.length || searchQuery.length > 2) {
      const selectedSources = Object.keys(sources).filter(id => sources[id]);
      const queries = {};
      for (let type in queryTypes) {
        if (queryTypes[type]) {
          queries[type] = searchQuery;
        }
      }
      refreshDebounced(queries, selectedSources);
    }
    return refreshDebounced.cancel;
  }, [searchQuery, sources, queryTypes]);

  const handleSourceChanged = e => {
    setSources({ ...sources, [e.target.name]: e.target.checked });
  };

  const handleQueryTypeChanged = e => {
    setQueryTypes({ ...queryTypes, [e.target.name]: e.target.checked });
  };

  return (
    <>
      <Columns marginless>
        <Columns.Column marginless>
          <Form.Control iconRight>
            <Form.Input
              placeholder="Vähemalt 3 tähemärki"
              autoComplete="off"
              value={searchQuery}
              onChange={e => setSearchQuery(e.target.value)}
            />
            <span className="icon is-right">
              <Icon path={mdiMagnify} size={1.5} color="#dbdbdb" />
            </span>
          </Form.Control>
        </Columns.Column>
      </Columns>
      <Columns className="admin-search-block" marginless>
        <Columns.Column narrow>
          <b>Otsi veergudelt:</b>
        </Columns.Column>
        {Object.values(QUERY_TYPES).map(({ DEV, ET }, key) => (
          <Columns.Column narrow key={key}>
            <Checkbox name={DEV} checked={queryTypes[DEV]} onChange={handleQueryTypeChanged}>
              {ET}
            </Checkbox>
          </Columns.Column>
        ))}
      </Columns>
      <Columns className="admin-search-block" marginless>
        <Columns.Column narrow>
          <b>Vali andmeallikad:</b>
        </Columns.Column>
        {Object.values(SOURCES).map(({ DEV, ET }, key) => (
          <Columns.Column narrow key={key}>
            <Checkbox name={DEV} checked={sources[DEV]} onChange={handleSourceChanged}>
              {ET}
            </Checkbox>
          </Columns.Column>
        ))}
      </Columns>
    </>
  );
};

const confirmDeletion = name => window.confirm(`Oled kindel, et soovid kustutada "${name}"?`);

const NameCell = ({ value, original: widget, lang }) => <Link to={`/${lang}/admin/widgets/${widget.id}`}>{value}</Link>;

const DashboardCell = ({ value }) => (
  <div>
    {value.map(dashboard => (
      <p key={dashboard}>{dashboard}</p>
    ))}
  </div>
);

const ActionsCell = ({ original: widget, onDelete }) => (
  <IconButton id="delete-widget-btn" onClick={() => onDelete(widget)}>
    <Icon path={mdiDelete} size={1} />
  </IconButton>
);

const StatusCell = ({ value }) => <span>{value === 'VISIBLE' ? 'nähtav' : 'peidetud'}</span>;

const SourcesCell = ({ value = [] }) => {
  if (!value.length) return 'puudub';
  return value.map(source => SOURCES[source].ET).join(', ');
};

const compareDashboards = (a, b) => {
  a = a === null || a === undefined || a.length === 0 ? '' : a[0];
  b = b === null || b === undefined || b.length === 0 ? '' : b[0];

  a = typeof a === 'string' ? a.toLowerCase() : a;
  b = typeof b === 'string' ? b.toLowerCase() : b;
  if (a === b) return 0;

  return a.localeCompare(b, 'et');
};

export default Widgets;
