import { useCallback } from 'react';
import { useFetch } from '../api/ApiProvider';

export const useGetDashboards = () => {
  const fetch = useFetch();
  return useCallback(
    query =>
      fetch('/admin/dashboard', {
        queryParams: query,
        errorNotification: 'Juhtimislaudade pärimine ebaõnnestus',
      }),
    [fetch]
  );
};

export const useGetRoles = () => {
  const fetch = useFetch();
  return useCallback(
    query =>
      fetch('/classifier/roles', {
        queryParams: query,
        errorNotification: 'Juhtimislaudade pärimine ebaõnnestus',
      }),
    [fetch]
  );
};

export const useGetDashboard = () => {
  const fetch = useFetch();
  return useCallback(
    dashboardId =>
      fetch(`/admin/dashboard/${dashboardId}`, {
        errorNotification: 'Juhtimislaua pärimine ebaõnnestus',
      }),
    [fetch]
  );
};

export const useDeleteDashboard = () => {
  const fetch = useFetch();
  return useCallback(
    dashboardId =>
      fetch(`/admin/dashboard/${dashboardId}`, {
        method: 'DELETE',
        errorNotification: 'Juhtimislaua kustutamine ebaõnnestus',
        successNotification: 'Juhtimislaua kustutamine õnnestus',
      }),
    [fetch]
  );
};

export const useCreateDashboard = () => {
  const fetch = useFetch();
  return useCallback(
    dashboard =>
      fetch('/admin/dashboard', {
        body: dashboard,
        errorNotification: 'Juhtimislaua loomine ebaõnnestus',
        successNotification: 'Juhtimislaua loomine õnnestus',
      }),
    [fetch]
  );
};

export const useUpdateDashboard = () => {
  const fetch = useFetch();
  return useCallback(
    dashboard =>
      fetch(`/admin/dashboard/${dashboard.id}`, {
        method: 'PUT',
        body: dashboard,
        errorNotification: 'Juhtimislaua salvestamine ebaõnnestus',
        successNotification: 'Juhtimislaua salvestamine õnnestus',
      }),
    [fetch]
  );
};

export const useGetRegions = () => {
  const fetch = useFetch();
  return useCallback(
    () =>
      fetch('/element?clfCode=EHAK', {
        errorNotification: 'Piirkondade pärimine ebaõnnestus',
      }),
    [fetch]
  );
};
