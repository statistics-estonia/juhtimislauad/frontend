import React from 'react';
import { Columns, Section } from 'react-bulma-components';
import { IntlProvider } from 'react-intl';
import { Route, Switch } from 'react-router';
import Header from '../components/Header';
import Sidenav from '../components/Sidenav';
import AdminHome from './AdminHome';
import Dashboards from './Dashboards';
import EditDashboard from './EditDashboard';
import EditWidget from './EditWidget';
import NewDashboard from './NewDashboard';
import NewWidget from './NewWidget';
import ProcessDetails from './ProcessDetails';
import ProcessLog from './ProcessLog';
import Widgets from './Widgets';
import CustomFilterForm from './CustomFilterForm/NewCustomFilter';

const Admin = () => (
  <IntlProvider locale="et">
    <>
      <Header />
      <Columns>
        <Columns.Column size={2} className="is-hidden-mobile">
          <Sidenav />
        </Columns.Column>
        <Columns.Column size={10}>
          <Section>
            <Switch>
              <Route path="/:lang/admin/dashboards/new" component={NewDashboard} />
              <Route path="/:lang/admin/dashboards/:id" component={EditDashboard} />
              <Route path="/:lang/admin/dashboards" component={Dashboards} />
              <Route path="/:lang/admin/filtervalue/new" component={CustomFilterForm} />
              <Route path="/:lang/admin/widgets/new" component={NewWidget} />
              <Route path="/:lang/admin/widgets/:id" component={EditWidget} />
              <Route path="/:lang/admin/widgets" component={Widgets} />
              <Route path="/:lang/admin/processlog/:id" component={ProcessDetails} />
              <Route path="/:lang/admin/processlog" component={ProcessLog} />
              <Route path="/:lang/admin" component={AdminHome} />
            </Switch>
          </Section>
        </Columns.Column>
      </Columns>
    </>
  </IntlProvider>
);

export default Admin;
