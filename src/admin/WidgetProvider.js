import { useCallback } from 'react';
import { useFetch } from '../api/ApiProvider';
import { useToken } from '../auth/TokenProvider';
import { downloadAsFile } from '../export/utils';
import { toastError } from '../notifications';

export const useGetWidgets = () => {
  const fetch = useFetch();
  return useCallback(
    query => fetch('/admin/widget', { queryParams: query, errorNotification: 'Kuvamoodulite laadimine ebaõnnestus' }),
    [fetch]
  );
};

export const useGetWidget = () => {
  const fetch = useFetch();
  return useCallback(
    widgetId =>
      fetch(`/admin/widget/${widgetId}`, {
        errorNotification: 'Kuvamooduli pärimine ebaõnnestus',
      }),
    [fetch]
  );
};

export const useDeleteWidget = () => {
  const fetch = useFetch();
  return useCallback(
    widgetId =>
      fetch(`/admin/widget/${widgetId}`, {
        method: 'DELETE',
        errorNotification: 'Kuvamooduli kustutamine ebaõnnestus',
        successNotification: 'Kuvamooduli kustutamine õnnestus',
      }),
    [fetch]
  );
};

export const useCreateWidget = () => {
  const fetch = useFetch();
  return useCallback(
    widget =>
      fetch('/admin/widget', {
        body: widget,
        errorNotification: 'Kuvamooduli loomine ebaõnnestus',
        warningNotification: 'Kuvamooduli metaandmed on salvestunud, aga graafiku andmete ümberarvutamisel tekkis viga',
        successNotification: 'Kuvamooduli loomine õnnestus',
      }),
    [fetch]
  );
};

export const useCreateNewFilterValue = () => {
  const fetch = useFetch();
  return useCallback(
    ({ widgetId, newFilter }) =>
      fetch(`/admin/widget/${widgetId}/filter-value-custom`, {
        method: 'POST',
        body: newFilter,
        errorNotification: 'Uue filtri väärtuse loomine ebaõnnestus',
        successNotification: 'Uue filtri väärtuse loomine õnnestus',
      }),
    [fetch]
  );
};

export const useDeleteAllCustomFilterValues = () => {
  const fetch = useFetch();
  return useCallback(
    widgetId =>
      fetch(`/admin/widget/${widgetId}/filter-value-custom`, {
        method: 'DELETE',
        errorNotification: 'Filtri väärtuste kustutamine ebaõnnestus',
        successNotification: 'Filtri väärtuste kustutamine õnnestus',
      }),
    [fetch]
  );
};

export const useUpdateWidget = () => {
  const fetch = useFetch();
  return useCallback(
    widget =>
      fetch(`/admin/widget/${widget.id}`, {
        method: 'PUT',
        body: widget,
        errorNotification: 'Kuvamooduli uuendamine ebaõnnestus',
        warningNotification: 'Kuvamooduli metaandmed on salvestunud, aga graafiku andmete ümberarvutamisel tekkis viga',
        successNotification: 'Kuvamooduli uuendamine õnnestus',
      }),
    [fetch]
  );
};

export const useGetDimensions = () => {
  const fetch = useFetch();
  return useCallback(
    (cube, type, widgetId) => {
      const requestPath = widgetId
        ? `/admin/stat/${cube}/dimensions?type=${type}&widgetId=${widgetId}`
        : `/admin/stat/${cube}/dimensions?type=${type}`;
      return fetch(requestPath, {
        errorNotification: 'Kuubi dimensioonide pärimine ebaõnnestus',
      });
    },
    [fetch]
  );
};

export const useUploadExcel = () => {
  const fetch = useFetch();
  return useCallback(
    formData =>
      fetch(`/admin/excel/upload`, {
        body: formData,
        errorNotification: 'Exceli üleslaadimine ebaõnnestus',
      }),
    [fetch]
  );
};

export const useDownloadExcel = () => {
  const [token] = useToken();
  const headers = {
    authorization: `Basic ${token}`,
    'content-type': 'application/octet-stream',
  };
  return ({ filename, excelType }) => {
    const encodedFilename = encodeURIComponent(filename);
    return fetch(
      `${process.env.REACT_APP_API_BASE_URL}/admin/excel/download/${encodedFilename}?excelType=${excelType}`,
      { headers }
    )
      .then(async res => {
        if (!res.ok) return Promise.reject();
        const blob = await res.blob();
        downloadAsFile(blob, filename);
      })
      .catch(() => {
        toastError('Exceli allalaadimine ebaõnnestus');
      });
  };
};
