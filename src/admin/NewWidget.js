import { FORM_ERROR } from 'final-form';
import React from 'react';
import { Heading } from 'react-bulma-components';
import { withRouter } from 'react-router';
import WidgetForm from './WidgetForm';
import { mapErrorCodeToMessage } from './WidgetForm/schema';
import { useCreateWidget } from './WidgetProvider';
import { toastError } from '../notifications';

const newWidget = {
  dashboards: [],
  elements: [],
  graphTypes: [],
  nameEn: 'New widget name in english',
  nameEt: 'Uue kuvamooduli pealkiri eesti keeles',
  shortnameEn: 'New widget short name in english',
  shortnameEt: 'Uue kuvamooduli lühipealkiri eesti keeles',
  status: 'HIDDEN',
  timePeriod: 'YEAR',
};

const NewWidget = withRouter(({ history, match }) => {
  const { lang } = match.params;
  const createWidget = useCreateWidget();
  const onSubmit = widget =>
    createWidget(widget).then(
      createdWidget => history.replace(`/${lang}/admin/widgets/${createdWidget.id}`),
      response =>
        response.json().then(error => {
          if (error.code === 'widget_admin_week_is_restricted') {
            toastError('Nädalapõhiseid andmeid saab lisada ainult joondiagrammile');
          }
          return {
            [FORM_ERROR]: error.code && [mapErrorCodeToMessage(error.code)],
          };
        })
    );

  return (
    <>
      <Heading size={3}>Lisa uus kuvamoodul</Heading>
      <WidgetForm value={newWidget} onSubmit={onSubmit} />
    </>
  );
});

export default NewWidget;
