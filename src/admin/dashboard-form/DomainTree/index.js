import React from 'react';
import { wrapToArray } from '../../../shared/utils';
import DomainNode from './DomainNode';
import TreeContext from './TreeContext';
import { levelNames } from './types';

export const DomainTree = ({ onChange, rootDomain = { subElements: [] } }) => {
  const [id, setId] = React.useState(1);

  const getNextId = () => {
    setId(id => id + 1);
    return id;
  };

  const onMove = (from, to) => onChange(prevState => moveWidget(prevState, from, to));
  const onDomainMove = (from, to) => onChange(prevState => moveDomain(prevState, from, to));
  const onCreateDomain = path => onChange(prevState => createDomain(prevState, path, getNextId()));
  const onDeleteDomain = path => onChange(prevState => deleteDomain(prevState, path));
  const onAddWidgets = (widget, path) =>
    onChange(prevState => addWidgetToPath(prevState, path, ...wrapToArray(widget)));
  const onDeleteWidget = path => onChange(prevState => removeWidgetByPath(prevState, path));
  const onDomainChange = (updater, path) => onChange(prevState => updateDomain(prevState, updater, path));

  const contextValue = {
    onMove,
    onDomainMove,
    onCreateDomain,
    onDeleteDomain,
    onAddWidgets,
    onDeleteWidget,
    onDomainChange
  };

  return (
    <TreeContext.Provider value={contextValue}>
      <DomainNode root domain={rootDomain} onMove={onMove} onDomainMove={onDomainMove} />
    </TreeContext.Provider>
  );
};

const createDomain = (domain, path, id) =>
  updateDomain(domain, path, ({ subElements = [], ...prevState } = {}) => ({
    ...prevState,
    subElements: [
      ...subElements,
      {
        levelNameEt: levelNames[path.length][0],
        nameEt: `Uus valdkond ${id}`,
        nameEn: `New domain ${id}`,
        id: `$${id}`
      }
    ]
  }));

const deleteDomain = (domain, path) =>
  updateDomain(domain, path.slice(0, -1), ({ subElements = [], ...prevState }) => ({
    ...prevState,
    subElements: subElements.filter((subDomain, index) => index !== path[path.length - 1])
  }));

const moveDomain = (domain, from, to) => {
  const fromDomain = getDomainByPath(domain, from);
  const emptiedTree = removeDomainByPath(domain, from);
  const addedTree = addDomainToPath(emptiedTree, to, fromDomain);

  return addedTree;
};

const getDomainByPath = (domain, path) => getByPath(domain, path, 'subElements');
const removeDomainByPath = (domain, path) => removeFromPath(domain, path, 'subElements');
const addDomainToPath = (domain, path, ...domains) => addToPath(domain, path, 'subElements', ...domains);

const moveWidget = (domain, from, to) => {
  const fromWidget = getWidgetByPath(domain, from);
  const emptiedTree = removeWidgetByPath(domain, from);
  const addedTree = addWidgetToPath(emptiedTree, to, fromWidget);

  return addedTree;
};

const getWidgetByPath = (domain, path) => getByPath(domain, path, 'widgets');
const removeWidgetByPath = (domain, path) => removeFromPath(domain, path, 'widgets');
const addWidgetToPath = (domain, path, ...widgets) => addToPath(domain, path, 'widgets', ...widgets);

const getByPath = (domain, path, property) => {
  if (path.length === 1) return domain[property][path[0]];

  return getByPath(domain.subElements[path[0]], path.slice(1), property);
};

const removeFromPath = (domain, path, property) =>
  updateDomain(domain, path.slice(0, -1), prevState => ({
    ...prevState,
    [property]: prevState[property].filter((item, index) => index !== path[path.length - 1])
  }));

const addToPath = (domain, path, property, ...items) =>
  updateDomain(domain, path.slice(0, -1), prevState => ({
    ...prevState,
    [property]: [
      ...(prevState[property] || []).slice(0, path[path.length - 1]),
      ...items,
      ...(prevState[property] || []).slice(path[path.length - 1])
    ]
  }));

const updateDomain = (domain, path, updater) => {
  if (path.length === 0) return updater(domain);

  return {
    ...domain,
    subElements: domain.subElements.map((subDomain, index) =>
      index === path[0] ? updateDomain(subDomain, path.slice(1), updater) : subDomain
    )
  };
};
