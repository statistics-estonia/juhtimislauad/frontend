import { mdiDelete } from '@mdi/js';
import Icon from '@mdi/react';
import { flow } from 'lodash';
import React from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import IconButton from '../../../../components/IconButton';
import TreeContext from '../TreeContext';
import { widgetType } from '../types';
import './styles.scss';

const widgetDragSource = DragSource(
  widgetType,
  {
    beginDrag: props => ({
      id: props.id,
      path: props.path
    }),
    isDragging: (props, monitor) => props.id === monitor.getItem().id
  },
  (connect, monitor, props) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  })
);

const widgetDropTarget = DropTarget(
  widgetType,
  {
    hover: (hovered, monitor) => {
      const dragged = monitor.getItem();

      if (dragged.path.join() === hovered.path.join()) return;

      if (typeof hovered.onMove === 'function') {
        hovered.onMove(dragged.path, hovered.path);
        monitor.getItem().path = hovered.path;
      }
    }
  },
  connect => ({
    connectDropTarget: connect.dropTarget()
  })
);

const dnd = flow(
  widgetDropTarget,
  widgetDragSource
);

const WidgetNode = dnd(({ shortname, shortnameEt, path, isDragging, connectDragSource, connectDropTarget }) => {
  const { onDeleteWidget } = React.useContext(TreeContext);

  const ref = React.useRef();
  connectDragSource(ref);
  connectDropTarget(ref);

  const onClickDelete = () => onDeleteWidget(path);

  return (
    <div
      ref={ref}
      className="widget-node"
      style={{
        opacity: isDragging ? 0.2 : 1
      }}
    >
      {shortnameEt || shortname}
      <IconButton onClick={onClickDelete}>
        <Icon path={mdiDelete} size={1} />
      </IconButton>
    </div>
  );
});

export default WidgetNode;
