import React from 'react';
import { AsyncSelect } from '../../../../components/Select';
import { SOURCES } from '../../../adminConstants';
import { useGetWidgets } from '../../../WidgetProvider';
import TreeContext from '../TreeContext';

const AddWidgets = ({ path, widgets }) => {
  const getWidgets = useGetWidgets();
  const { onAddWidgets } = React.useContext(TreeContext);

  const loadOptions = React.useCallback(
    async inputValue =>
      inputValue.length > 1
        ? (await getWidgets({ queryForWidget: inputValue, sources: Object.values(SOURCES).map(e => e.DEV) })).content
        : [],
    [getWidgets]
  );

  return (
    <AsyncSelect
      cacheOptions
      loadOptions={loadOptions}
      value={[]}
      components={{ Option }}
      onChange={widget => onAddWidgets(widget, [...path, widgets.length])}
      filterOption={option => widgets.every(existingWidget => option.value !== existingWidget.id)}
      getOptionValue={widget => widget.id}
      getOptionLabel={widget => widget.shortnameEt}
      isSearchable
      autoFocus
    />
  );
};

const Option = props => {
  const { label, data: widget, getStyles, selectOption } = props;
  const styles = getStyles('option', props);

  return (
    <div style={styles} onClick={() => selectOption(widget)}>
      <div>{label}</div>
      <div>
        <small>
          <strong>ID: </strong>
          {widget.id}
        </small>
      </div>
      {widget.cube && (
        <div>
          <small>
            <strong>Kuup:</strong> {widget.cube}
          </small>
        </div>
      )}
      <div>
        <small>{widget.nameEt}</small>
      </div>
    </div>
  );
};

export default AddWidgets;
