import { flow } from 'lodash';
import React from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import Header from '../Header';
import { folderType, widgetType } from '../types';
import WidgetNode from '../WidgetNode';
import AddWidgets from './AddWidgets';
import './styles.scss';

const folderDragSource = DragSource(
  folderType,
  {
    beginDrag: props => ({
      id: props.domain.id,
      path: props.path
    }),
    isDragging: (props, monitor) => props.domain.id === monitor.getItem().id,
    canDrag: props => !props.root
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  })
);

const folderDropTarget = DropTarget(
  [folderType, widgetType],
  {
    hover: ({ path: hoveredPath, root, onMove, onDomainMove }, monitor) => {
      if (!monitor.canDrop()) return;

      const { path: draggedPath } = monitor.getItem();

      switch (monitor.getItemType()) {
        case widgetType: {
          const newPath = [...hoveredPath, 0];

          if (draggedPath.join() === newPath.join()) return;

          onMove(draggedPath, newPath);
          monitor.getItem().path = newPath;
          break;
        }

        default: {
          const levelDifference = draggedPath.length - hoveredPath.length;
          const newPath = levelDifference === 0 ? hoveredPath : [...hoveredPath, 0];

          if (draggedPath.join() === newPath.join()) return;

          onDomainMove(draggedPath, newPath);
          monitor.getItem().path = newPath;
        }
      }
    },
    canDrop: ({ root, path: hoveredPath = [] }, monitor) => {
      if (monitor.getItemType() === widgetType) return true;
      if (root) return false;

      const { path: draggedPath } = monitor.getItem();
      const levelDifference = draggedPath.length - hoveredPath.length;

      return 0 <= levelDifference && levelDifference <= 1;
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop()
  })
);

const dnd = flow(
  folderDropTarget,
  folderDragSource
);

const DomainNode = dnd(
  ({
    root,
    domain,
    onDomainMove,
    onMove,
    isOver,
    isDragging,
    canDrop,
    connectDragSource,
    connectDragPreview,
    connectDropTarget,
    path = []
  }) => {
    const ref = React.useRef();
    connectDragPreview(ref);
    connectDropTarget(ref);

    const dragHandleRef = React.useRef();
    connectDragSource(dragHandleRef);

    const [isAddingWidgets, setIsAddingWidgets] = React.useState(false);

    const { subElements = [], widgets = [] } = domain;

    return (
      <div className="domain-node" style={{ opacity: isDragging ? 0.2 : 1 }}>
        <Header
          ref={ref}
          dragHandleRef={dragHandleRef}
          root={root}
          domain={domain}
          path={path}
          highlighted={canDrop && isOver}
          onToggleWidgetInsert={() => setIsAddingWidgets(prevState => !prevState)}
        />

        <div className="domain-node-contents">
          {subElements.map((domain, index) => (
            <DomainNode
              key={domain.id}
              domain={domain}
              path={[...path, index]}
              onMove={onMove}
              onDomainMove={onDomainMove}
            />
          ))}
          {widgets.map((widget, index) => (
            <WidgetNode key={widget.id} {...widget} path={[...path, index]} onMove={onMove} />
          ))}
          {isAddingWidgets && <AddWidgets path={path} widgets={widgets} />}
        </div>
      </div>
    );
  }
);

export default DomainNode;
