import { mdiArrowAll, mdiDelete, mdiFolder, mdiPlus } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { Form } from 'react-bulma-components';
import IconButton from '../../../../components/IconButton';
import TreeContext from '../TreeContext';
import { levelNames, PERFORMANCE, PLAN, PROGRAM } from '../types';
import './styles.scss';

const Header = React.forwardRef(({ dragHandleRef, domain, root, path, highlighted, onToggleWidgetInsert }, ref) => {
  const { onDomainChange, onDeleteDomain, onCreateDomain } = React.useContext(TreeContext);

  const onFieldChange = ({ target: { name, value } }) =>
    onDomainChange(path, prevState => ({
      ...prevState,
      [name]: value
    }));

  const onLevelNameChange = event => {
    const {
      target: { name, value }
    } = event;

    if (domain.levelNameEt === PERFORMANCE && value === PLAN && (domain.subElements || []).length > 0) {
      if (window.confirm('Muutes tulemusvaldkonna valdkonnaks, kustuvad alamvaldkonnad. Oled kindel?'))
        onDomainChange(path, prevState => ({
          ...prevState,
          [name]: value,
          subElements: []
        }));
    } else {
      onFieldChange(event);
    }
  };

  return (
    <div ref={ref} className={`domain-node-header ${highlighted ? 'highlighted' : ''}`}>
      {!root && (
        <>
          <Form.Select name="levelNameEt" value={domain.levelNameEt} onChange={onLevelNameChange}>
            {levelNames[path.length - 1].map(value => (
              <option key={value} value={value} checked={value === domain.levelNameEt}>
                {value}
              </option>
            ))}
          </Form.Select>
          <Form.Input
            autoComplete="off"
            name="nameEt"
            placeholder="Eestikeelne nimetus"
            value={domain.nameEt}
            onChange={onFieldChange}
          />
          <Form.Input
            autoComplete="off"
            name="nameEn"
            placeholder="Name in English"
            value={domain.nameEn}
            onChange={onFieldChange}
          />
          <IconButton name="domain-drag-btn" ref={dragHandleRef} renderAs="span">
            <Icon path={mdiArrowAll} size={1} />
          </IconButton>
          <IconButton name="domain-delete-btn" onClick={() => onDeleteDomain(path)} renderAs="span">
            <Icon path={mdiDelete} size={1} />
          </IconButton>
        </>
      )}
      {(root || [PERFORMANCE, PROGRAM].includes(domain.levelNameEt)) && (
        <IconButton name="add-domain-btn" onClick={() => onCreateDomain(path)} renderAs="span">
          <Icon path={mdiFolder} size={1} />
        </IconButton>
      )}
      {!root && (
        <IconButton name="domain-add-widget-btn" onClick={onToggleWidgetInsert} renderAs="span">
          <Icon path={mdiPlus} size={1} />
        </IconButton>
      )}
    </div>
  );
});

export default Header;
