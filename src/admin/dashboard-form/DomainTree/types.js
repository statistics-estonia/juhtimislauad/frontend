export const folderType = 'domain-tree-folder';
export const widgetType = 'domain-tree-widget';

export const PLAN = 'Valdkond';
export const PERFORMANCE = 'Tulemusvaldkond';
export const PROGRAM = 'Programm';
export const MEASURE = 'Meede';

export const levelNames = [[PLAN, PERFORMANCE], [PROGRAM], [MEASURE]];
