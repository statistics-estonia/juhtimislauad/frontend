import { omitBy, uniqBy } from 'lodash';
import React from 'react';
import { Button, Form, Heading, Section } from 'react-bulma-components';
import useForm, { useValidator } from '../../shared/form';
import { usePromise } from '../../shared/hooks';
import { useGetRegions } from '../DashboardProvider';
import { DomainTree } from './DomainTree';
import RoleSelector from './RoleSelector';

export const omitTemporaryIds = ({ elements = [], ...dashboard }) => ({
  ...dashboard,
  elements: elements.map(omitTemporaryDomainIds)
});

const omitTemporaryDomainIds = ({ subElements = [], ...domain }) => ({
  ...omitBy(domain, (value, key) => key === 'id' && String(value).startsWith('$')),
  subElements: subElements.map(omitTemporaryDomainIds)
});

const DashboardForm = ({ value, onChange, onSave }) => {
  const getRegions = useGetRegions();

  const allRegions = usePromise(getRegions, [getRegions], []);
  const isDashboard = value.level === 0;
  const typeLabel = isDashboard ? 'Juhtimislaua' : 'Arengukava';

  const selectAllRegions = () =>
    onChange(prevState => ({
      ...prevState,
      regions: allRegions
    }));

  const selectRegionsForLevel = level =>
    onChange(({ regions = [], ...prevState }) => ({
      ...prevState,
      regions: uniqBy([...regions, ...allRegions.filter(region => region.level === level)], 'id')
    }));

  const [saveErrors, setSaveErrors] = React.useState(EMPTY_ARRAY);

  React.useEffect(() => {
    setSaveErrors(EMPTY_ARRAY);
  }, [value]);

  const onSubmit = async event => {
    event.preventDefault();

    if (!isFormValid) return;

    try {
      await onSave(event);
    } catch (error) {
      setSaveErrors(mapApiErrorMessageToValidationErrors(error.message));
    }
  };

  const [isFormValid, invalidRules] = useValidator(value, dashboardValidationRules, saveErrors);
  const { text, checkbox, select } = useForm(value, onChange, invalidRules);

  return (
    <form id="dashboard-form" onSubmit={onSubmit}>
      <RoleSelector onChange={onChange} value={value} />

      <Section>
        <Heading size={4}>Sätted</Heading>
        <Form.Field>
          <Form.Label>Nimetus eesti keeles</Form.Label>
          {text('nameEt')}
        </Form.Field>
        <Form.Field>
          <Form.Label>Nimetus inglise keeles</Form.Label>
          {text('nameEn')}
        </Form.Field>
        <Form.Label>{`${typeLabel} olek`}</Form.Label>
        <Form.Field kind="group">
          {checkbox('status', { label: 'Nähtav', value: 'VISIBLE' })}
          {checkbox('status', { label: 'Peidetud', value: 'HIDDEN' })}
        </Form.Field>
        <Form.Field>
          <Form.Label>Lubatud piirkonnad</Form.Label>
          {select('regions', {
            id: 'select-allowed-regions',
            options: allRegions,
            value: value.regions,
            placeholder: 'Vali piirkonnad',
            getOptionLabel: region => region.nameEt || region.name,
            getOptionValue: region => region.id,
            isSearchable: true,
            isMulti: true
          })}
          <Form.Help>
            <Button
              id="select-all-district"
              type="button"
              size="small"
              onClick={selectAllRegions}
              style={{ padding: '1rem' }}
            >
              Vali kõik
            </Button>{' '}
            <Button
              id="select-all-district"
              type="button"
              size="small"
              onClick={() => selectRegionsForLevel(1)}
              style={{ padding: '1rem' }}
            >
              Vali kõik maakonnad
            </Button>{' '}
            <Button
              id="select-all-kov"
              type="button"
              size="small"
              onClick={() => selectRegionsForLevel(2)}
              style={{ padding: '1rem' }}
            >
              Vali kõik KOVid
            </Button>
          </Form.Help>
        </Form.Field>
      </Section>

      <Section id="domain-tree">
        <Heading size={4}>Valdkonnapuu</Heading>
        <DomainTree
          rootDomain={{ subElements: value.elements }}
          onChange={updater =>
            onChange(prevState => ({
              ...prevState,
              elements: updater({ subElements: prevState.elements }).subElements
            }))
          }
        />
      </Section>

      <Button className="is-primary" type="submit" disabled={!isFormValid}>
        Salvesta
      </Button>
    </form>
  );
};

const EMPTY_ARRAY = [];

const mapApiErrorMessageToValidationErrors = message => {
  const errors = [];

  switch (message) {
    case 'Problem with dashboard, reason: nameEt already exists':
      errors.push({
        name: 'nameEt',
        message: 'Sellise nimega juhtimislaud on juba olemas'
      });
      break;
    case 'Problem with dashboard, reason: nameEn already exists':
      errors.push({
        name: 'nameEn',
        message: 'Sellise nimega juhtimislaud on juba olemas'
      });
      break;
    default:
      break;
  }

  return errors;
};

const dashboardValidationRules = [
  {
    name: 'nameEt',
    message: 'See väli on nõutud',
    predicate: value => value.nameEt
  },
  {
    name: 'nameEn',
    message: 'See väli on nõutud',
    predicate: value => value.nameEn
  }
];

export default DashboardForm;
