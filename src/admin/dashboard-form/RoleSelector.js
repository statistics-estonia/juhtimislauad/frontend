import React from 'react';
import { Form, Heading, Section, Columns, Content } from 'react-bulma-components';
import { usePromise } from '../../shared/hooks';
import { useGetRoles } from '../DashboardProvider';
import Select from '../../components/Select/';

const RoleSelector = ({ onChange, value }) => {
  const getRoles = useGetRoles();
  const roles = usePromise(async () => await getRoles(), [getRoles], []);
  const mainRole = value.mainRole ? roles.find(role => role.id === value.mainRole.id) : null;
  const subRoleOptions = value.mainRole && mainRole ? mainRole.elements : [];
  const isRegional = mainRole && !mainRole.elements;

  const onMainRoleChange = role => {
    onChange({
      ...value,
      mainRole: role ? role : undefined,
      subRole: role && role.elements ? role.elements[0] : undefined
    });
  };

  const onSubRoleChange = role => {
    onChange({
      ...value,
      subRole: role
    });
  };

  return (
    <Section id="roles">
      <Heading size={4}>Seos rolliga</Heading>
      <Columns>
        <Columns.Column size="half">
          <Content>
            <Form.Field>
              <Form.Label>Katusroll</Form.Label>
              <Select
                id="mainRole"
                aria-label="select main role"
                options={roles}
                placeholder={'Vali roll'}
                onChange={opt => onMainRoleChange(opt)}
                isSearchable
                isClearable
                getOptionLabel={role => (role.name ? role.name : role.nameEt)}
                getOptionValue={role => role.id}
                noOptionsMessage={() => 'Väärtused puuduvad'}
                value={mainRole}
              />
            </Form.Field>
          </Content>
        </Columns.Column>
        {mainRole && !isRegional && (
          <Columns.Column size="half">
            <Content>
              <Form.Field>
                <Form.Label>Alamroll</Form.Label>
                <Select
                  id="mainRole"
                  aria-label="select sub role"
                  options={subRoleOptions}
                  placeholder={'Vali roll'}
                  onChange={opt => onSubRoleChange(opt)}
                  isSearchable
                  getOptionLabel={dashboard => (dashboard.name ? dashboard.name : dashboard.nameEt)}
                  getOptionValue={dashboard => dashboard.id}
                  noOptionsMessage={() => 'Väärtused puuduvad'}
                  value={value.subRole}
                />
              </Form.Field>
            </Content>
          </Columns.Column>
        )}
      </Columns>
    </Section>
  );
};

export default RoleSelector;
