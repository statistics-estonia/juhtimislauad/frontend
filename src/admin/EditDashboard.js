import React from 'react';
import { useDeferredState, usePromise } from '../shared/hooks';
import DashboardForm, { omitTemporaryIds } from './dashboard-form/DashboardForm';
import { useGetDashboard, useUpdateDashboard } from './DashboardProvider';

const EditDashboard = ({ match, history }) => {
  const { lang } = match.params;
  const getDashboard = useGetDashboard();
  const updateDashboard = useUpdateDashboard();

  const originalDashboard = usePromise(() => getDashboard(match.params.id), [getDashboard]);
  const [dashboard, setDashboard] = useDeferredState(originalDashboard);

  if (!dashboard) return null;

  const onSave = async () => {
    await updateDashboard(omitTemporaryIds(dashboard));
    history.replace(`/${lang}/admin/dashboards`);
  };

  return <DashboardForm value={dashboard} onChange={setDashboard} onSave={onSave} />;
};

export default EditDashboard;
