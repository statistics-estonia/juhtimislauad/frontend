export const SOURCES = {
  NONE: { ET: 'Puudub', DEV: 'NONE' },
  DATA_API: { ET: 'PxWeb', DEV: 'DATA_API' },
  DATA_SQL: { ET: 'PxWebSql', DEV: 'DATA_SQL' },
  EXCEL: { ET: 'Excel', DEV: 'EXCEL' },
};

export const QUERY_TYPES = {
  WIDGET: { ET: 'Kuvamooduli nimi', DEV: 'queryForWidget' },
  DASHBOARD: { ET: 'Juhtimislaud', DEV: 'queryForDashboard' },
  CUBE: { ET: 'Kuubikood', DEV: 'queryForCube' },
};

export const EXCEL_DELETE_CONFIRMATION = "'Oled kindel, et soovid Exceli eemaldada?'";
export const DELETE_CUSTOM_FILTERS_CONFIRMATION =
  "'Oled kindel, et soovid kõik antud kuvamooduli käsitsi loodud filtri väärtused kustutada?'";
