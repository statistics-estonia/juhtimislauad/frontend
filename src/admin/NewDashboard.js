import React from 'react';
import { Columns } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { withRouter } from 'react-router';
import DashboardForm, { omitTemporaryIds } from './dashboard-form/DashboardForm';
import { useCreateDashboard } from './DashboardProvider';

const NewDashboard = withRouter(({ history, match }) => {
  const createDashboard = useCreateDashboard();
  const { lang } = match.params;
  const [dashboard, setDashboard] = React.useState({
    type: 'GLOBAL',
    nameEt: 'Uus juhtimislaud',
    nameEn: 'New dashboard',
    status: 'HIDDEN',
    regions: [],
    elements: [],
    level: 0
  });

  const onSave = async () => {
    await createDashboard(addId(omitTemporaryIds(dashboard)));
    history.replace(`/${lang}/admin/dashboards`);
  };

  return (
    <Columns>
      <Columns.Column size="two-thirds">
        <Heading size={3}>Lisa uus juhtimislaud</Heading>
        <DashboardForm value={dashboard} onChange={setDashboard} onSave={onSave} />
      </Columns.Column>
    </Columns>
  );
});

const addId = dashboard => ({
  code: String(new Date().getTime()),
  ...dashboard
});

export default NewDashboard;
