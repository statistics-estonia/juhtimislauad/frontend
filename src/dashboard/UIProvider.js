import React, { createContext, useContext, useEffect, useState } from 'react';
import { useLocation } from 'react-router';

const UIContext = createContext({});

const UIProvider = ({ children }) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [openModals, setOpenModals] = useState(new Set());

  const location = useLocation();

  useEffect(() => {
    setOpenModals(new Set());
  }, [location.pathname]);

  const value = {
    isDrawerOpen,
    setIsDrawerOpen,
    isMenuOpen,
    setIsMenuOpen,
    openModals,
    setOpenModals,
  };

  return <UIContext.Provider value={value}>{children}</UIContext.Provider>;
};

export const useDrawer = () => {
  const { isDrawerOpen, setIsDrawerOpen } = useContext(UIContext);
  const toggleDrawer = () => {
    window.scrollTo(0, 0);
    setIsDrawerOpen(!isDrawerOpen);
  };
  const closeDrawer = () => setIsDrawerOpen(false);
  const openDrawer = () => setIsDrawerOpen(true);

  return { isDrawerOpen, openDrawer, closeDrawer, toggleDrawer };
};

export const useMenu = () => {
  const { isMenuOpen, setIsMenuOpen } = useContext(UIContext);
  const toggleMenu = () => {
    window.scrollTo(0, 0);
    setIsMenuOpen(!isMenuOpen);
  };
  const closeMenu = () => setIsMenuOpen(false);
  const openMenu = () => setIsMenuOpen(true);

  return { isMenuOpen, openMenu, closeMenu, toggleMenu };
};

export const useModal = () => {
  const { openModals, setOpenModals } = useContext(UIContext);

  const open = modalName => {
    setOpenModals(prevModals => new Set(prevModals).add(modalName));
  };

  const close = modalName => {
    setOpenModals(prevModals => {
      const updatedModals = new Set(prevModals);
      updatedModals.delete(modalName);
      return updatedModals;
    });
  };

  const isOpen = modalName => openModals.has(modalName);

  return { open, close, isOpen };
};

export default UIProvider;
