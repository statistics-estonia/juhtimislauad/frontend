import { mdiDownload, mdiPrinter, mdiShareVariant, mdiPin } from '@mdi/js';
import Icon from '@mdi/react';
import React, { useState } from 'react';
import { useIsSharingDisabled, useWidget } from '../../dashboard/WidgetProvider';
import SharePopup from '../../embed/SharePopup';
import ExportPopup from '../../export/ExportPopup';
import { usePrint } from '../../export/ExportProvider';
import { useTranslate, Translate } from '../../i18n/LanguageProvider';
import IconButton from '../IconButton';
import { Modal, Section } from 'react-bulma-components';
import { useIsGuest } from '../../auth/UserProvider';
import { useWidgetPinned } from '../../preferences/widget';
import { useMedia } from '../../shared/hooks';

const ShareButtons = ({ isWidgetInfoOpen }) => {
  const widget = useWidget();
  const isSharingDisabled = useIsSharingDisabled();
  const [pinned, setPinned] = useWidgetPinned();
  const translate = useTranslate();
  const print = usePrint();
  const isGuest = useIsGuest();
  const isTabletOrSmaller = useMedia(['(max-width: 1024px)', '(min-width: 1px)'], [true, false], false);
  const [showIEPrintWarning, setShowIEPrintWarning] = useState(false);
  const isIE = /MSIE|Trident/.test(window.navigator.userAgent);
  const handlePrint = () => {
    isIE ? setShowIEPrintWarning(true) : print();
  };

  const handlePin = () => setPinned(widget.id, { pinned: !pinned });

  return (
    <>
      <ExportPopup>
        <ShareIconButton
          title={translate('stat.share.download')}
          disabled={isWidgetInfoOpen || isSharingDisabled}
          aria-label="export"
          icon={mdiDownload}
        />
      </ExportPopup>

      <ShareIconButton
        title={translate('stat.share.print')}
        aria-label="print"
        icon={mdiPrinter}
        disabled={isWidgetInfoOpen || isSharingDisabled}
        onClick={handlePrint}
      />

      <SharePopup>
        <ShareIconButton
          title={translate('stat.share')}
          disabled={isWidgetInfoOpen || isSharingDisabled}
          aria-label="share"
          icon={mdiShareVariant}
        />
      </SharePopup>

      {!isSharingDisabled && !(isGuest && isTabletOrSmaller) && (
        <ShareIconButton
          title={
            isGuest
              ? translate('stat.share.pinGuest')
              : pinned
              ? translate('stat.share.unpin')
              : translate('stat.share.pin')
          }
          className={`icon--button is-inline${pinned ? ' active' : ''}`}
          disabled={isGuest}
          aria-label="pin"
          icon={mdiPin}
          onClick={handlePin}
        />
      )}

      {showIEPrintWarning && <IEPrintWarning onClose={() => setShowIEPrintWarning(false)} />}
    </>
  );
};

const ShareIconButton = React.forwardRef(({ icon, ...props }, ref) => (
  <IconButton ref={ref} rounded color="white" className="icon--button is-inline" {...props}>
    <Icon path={icon} size={1} />
  </IconButton>
));

const IEPrintWarning = ({ onClose }) => (
  <Modal show onClose={onClose} closeOnBlur={true}>
    <Modal.Content>
      <Section style={{ backgroundColor: 'white' }}>
        <span style={{ fontSize: '1.5em' }}>
          <Translate>stat.share.iePrint</Translate>
        </span>
      </Section>
    </Modal.Content>
  </Modal>
);

export default ShareButtons;
