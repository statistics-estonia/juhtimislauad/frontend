import { mdiChartBar, mdiChartBarStacked, mdiChartLine, mdiInformation } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { ReactComponent as IconArea } from '../../assets/images/icons/area.svg';
import { ReactComponent as IconVerticalBar } from '../../assets/images/icons/bar-x.svg';
import { ReactComponent as IconMap } from '../../assets/images/icons/eesti.svg';
import { ReactComponent as IconPie } from '../../assets/images/icons/pie.svg';
import { ReactComponent as IconPyramid } from '../../assets/images/icons/pyramid.svg';
import { ReactComponent as IconRadar } from '../../assets/images/icons/radar.svg';
import { ReactComponent as IconTreemap } from '../../assets/images/icons/treemap.svg';
import { useWidget } from '../../dashboard/WidgetProvider';
import { useChartType } from '../../preferences/widget';
import IconButton from '../IconButton';
import { useTranslate } from '../../i18n/LanguageProvider';

const iconFromChartType = type => {
  switch (type) {
    case 'bar':
      return <Icon path={mdiChartBar} size={1} />;
    case 'vertical':
      return <IconVerticalBar />;
    case 'pyramid':
      return <IconPyramid />;
    case 'map':
      return <IconMap />;
    case 'pie':
      return <IconPie />;
    case 'stacked':
      return <Icon path={mdiChartBarStacked} size={1} />;
    case 'area':
      return <IconArea />;
    case 'treemap':
      return <IconTreemap />;
    case 'radar':
      return <IconRadar />;
    default:
      return <Icon path={mdiChartLine} size={1} />;
  }
};

const ChartButtons = ({ isWidgetInfoOpen, setIsWidgetInfoOpen }) => {
  const { graphTypes: { options: chartTypes = [] } = {} } = useWidget();
  const [chartType, setChartType] = useChartType();
  const translate = useTranslate();

  const changeChartType = aChartType => {
    setIsWidgetInfoOpen(false);
    setChartType(aChartType);
  };

  return (
    <>
      {chartTypes.map(option => {
        const aChartType = option.toLowerCase();
        return (
          <IconButton
            key={aChartType}
            aria-label={`Choose ${aChartType}`}
            className={chartType === aChartType && !isWidgetInfoOpen ? 'active is-inline' : 'is-inline'}
            onClick={() => changeChartType(aChartType)}
            title={translate(`stat.chart.${aChartType}`)}
          >
            {iconFromChartType(aChartType)}
          </IconButton>
        );
      })}
      <IconButton
        aria-label="Choose info"
        className={isWidgetInfoOpen ? 'active is-inline' : 'is-inline'}
        onClick={() => setIsWidgetInfoOpen(true)}
        title={translate('stat.chart.info')}
      >
        <Icon path={mdiInformation} size={1} />
      </IconButton>
    </>
  );
};

export default ChartButtons;
