import React, { useState, useEffect, useRef } from 'react';
import { mdiChevronDown, mdiChevronUp } from '@mdi/js';
import Icon from '@mdi/react';
import './LoginSelect.scss';
import { useIsLoggedIn } from '../../../auth/UserProvider';

const LoginSelect = ({ label, options, onChange }) => {
  const [isOpen, setIsOpen] = useState(false);
  const selectRef = useRef(null);
  const isLoggedIn = useIsLoggedIn();

  useEffect(() => {
    const handleClickOutside = event => {
      if (selectRef.current && !selectRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  const filteredOptions = options.filter(option => option.value !== label.value);

  return (
    <div className="login-select" ref={selectRef}>
      {isLoggedIn && <div className="profile"></div>}

      <div className="login-select__dropdown" onClick={() => setIsOpen(!isOpen)}>
        {label.name}
        <Icon path={isOpen ? mdiChevronUp : mdiChevronDown} size={1} className="arrow-icon" />
      </div>

      {isOpen && (
        <ul className="login-select__options">
          {filteredOptions.map(option => (
            <li
              key={option.value}
              className="login-select__option"
              onClick={() => {
                onChange(option);
                setIsOpen(false);
              }}
            >
              {option.name}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default LoginSelect;
