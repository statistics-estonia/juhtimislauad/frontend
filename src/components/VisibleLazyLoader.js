import React, { useState } from 'react';
import { Waypoint } from 'react-waypoint';

const VisibleLazyLoader = ({ children }) => {
  const [isVisible, setIsVisible] = useState(false);

  const waypoint = <Waypoint onEnter={() => setIsVisible(true)} />;

  return children(waypoint, isVisible);
};

export default VisibleLazyLoader;
