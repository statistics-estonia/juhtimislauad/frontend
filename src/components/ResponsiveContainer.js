import React from 'react';
import { debounce } from 'lodash';

export const useResponsiveContainer = isEmbedded => {
  const [size, setSize] = React.useState({ width: 0, height: 0 });
  const ref = React.useRef();
  const hasBeenReset = React.useRef(true);

  const setNode = React.useCallback(node => {
    ref.current = node;
    updateSize();
  });

  const updateSize = () => {
    if (!ref.current) return;

    setSize({
      width: ref.current.getBoundingClientRect().width,
      height: isEmbedded
        ? ref.current.getBoundingClientRect().height > 300
          ? ref.current.getBoundingClientRect().height
          : 300
        : ref.current.getBoundingClientRect().height
    });
    hasBeenReset.current = false;
  };

  const updateSizeDebounced = React.useCallback(debounce(updateSize, 150), []);

  React.useEffect(() => {
    window.addEventListener('resize', updateSizeDebounced);

    return () => {
      updateSizeDebounced.cancel();
      window.removeEventListener('resize', updateSizeDebounced);
    };
  }, []);

  return [size, setNode];
};
