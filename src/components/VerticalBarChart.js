import React from 'react';
import { Bar, BarChart, CartesianGrid, Cell, Tooltip as RCTooltip, XAxis, YAxis } from 'recharts';
import { COLOR_THEME, DEFAULT_MARGIN } from '../assets/styles';
import { useFormatNumber } from '../i18n/hooks';
import { useLayoutReady, useYAxisWidth, useMedia } from '../shared/hooks';
import { getFirstSeriesData } from '../shared/series';
import { AxisLabel } from './Charts/chart';
import Tooltip from './Charts/Tooltip';
import { useResponsiveContainer } from './ResponsiveContainer';
import { useWidgetFilterValues } from '../dashboard/WidgetProvider';
import * as d3 from 'd3';
import { useActiveLanguageCode } from '../i18n/LanguageProvider';
import { useWidgetHeight } from '../container/Dashboard/WidgetDetails';

const { primary, secondary } = COLOR_THEME;

const seriesToChartData = series => getFirstSeriesData(series).map(toNameValuePair);
const toNameValuePair = ({ abscissa, value, selected }) => ({ name: abscissa, value, selected });

const VerticalBarChart = ({ widget, isExpanded, isEmbedded, disableAnimation, onLayoutReady = () => {} }) => {
  const formatNumber = useFormatNumber();
  const filterValues = useWidgetFilterValues()();
  const dataSetForChart = React.useMemo(() => seriesToChartData(widget.diagram.series), [widget]);
  const tooltipPayloadValueFormatter = (value, item, payload, defaultFormatter) =>
    `${defaultFormatter(value)} ${widget.unit ? widget.unit : ``}`;

  const callbackRef = React.useCallback(node => {
    setContainerNode(node);
    setYAxisNode(node);
  }, []);
  const [{ width, height }, setContainerNode] = useResponsiveContainer(isEmbedded);
  const [yAxisWidth, setYAxisNode] = useYAxisWidth([height, width, dataSetForChart]);
  const margin = { ...DEFAULT_MARGIN, bottom: widget.unit ? 20 : DEFAULT_MARGIN.bottom };
  useLayoutReady(() => yAxisWidth > 0, onLayoutReady, [yAxisWidth]);
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);

  return (
    <div className={`chart vertical_bar-chart${isEmbedded ? '' : ' is-clipped'}`} ref={callbackRef}>
      <BarChart margin={margin} data={dataSetForChart} layout="vertical" width={width} height={height}>
        <XAxis
          type="number"
          tickFormatter={value =>
            formatNumber(value, {
              style: 'decimal',
              minimumFractionDigits: false
            })
          }
          label={<AxisLabel value={widget.unit} axis="x" />}
        />
        <YAxis
          type="category"
          dataKey="name"
          tick={<VerticalYAxisTick type="category" dy="0.3em" chartWidth={width} disableAnimation={disableAnimation} />}
          width={yAxisWidth}
          interval={isExpanded && !isMobile ? 0 : 'preserveStartEnd'}
        />
        <CartesianGrid horizontal={false} width={width} />
        <Bar dataKey="value" fill={primary} isAnimationActive={!disableAnimation}>
          {dataSetForChart.map(({ name, selected }) =>
            selected === true ? <Cell key={name} fill={secondary} /> : <Cell key={name} fill={primary} />
          )}
        </Bar>
        <RCTooltip
          content={
            isExpanded ? (
              <Tooltip
                noFiltersInTitle
                itemLabel={filterValues.join(', ')}
                showRectangle={false}
                payloadValueFormatter={tooltipPayloadValueFormatter}
              />
            ) : (
              () => null
            )
          }
        />
      </BarChart>
    </div>
  );
};

const VerticalYAxisTick = ({
  x,
  y,
  payload: { value },
  height,
  width,
  chartWidth,
  dy = '-0.355em',
  textAnchor = 'end',
  disableAnimation
}) => {
  const lang = useActiveLanguageCode();
  const [, isOverflowingChart] = useWidgetHeight();
  const textRef = React.useRef(null);

  React.useLayoutEffect(() => {
    if (textRef.current && width > 0) d3.select(textRef.current).call(ellipsifyTick, chartWidth, value);
  }, [value, lang, chartWidth, width]);

  return (
    <text
      style={{ fontSize: disableAnimation && isOverflowingChart ? '0.6em' : '0.875em' }}
      ref={textRef}
      height={height}
      x={x}
      y={y}
      dy={dy}
      textAnchor={textAnchor}
      stroke="none"
      className="recharts-text recharts-cartesian-axis-tick-value"
      fill="#666"
    />
  );
};

const ellipsifyTick = (tick, chartWidth, value) => {
  tick.text(value);
  if (tick.node() && tick.node().textContent) {
    let textLength = tick.node().getComputedTextLength();
    let text = value;
    while (textLength > chartWidth * 0.5 && text.length > 0) {
      text = text.slice(0, -1);
      tick.text(`${text}...`);
      textLength = tick.node().getComputedTextLength();
    }
  }
};

export default VerticalBarChart;
