import React from 'react';
import './styles.css';

const WidgetError = props => <div className="widget-screen-of-death" {...props} />;

export default WidgetError;
