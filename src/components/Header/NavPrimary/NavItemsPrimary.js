import React from 'react';
import { Navbar } from 'react-bulma-components';

const NavItemsPrimary = ({ additionalMenuItems = null }) => {
  return <Navbar.Item className="has-dropdown is-hoverable">{additionalMenuItems}</Navbar.Item>;
};

export default NavItemsPrimary;
