import React from 'react';
import { Navbar } from 'react-bulma-components';
import { Link, useHistory, withRouter } from 'react-router-dom';
import logo from '../../../assets/images/statistikaamet-logo.svg';
import logoEn from '../../../assets/images/statistikaamet-logo-en.svg';
import { useMedia } from '../../../shared/hooks';
import NavMobile from '../NavMobile';
import NavMobileBurger from '../NavMobile/NavMobileBurger';
import NavItemsPrimary from './NavItemsPrimary';
import './NavPrimary.scss';
import '../NavMobile/NavPrimaryMobile.scss';
import { useMenu, useDrawer } from '../../../dashboard/UIProvider';
import { useActiveLanguageCode, useTranslate } from '../../../i18n/LanguageProvider';
import { isDev } from '../../../shared/utils';
import Login from '../Login';
import { useCurrentUser, useIsGuest, useLogout } from '../../../auth/UserProvider';
import LoginSelect from '../../Select/LoginSelect/LoginSelect';
import DemoLogin from '../Login/DemoLogin';

const NavPrimary = ({ additionalMenuItems = null, dashboard }) => {
  const { isMenuOpen, closeMenu } = useMenu();
  const { toggleDrawer } = useDrawer();
  const user = useCurrentUser();
  const isGuest = useIsGuest();
  const logout = useLogout();
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const isTablet = useMedia(['(max-width: 1023px)', '(max-width: 769px)'], [true, false], false);
  const isTouchDevice = isMobile || isTablet;
  const lang = useActiveLanguageCode();
  const clickHandler = () => {
    toggleDrawer();
    closeMenu();
  };

  const NavItemUser = ({ user, onClickLogout }) => {
    const isGuest = useIsGuest();
    const history = useHistory();
    const translate = useTranslate();

    const userMenuOptions = [
      { value: 'user', name: user ? user.username : null },
      { value: 'allDashboards', name: translate('stat.header.allDashboards') },
      ...(!isGuest ? [{ value: 'myDashboard', name: translate('stat.header.myDashboard') }] : []),
      { value: 'logOut', name: translate('stat.header.logout') },
    ];

    const onMenuOptionChange = ({ value }) => {
      switch (value) {
        case 'myDashboard':
          history.push(translate('stat.myDashboardPath'));
          break;
        case 'allDashboards':
          history.push('/');
          break;
        case 'logOut':
          onClickLogout();
          break;
        default:
          break;
      }
    };

    return (
      <div className="header__personal__person">
        <LoginSelect label={userMenuOptions[0]} options={userMenuOptions} onChange={onMenuOptionChange} />
      </div>
    );
  };

  return (
    <div className="navbar-wrapper navbar-primary dotted">
      <Navbar className="navbar-primary dotted" aria-label="main navigation" active={isMenuOpen}>
        <div className="u-container">
          <Navbar.Brand>
            <Link to="/" className="navbar-item">
              <img src={lang === 'et' ? logo : logoEn} className="header-logo" alt="Statameti logo" />
            </Link>
            {dashboard && isTouchDevice && (
              <div className="navbar-item-mobile" onClick={clickHandler}>
                <NavItemsPrimary additionalMenuItems={additionalMenuItems} />
              </div>
            )}
            {isTouchDevice && <NavMobileBurger />}
          </Navbar.Brand>
          <Navbar.Menu active="true" id="primaryNavbar" className="primary-nav">
            {isTouchDevice ? (
              <NavMobile dashboard={dashboard} />
            ) : (
              <>
                <Navbar.Container position="end" className="navbar-end-container">
                  <Navbar.Item>
                    {!isGuest && <NavItemUser onClickLogout={logout} user={user} />}
                    {isGuest && isDev && <DemoLogin />}
                    {isGuest && !isDev && <Login />}
                  </Navbar.Item>
                  <NavItemsPrimary additionalMenuItems={additionalMenuItems} />
                </Navbar.Container>
              </>
            )}
          </Navbar.Menu>
        </div>
      </Navbar>
    </div>
  );
};

export default withRouter(NavPrimary);
