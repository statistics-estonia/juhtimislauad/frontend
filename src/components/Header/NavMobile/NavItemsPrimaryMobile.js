import React from 'react';
import { useHistory } from 'react-router-dom';
import LangSelector from '../NavSecondary/LangSelector';
import { useTranslate } from '../../../i18n/LanguageProvider';
import { useIsGuest } from '../../../auth/UserProvider';
import { useWidgets } from '../../../dashboard/DashboardProvider';
import Login from '../Login';
import MobileLoginMenu from './MobileLoginMenu/MobileLoginMenu';
import { useMenu } from '../../../dashboard/UIProvider';

export const NavItemUser = ({ user, onClickLogout }) => {
  const isGuest = useIsGuest();
  const history = useHistory();
  const translate = useTranslate();
  const { closeMenu } = useMenu();
  const userMenuOptions = [
    { value: 'user', name: user ? user.username : null },
    { value: 'allDashboards', name: translate('stat.header.allDashboards') },
    ...(!isGuest ? [{ value: 'myDashboard', name: translate('stat.header.myDashboard') }] : []),
    { value: 'logOut', name: translate('stat.header.logout') },
  ];

  const onMenuOptionChange = ({ value }) => {
    closeMenu();
    if (value === 'myDashboard') {
      history.push('/dashboard/me');
    } else if (value === 'allDashboards') {
      history.push('/');
    } else if (value === 'logOut') {
      onClickLogout();
    }
  };

  return (
    <div className="nav-item-mobile-user">
      <div className={`auth-wrapper ${!isGuest ? 'mobile-login-active' : ''}`}>
        {!isGuest ? (
          <MobileLoginMenu userMenuOptions={userMenuOptions} onMenuOptionChange={onMenuOptionChange} />
        ) : (
          <Login />
        )}
      </div>

      <div className="lang-selector-wrapper">
        <div className="lang-buttons-wrapper">
          <LangSelector />
        </div>
      </div>
    </div>
  );
};

export const NavItemGraph = ({ graphId }) => {
  const allWidgets = useWidgets();
  const widget = allWidgets.find(w => w.id === graphId);
  return (
    <div className="highlightable nav-item-mobile">
      <div className="graph">{widget ? widget.shortname : '404'}</div>
    </div>
  );
};
