import React from 'react';
import './MobileLoginMenu.scss';

const MobileLoginMenu = ({ userMenuOptions, onMenuOptionChange }) => {
  return (
    <div className="mobile-login-menu">
      <ul>
        {userMenuOptions.map((option, index) => (
          <li key={option.value} onClick={() => onMenuOptionChange(option)} className="menu-option">
            {index === 0 ? (
              <div className="profile-container">
                <span className="profile"></span>
                {option.name}
              </div>
            ) : (
              option.name
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default MobileLoginMenu;
