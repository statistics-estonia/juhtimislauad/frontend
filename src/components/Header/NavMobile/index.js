import React from 'react';
import { withRouter } from 'react-router';
import { useToken } from '../../../auth/TokenProvider';
import { useCurrentUser, useIsGuest } from '../../../auth/UserProvider';
import { useTranslate } from '../../../i18n/LanguageProvider';
import { clearDashboardPreferences } from '../../../preferences/dashboard';
import { clearWidgetPreferences } from '../../../preferences/widget';
import { useSlugIds } from '../../../shared/hooks';
import { NavItemUser, NavItemGraph } from './NavItemsPrimaryMobile';
import './NavPrimaryMobile.scss';

const NavMobile = ({ history, dashboard: currentDashboard }) => {
  const translate = useTranslate();
  const user = useCurrentUser();
  const [, setToken] = useToken();
  const isGuest = useIsGuest();
  const { graphId } = useSlugIds();

  const onClickLogout = () => {
    if (!window.confirm(translate(isGuest ? 'stat.logout.confirmGuest' : 'stat.logout.confirm'))) return;

    if (isGuest) {
      clearDashboardPreferences();
      clearWidgetPreferences();
    }
    history.push('/');
    setToken('guest');
  };

  return (
    <>
      {currentDashboard && <>{graphId && <NavItemGraph graphId={graphId} />}</>}

      <NavItemUser
        user={user}
        onClickLogout={onClickLogout}
        isGuest={isGuest}
        history={history}
        translate={translate}
      />
    </>
  );
};

export default withRouter(NavMobile);
