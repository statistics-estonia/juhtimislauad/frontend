import React, { useEffect } from 'react';
import { Navbar } from 'react-bulma-components';
import LangSelector from './LangSelector';
import './NavSecondary.scss';
import { useMedia } from '../../../shared/hooks';
import { useActiveLanguageCode } from '../../../i18n/LanguageProvider';

const NavSecondary = () => {
  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);
  const isTablet = useMedia(['(max-width: 1023px)', '(max-width: 769px)'], [true, false], false);
  const language = useActiveLanguageCode();

  useEffect(() => {
    const REACT_APP_STAT_HEADER_COMPONENT = process.env.REACT_APP_STAT_HEADER_COMPONENT;
    const script = document.createElement('script');
    script.type = 'module';
    script.src = REACT_APP_STAT_HEADER_COMPONENT;
    script.async = true;
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <div className="navbar-wrapper navbar-secondary">
      <div className="u-container">
        <Navbar color={'dark'} aria-label="links navigation" className="navbar-secondary">
          <Navbar.Menu>
            <Navbar.Container>
              <stat-ee-header-apps data-app="juhtimislauad" data-lang={language}>
                "
              </stat-ee-header-apps>
            </Navbar.Container>

            {!isMobile && !isTablet && (
              <>
                <Navbar.Container style={{ justifyContent: 'center' }}>
                  <stat-ee-header-social
                    data-app="juhtimislauad"
                    data-lang={language}
                    data-style="black"
                  ></stat-ee-header-social>
                </Navbar.Container>

                <Navbar.Container position="end">
                  <Navbar.Item>
                    <LangSelector />
                  </Navbar.Item>
                </Navbar.Container>
              </>
            )}
          </Navbar.Menu>
        </Navbar>
      </div>
    </div>
  );
};

export default NavSecondary;
