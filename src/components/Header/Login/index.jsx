import React from 'react';
import { Translate } from '../../../i18n/LanguageProvider';
import { Button } from 'react-bulma-components';

const Login = () => {
  return (
    <form className="is-flex" action="/api/login/tara/authenticate" method="get">
      <Button submit className="login is-link">
        <Translate>stat.login.enter</Translate>
      </Button>
    </form>
  );
};

export default Login;
