import React, { useState, useRef, useEffect } from 'react';
import { mdiChevronDown, mdiChevronUp } from '@mdi/js';
import Icon from '@mdi/react';
import './Collapse.scss';
import { Container } from 'react-bulma-components';

const Collapse = ({ title, description, icon, children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const contentRef = useRef(null);

  const toggleCollapse = () => {
    setIsOpen(prev => !prev);
  };

  useEffect(() => {
    if (contentRef.current) {
      contentRef.current.style.maxHeight = isOpen ? `${contentRef.current.scrollHeight}px` : '0px';
    }
  }, [isOpen]);

  return (
    <div>
      <Container className="collapse-card">
        <div className="collapse-header" onClick={toggleCollapse}>
          {icon && <div className="icon-wrapper">{icon}</div>}
          <div className="header-content">
            <span className="collapse-title">{title}</span>
            {description && <div className="collapse-description">{description}</div>}
          </div>
          <Icon
            path={isOpen ? mdiChevronUp : mdiChevronDown}
            size={1.5}
            className={`collapse-icon ${isOpen ? 'open' : ''}`}
          />
        </div>
      </Container>

      <div ref={contentRef} className={`collapse-content ${isOpen ? 'open' : ''}`}>
        <div className="collapse-content-wrapper">{children}</div>
      </div>
    </div>
  );
};

export default Collapse;
