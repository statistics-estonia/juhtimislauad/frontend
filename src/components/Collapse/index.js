import React from 'react';

const Collapse = ({ expanded, children }) => {
  const currentHeight = expanded ? 'auto' : '0px';

  return (
    <>
      <div className={`collapse__body ${expanded ? 'collapse__body-open' : ''}`} style={{ height: currentHeight }}>
        <div className="collapse__content">{children}</div>
      </div>
    </>
  );
};

export default Collapse;
