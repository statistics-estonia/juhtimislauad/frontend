import React from 'react';
import Icon from '@mdi/react';
import { mdiHome, mdiChartBar, mdiViewDashboard, mdiHelpCircle, mdiTable } from '@mdi/js';
import { Menu } from 'react-bulma-components';
import './Sidenav.scss';
import { NavLink as ReactRouterNavLink } from 'react-router-dom';
import { useActiveLanguageCode } from '../../i18n/LanguageProvider';

const Sidenav = () => {
  const lang = useActiveLanguageCode();
  return (
    <Menu>
      <Menu.List>
        <li>
          <NavLink id="admin-home-link" exact to={`/${lang}/admin`}>
            <Icon path={mdiHome} size={1} />
            Töölaud
          </NavLink>
        </li>
        <li>
          <NavLink exact to={`/${lang}/admin/widgets`}>
            <Icon path={mdiChartBar} size={1} />
            Kuvamoodulid
          </NavLink>
          <ul>
            <NavLink id="all-widgets-link" exact to={`/${lang}/admin/widgets`}>
              Kõik kuvamoodulid
            </NavLink>
            <NavLink id="add-new-widget-link" exact to={`/${lang}/admin/widgets/new`}>
              Lisa uus
            </NavLink>
          </ul>
        </li>
        <li>
          <NavLink to={`/${lang}/admin/dashboards`}>
            <Icon path={mdiViewDashboard} size={1} />
            Juhtimislauad
          </NavLink>
          <ul>
            <NavLink id="all-dashboards-link" exact to={`/${lang}/admin/dashboards`}>
              Kõik juhtimislauad
            </NavLink>
            <NavLink id="add-new-dashboard-link" exact to={`/${lang}/admin/dashboards/new`}>
              Lisa uus
            </NavLink>
          </ul>
        </li>
        <li>
          <NavLink exact to={`/${lang}/admin/processlog`}>
            <Icon path={mdiTable} size={1} />
            Protsesside logi
          </NavLink>
        </li>
        <li>
          <a href="https://confluence.rmit.ee/display/SA0135/Adminliidese+kasutusjuhend">
            <Icon path={mdiHelpCircle} size={1} />
            Kasutusjuhend
          </a>
        </li>
      </Menu.List>
    </Menu>
  );
};

const NavLink = props => <ReactRouterNavLink activeClassName="is-active" {...props} />;

export default Sidenav;
