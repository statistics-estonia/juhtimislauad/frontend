import React from 'react';
import './Toggle.scss';

const Toggle = ({ label, checked = false, onChange, ...props }) => {
  const handleToggle = event => {
    event.preventDefault();
    onChange && onChange(!checked);
  };

  return (
    <div className="toggle-container">
      <label className="toggle" onClick={handleToggle} onKeyUp={e => e.key === 'Enter' && handleToggle(e)} tabIndex={1}>
        <input type="checkbox" checked={checked} onChange={e => onChange && onChange(e.target.checked)} {...props} />
        <span className="slider"></span>
      </label>

      {label && <span className="toggle-label">{label}</span>}
    </div>
  );
};

export default Toggle;
