import * as Observable from 'ol/Observable';
import 'ol/ol.css';
import React, { useEffect, useRef } from 'react';
import { useWidgetFilterValues } from '../../dashboard/WidgetProvider';
import { useFormatNumber } from '../../i18n/hooks';
import { useActiveLanguageCode } from '../../i18n/LanguageProvider';
import { isMobile } from '../../shared/utils';
import { useResponsiveContainer } from '../ResponsiveContainer';
import './Map.scss';
import { createTooltip, displayTooltip } from './MapTooltip';
import { addVectorLayer, createLegend, createMap, loadMapFeatures, removeLayers, removeLegend } from './MapUtils';

const Map = ({ isExpanded, widget, disableControls, onLayoutReady = () => {} }) => {
  const mapTarget = React.useCallback(node => {
    mapRef.current = createMap({ isExpanded, disableControls, target: node });
    setContainerNode(node);
  }, []);

  const [{ width, height }, setContainerNode] = useResponsiveContainer();

  const { diagram, shortname, unit } = widget;

  const mapRef = useRef(null);
  const mouseEvtKey = useRef(null);
  const layoutReadyCalled = React.useRef(false);
  const activeLanguageCode = useActiveLanguageCode();
  const formatNumber = useFormatNumber();
  const filterValues = useWidgetFilterValues()();
  const formatLegendNumber = value =>
    formatNumber(value, {
      style: 'decimal',
      minimumFractionDigits: widget.precisionScale || undefined
    });

  useEffect(() => {
    const map = mapRef.current;
    if (map) {
      (async () => {
        const mapFeatures = await loadMapFeatures(map, diagram.mapType, diagram.series, activeLanguageCode);
        initMapFeatures(map, mapFeatures);

        if (isExpanded) {
          map.set('name', shortname.replace(/&shy;/g, ''));
          map.set('filterValues', filterValues);
          map.set('lang', activeLanguageCode);
          initLegend(map);
          if (diagram.series.length > 0) {
            createTooltip(mapRef.current, unit);
            initTooltip(map);
          }
        }

        const layer = map.getLayers().getArray()[0];
        map.getView().fit(layer.getSource().getExtent(), {
          constrainResolution: false
        });
      })();

      map.on('rendercomplete', () => {
        if (onLayoutReady && !layoutReadyCalled.current) {
          onLayoutReady(widget);
          layoutReadyCalled.current = true;
        }
      });

      return () => {
        if (mouseEvtKey.current) Observable.unByKey(mouseEvtKey.current);
      };
    }
  }, [diagram.series]);

  useEffect(() => {
    if (mapRef.current) {
      const map = mapRef.current;
      map.setSize([width, isExpanded ? height : height - 80]);
      map.updateSize();
      const layer = map.getLayers().getArray()[0];
      if (layer)
        map.getView().fit(layer.getSource().getExtent(), {
          constrainResolution: false
        });
    }
  }, [width, height]);

  const initLegend = map => {
    removeLegend();
    if (map.get('maxValue') >= 0) createLegend(map, formatLegendNumber);
  };

  const initTooltip = map => {
    if (mouseEvtKey.current) Observable.unByKey(mouseEvtKey.current);
    mouseEvtKey.current = map.on(isMobile() ? 'click' : 'pointermove', evt => displayTooltip(evt, map, formatNumber));
  };

  return <div className="map" style={{ height: disableControls ? '90%' : '100%' }} ref={mapTarget} />;
};

const initMapFeatures = (map, features) => {
  removeLayers(map);
  addVectorLayer(map, features);
};

export default Map;
