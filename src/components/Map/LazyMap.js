import React, { lazy, Suspense } from 'react';

const Map = lazy(() => import('./Map'));

const LazyMap = props => (
  <Suspense fallback={null}>
    <Map {...props} />
  </Suspense>
);

export default LazyMap;
