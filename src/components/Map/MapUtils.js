import * as d3 from 'd3';
import { legendColor } from 'd3-svg-legend';
import Zoom from 'ol/control/Zoom';
import { click, never, pointerMove } from 'ol/events/condition';
import GeoJSON from 'ol/format/GeoJSON';
import { defaults as defaultInteractions } from 'ol/interaction';
import Select from 'ol/interaction/Select';
import VectorLayer from 'ol/layer/Vector';
import OlMap from 'ol/Map';
import * as proj from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import View from 'ol/View';
import { isMobile } from '../../shared/utils';
import { DISABLED_INTERACTIONS, mapColorScale, MAP_EXTENT_INITIAL, styleFunction, mapRange } from './MapConstants';

export const createMap = ({ isExpanded, disableControls = false, target } = {}) => {
  const extent = MAP_EXTENT_INITIAL;
  const projection = proj.get('EPSG:3301');

  const OlMapInstance = new OlMap({
    target,
    layers: [],
    controls: [],
    view: new View({
      projection: projection,
      extent: extent,
      maxResolution: 1100,
      minResolution: 100,
      center: [550000, 6520000]
    }),
    interactions: isExpanded
      ? defaultInteractions({ mouseWheelZoom: false, dragRotate: false, pinchRotate: false })
      : defaultInteractions(DISABLED_INTERACTIONS)
  });

  if (isExpanded && !disableControls) {
    const select = new Select({
      condition: isMobile() ? click : pointerMove,
      toggleCondition: never,
      hitTolerance: 5
    });
    select.on('select', onSelectFeature);
    OlMapInstance.addInteraction(select);
    OlMapInstance.addControl(new Zoom());
  }

  return OlMapInstance;
};

export const loadMapFeatures = async (mapInstance, baseMap, series, languageCode) => {
  const format = new GeoJSON();
  const values = [];
  const mapFeatures =
    baseMap === 'MK'
      ? languageCode === 'et'
        ? format.readFeaturesFromObject(await import('./basemaps/basemap_MK'))
        : format.readFeaturesFromObject(await import('./basemaps/basemap_MK_en'))
      : languageCode === 'et'
      ? format.readFeaturesFromObject(await import('./basemaps/basemap_KOV'))
      : format.readFeaturesFromObject(await import('./basemaps/basemap_KOV_en'));

  if (series.length > 0) {
    mapFeatures.forEach(feature => {
      const region = baseMap === 'MK' ? feature.get('FIRST_M_NI') : feature.get('O_NIMI_17');
      const regionData = series[0].series.filter(item =>
        item.abscissa ? item.abscissa.toUpperCase() === region.toUpperCase() : null
      );
      if (regionData.length > 0) {
        feature.set('abscissa', regionData[0].abscissa);
        feature.set('value', regionData[0].value);
        if (regionData[0].selected) feature.set('selected', true);
        values.push(regionData[0].value);
      } else feature.set('value', undefined);
    });
  }

  const emptyMap = values.length === 0;

  mapInstance.setProperties({
    minValue: emptyMap ? null : d3.min(values),
    maxValue: emptyMap ? null : d3.max(values),
    baseMap
  });

  return mapFeatures;
};

const countDecimals = (value, lang) => {
  const valueSplit = value.toString().split(lang === 'en' ? '.' : ',');
  if (Math.floor(value) !== value && valueSplit.length > 1) return valueSplit[1].length || 0;
  return 0;
};

export const createLegend = (map, labelFormatter = x => x) => {
  const legendLabelsCount = mapRange(map);
  const createLegendLabel = (start, end, index) => {
    const startLabel = labelFormatter(start);
    const endLabelDecimals = countDecimals(labelFormatter(end), map.get('lang'));
    const endLabel =
      index + 1 !== legendLabelsCount
        ? labelFormatter(end - (endLabelDecimals > 0 ? Math.pow(0.1, endLabelDecimals) : 1))
        : labelFormatter(end);
    return legendLabelsCount === 1 ? startLabel : `${startLabel} - ${endLabel}`;
  };

  const legend = legendColor()
    .classPrefix('map-legend-')
    .shape('circle')
    .shapePadding(5)
    .shapeRadius(6)
    .orient('vertical')
    .scale(mapColorScale(map))
    .labels(({ i, generatedLabels, labelDelimiter }) => {
      const labelValues = generatedLabels[i].split(labelDelimiter);
      return createLegendLabel(labelValues[0], labelValues[1], i);
    });

  d3.select(map.getViewport())
    .append('svg')
    .attr('class', 'legend_container')
    .style('height', `${legendLabelsCount * 26}px`)
    .append('g')
    .attr('class', 'legend')
    .call(legend);

  d3.select('.map-legend-legendCells').attr('transform', 'translate(10, 10)');
};

export const removeLegend = () => d3.select('.legend_container').remove();

export const addVectorLayer = (map, features) => {
  const vectorSource = new VectorSource({
    format: new GeoJSON(),
    features: features
  });

  const geojsonLayer = new VectorLayer({
    title: 'added Layer',
    source: vectorSource,
    style: feature => styleFunction(feature, map)
  });

  map.addLayer(geojsonLayer);
};

export const removeLayers = map => {
  const layers = map.getLayers().getArray();
  layers.forEach(layer => map.removeLayer(layer));
};

const onSelectFeature = evt => {
  evt.selected.forEach(selectedFeature => {
    const style = styleFunction(selectedFeature, evt.target.map_);
    style.setStroke(new Stroke({ color: '#f58fa9', width: 2 }));
    style.setFill(
      new Fill({
        color: style
          .getFill()
          .getColor()
          .replace(')', ', 1)')
          .replace('rgb', 'rgba')
      })
    );
    selectedFeature.setStyle(style);
  });
  evt.deselected.forEach(deSelectedFeature => {
    deSelectedFeature.setStyle(null);
  });
};
