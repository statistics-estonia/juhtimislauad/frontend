import Overlay from 'ol/Overlay';

export const createTooltip = (map, unit = '') => {
  map
    .getOverlays()
    .getArray()
    .forEach(item => map.removeOverlay(item));

  const tooltipContainer = document.createElement('div');
  tooltipContainer.id = 'map_tooltip';

  const overlay = new Overlay({
    element: tooltipContainer,
    offset: [10, 0],
    positioning: 'top-left',
    className: 'map_tooltip',
    stopEvent: false
  });

  overlay.set('unit', unit);
  map.addOverlay(overlay);
};

export const displayTooltip = (evt, map, valueFormatter = x => x) => {
  const pixel = evt.pixel;
  const feature = map.forEachFeatureAtPixel(pixel, feature => {
    if (feature.get('value') !== undefined) return feature;
  });

  const tooltip = map.getOverlays().getArray()[0];

  if (tooltip) {
    setTooltipPosition(tooltip, evt.coordinate);
    tooltip.element.style.display = feature ? '' : 'none';
  }
  if (feature && tooltip) {
    tooltip.element.innerHTML = `
      <div class="recharts-custom-tooltip">
        <p class="recharts-custom-tooltip-label">${feature.get('abscissa')}</p>
        <p class="recharts-custom-tooltip-item">${map.get('filterValues').join(', ')}
        : ${valueFormatter(feature.get('value'))} ${tooltip.get('unit')}</p>
      </div>
    `;
  }
};

const setTooltipPosition = (tooltip, coordinates) => {
  tooltip.setPosition(coordinates);
  tooltip.setOffset([0, 0]);
  const delta = getTooltipOffset(tooltip);
  tooltip.setOffset(delta);
};

const getTooltipOffset = overlay => {
  const overlayRect = overlay.element.getBoundingClientRect();
  const mapRect = overlay
    .getMap()
    .getTargetElement()
    .getBoundingClientRect();
  const offsetLeft = overlayRect.left - mapRect.left;
  const offsetRight = mapRect.right - overlayRect.right;
  const offsetTop = overlayRect.top - mapRect.top;
  const offsetBottom = mapRect.bottom - overlayRect.bottom;

  const delta = [0, 0];
  if (offsetLeft < 0) {
    delta[0] = -offsetLeft;
  } else if (offsetRight < 0) {
    delta[0] = -Math.abs(offsetRight);
  }
  if (offsetTop < 0) {
    delta[1] = -offsetTop;
  } else if (offsetBottom < 0) {
    delta[1] = -Math.abs(offsetBottom);
  }
  return delta;
};
