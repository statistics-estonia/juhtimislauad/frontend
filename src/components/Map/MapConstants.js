import Style from 'ol/style/Style';
import Stroke from 'ol/style/Stroke';
import Fill from 'ol/style/Fill';
import * as d3 from 'd3';
import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';

proj4.defs(
  'EPSG:3301',
  '+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 +lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
);
register(proj4);

const MAP_COLORS = ['rgb(204,176,255)', 'rgb(164,118,249)', 'rgb(105,57,197)', 'rgb(63,26,132)', 'rgb(48,21,99)'];

export const MAP_EXTENT_INITIAL = [292560.67, 6381157.44, 734255.01, 6658861.37];

export const DISABLED_INTERACTIONS = {
  dragRotate: false,
  doubleClickZoom: false,
  dragPan: false,
  pinchRotate: false,
  pinchZoom: false,
  keyboardPan: false,
  keyboardZoom: false,
  mouseWheelZoom: false,
  dragZoom: false,
  pointer: false,
  select: false
};

export const styleFunction = (feature, map) => {
  const isSelectedRegion = feature.get('selected');
  let style = new Style({
    stroke: new Stroke({
      color: isSelectedRegion ? '#f58fa9' : 'dimgray',
      width: isSelectedRegion ? 2 : 1
    }),
    zIndex: isSelectedRegion ? 999 : 1
  });
  const featureValue = feature.get('value');
  style.setFill(
    new Fill({
      color: featureValue !== undefined ? mapColorScale(map)(featureValue) : 'white'
    })
  );
  return style;
};

export const mapDomain = map => [map.get('minValue'), map.get('maxValue')];
export const mapRange = map => (map.get('minValue') === map.get('maxValue') ? 1 : 5);
export const mapColorRange = map => (mapRange(map) === 1 ? MAP_COLORS.slice(0, 1) : MAP_COLORS);

export const mapColorScale = map =>
  d3
    .scaleQuantile()
    .domain(mapDomain(map))
    .range(mapColorRange(map));
