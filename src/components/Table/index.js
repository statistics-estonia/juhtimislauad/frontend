import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import './Table.scss';

const Table = props => <ReactTable sortable={false} {...props} />;

export default Table;
