import React from 'react';
import { Button } from 'react-bulma-components';
import './IconButton.scss';

const IconButton = React.forwardRef(({ className, ...props }, ref) => (
  <Button type="button" ref={ref} color="white" className={`icon--button ${className}`} {...props} />
));

export default IconButton;
