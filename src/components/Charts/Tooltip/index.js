import React from 'react';
import { useWidgetFilterValues } from '../../../dashboard/WidgetProvider';
import { useFormatNumber } from '../../../i18n/hooks';
import { createDangerousHTML } from '../../../shared/utils';
import { ColoredIcon } from '../Legend';
import './styles.scss';

const Tooltip = ({
  active,
  payload,
  label,
  itemLabel,
  showRectangle = true,
  payloadValueFormatter,
  labelFormatter = x => x,
  noFiltersInTitle
}) => {
  const formatNumber = useFormatNumber();
  const filterValues = useWidgetFilterValues()();
  const tooltipLabel = [label && labelFormatter(label), ...(noFiltersInTitle ? [] : filterValues)]
    .filter(Boolean)
    .join(', ');

  const finalPayloadValueFormatter = payloadValueFormatter || formatNumber;

  const dataRows = (payload || []).map(item => (
    <li key={item.name} className="recharts-custom-tooltip-item">
      {showRectangle && (
        <>
          <ColoredIcon
            color={item.color || item.payload.fill}
            isDisabled={false}
            isBordered={item.payload.source === 'excel'}
          />{' '}
        </>
      )}
      <span
        className="recharts-legend-item-text"
        dangerouslySetInnerHTML={createDangerousHTML(
          `${itemLabel || item.name || item.dataKey || item.payload.name}: ${finalPayloadValueFormatter(
            item.value,
            item,
            payload,
            formatNumber
          )}`
        )}
      ></span>
    </li>
  ));

  return active && payload ? (
    <div className="recharts-custom-tooltip">
      <p className="recharts-custom-tooltip-label">{tooltipLabel}</p>
      <ul>{dataRows}</ul>
    </div>
  ) : null;
};

export default Tooltip;
