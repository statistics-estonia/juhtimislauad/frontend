import React, { memo } from 'react';
import {
  CartesianGrid,
  Legend as RCLegend,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Tooltip as RCTooltip,
  XAxis,
  YAxis
} from 'recharts';
import { DEFAULT_MARGIN } from '../../assets/styles';
import { useFormatDate, useFormatNumber } from '../../i18n/hooks';
import { LanguageContext } from '../../i18n/LanguageProvider';
import { useDisabledSeries } from '../../preferences/widget';
import { useLayoutReady, useYAxisWidth, useMedia } from '../../shared/hooks';
import { getLegendFilter, makeCartesians, useSerializedFilters } from '../../shared/series';
import { useResponsiveContainer } from '../ResponsiveContainer';
import Legend from './Legend';
import './styles.scss';
import Tooltip from './Tooltip';
import { getISOWeekYear } from 'date-fns';

export const makeChart = ({
  transformAllSeries,
  className,
  chartComponent: Chart,
  cartesianComponent: Cartesian,
  makeAppearanceProps,
  allowDuplicatedCategory = true,
  axes = 'cartesian'
}) =>
  memo(({ widget, isExpanded, isEmbedded, style, disableAnimation, onLayoutReady = () => {} }) => {
    const { translate } = React.useContext(LanguageContext);
    const [isSeriesDisabled] = useDisabledSeries();
    const formatDate = useFormatDate();
    const xAxisTickFormatter = useXAxisTickFormatter(
      widget.timePeriod === 'WEEK' && !isExpanded ? 'YEAR' : widget.timePeriod
    );
    const serializeFilters = useSerializedFilters();
    const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);

    const appendExpectationTranslation = value => `${value} ${translate('stat.chart.expectation')}`;

    const { diagram: { filters: widgetFilters = [], series: allSeries = [] } = {}, unit, shortname } = widget;

    const finalSeries = React.useMemo(() => transformAllSeries(allSeries, widgetFilters), [widget]);
    const allCartesians = React.useMemo(() => {
      const legendFilter = getLegendFilter(widgetFilters);

      return makeCartesians(allSeries, widgetFilters, legendFilter && legendFilter.name).map(
        ({ index, dataKey, filters, isExpectation }) => {
          const rawName = filters ? serializeFilters(filters) : shortname;
          const name = isExpectation ? appendExpectationTranslation(rawName) : rawName;

          return {
            key: dataKey,
            dataKey,
            name,
            filters,
            isExpectation,
            appearance: makeAppearanceProps({ index, isExpectation, isExpanded })
          };
        }
      );
    }, [widget]);

    const filteredCartesians = allCartesians.filter(({ dataKey }) => !isSeriesDisabled(dataKey));
    const tooltipPayloadValueFormatter = (value, item, payload, defaultFormatter) =>
      `${defaultFormatter(value)} ${unit ? unit : ``}`;

    const callbackRef = React.useCallback(node => {
      setContainerNode(node);
      setYAxisNode(node);
    }, []);
    const [{ width, height }, setContainerNode] = useResponsiveContainer(isEmbedded);
    const [yAxisWidth, setYAxisNode] = useYAxisWidth([height, finalSeries]);
    const margin = { ...DEFAULT_MARGIN, top: unit ? 40 : DEFAULT_MARGIN.top };
    const dates = finalSeries.map(serie => serie.date);
    const xAxisTicks =
      widget.timePeriod === 'WEEK'
        ? isExpanded
          ? dates
          : dates.filter(
              (date, index) =>
                !(index > 0 && getISOWeekYear(new Date(dates[index - 1])) === getISOWeekYear(new Date(date)))
            )
        : dates.filter(date => new Date(date).getMonth() === 0);
    useLayoutReady(() => (axes === 'cartesian' ? yAxisWidth > 0 : height > 0), onLayoutReady, [height, yAxisWidth]);

    return (
      <div className={`chart${isEmbedded ? '' : ' is-clipped'} ${className}`} style={style} ref={callbackRef}>
        <Chart data={finalSeries} margin={margin} width={width} height={height}>
          {axes === 'cartesian' && [
            <XAxis
              dataKey="date"
              allowDuplicatedCategory={allowDuplicatedCategory}
              interval={isExpanded && !isMobile && widget.timePeriod !== 'WEEK' ? 0 : 'preserveStartEnd'}
              tickFormatter={xAxisTickFormatter}
              key="xAxis"
              ticks={xAxisTicks}
            />,
            <YAxis
              width={yAxisWidth}
              tick={<YAxisTick unit={unit} type="number" />}
              key="yAxis"
              label={<AxisLabel value={unit} axis="y" />}
            />,
            <CartesianGrid stroke="#eee" vertical={false} width={width} x={0} key="cartesianGrid" />,

            <RCTooltip
              key="tooltip"
              noFiltersInTitle
              labelFormatter={date =>
                date
                  ? formatDate(date, {
                      periodType: widget.timePeriod === 'WEEK' ? 'WEEK_LONG' : widget.timePeriod
                    })
                  : ''
              }
              content={isExpanded ? <Tooltip payloadValueFormatter={tooltipPayloadValueFormatter} /> : () => null}
            />
          ]}

          {axes === 'polar' && [
            <PolarGrid key="polarGrid" />,
            <PolarAngleAxis dataKey="abscissa" key="angleAxis" />,
            <PolarRadiusAxis
              tick={<PolarAxisTick fill="#929292" />}
              orientation="middle"
              angle={90}
              key="radiusAxis"
            />,
            <RCTooltip
              noFiltersInTitle
              key="tooltip"
              content={isExpanded ? <Tooltip payloadValueFormatter={tooltipPayloadValueFormatter} /> : () => null}
            />
          ]}

          {isExpanded && (
            <RCLegend
              align="left"
              isEmbedded={isEmbedded}
              payload={allCartesians}
              content={<Legend />}
              widgetFilters={widgetFilters}
            />
          )}

          {filteredCartesians.map(({ appearance, ...props }) => (
            <Cartesian
              {...props}
              {...appearance}
              isAnimationActive={!disableAnimation && process.env.NODE_ENV === 'production'}
            />
          ))}
        </Chart>
      </div>
    );
  });

const useXAxisTickFormatter = timePeriod => {
  const formatDate = useFormatDate();

  return value =>
    formatDate(value, {
      periodType: timePeriod === 'WEEK' ? timePeriod : 'YEAR'
    });
};

export const YAxisTick = ({ x, y, payload, height, type, isExpanded, dy = '-0.355em', textAnchor = 'end' }) => {
  const formatNumber = useFormatNumber(isExpanded);
  return (
    <text
      height={height}
      x={y}
      y={y}
      dy={16}
      textAnchor={textAnchor}
      stroke="none"
      className="recharts-text recharts-cartesian-axis-tick-value"
      fill="#666"
    >
      <tspan x={x} dy={dy}>
        {type === 'number' && formatNumber(payload.value)}
        {type === 'category' && payload.value}
      </tspan>
    </text>
  );
};

const PolarAxisTick = ({ fill, textAnchor, payload, radius, transform, x, y, stroke }) => (
  <text
    x={y}
    y={y}
    transform={transform}
    textAnchor={textAnchor}
    stroke={stroke}
    radius={radius}
    className="recharts-text recharts-polar-radius-axis-tick-value"
    fill={fill}
  >
    <tspan x={x} dy={5}>
      {payload.value}
    </tspan>
  </text>
);

export const AxisLabel = props => {
  const { viewBox, value, axis } = props;
  const pos = calculateAxisLabelPosition(viewBox, axis);

  return (
    <text
      className="recharts-text recharts-label"
      fill="#666"
      stroke="none"
      x={pos.x}
      y={pos.y}
      textAnchor={axis === 'y' ? 'start' : 'end'}
    >
      <tspan x={pos.x} dy="0.355em">
        {value}
      </tspan>
    </text>
  );
};

const calculateAxisLabelPosition = (viewBox, axis) => {
  const { x, y, width, height } = viewBox;

  return axis === 'y'
    ? {
        x: x + width / 2,
        y: y - 30
      }
    : {
        x: x + width,
        y: y + height + 10
      };
};
