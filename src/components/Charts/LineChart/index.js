import { Line, LineChart as RCLineChart } from 'recharts';
import { COLOR_SCALE, COLOR_SCALE_EXPECTED } from '../../../assets/styles';
import { combineSeries } from '../../../shared/series';
import { makeChart } from '../chart';
import './styles.scss';

const makeLineAppearanceProps = ({ index, isExpanded, isExpectation }) => {
  const colorScale = isExpectation ? COLOR_SCALE_EXPECTED : COLOR_SCALE;
  const color = colorScale[index % colorScale.length];

  return {
    stroke: color,
    dot: isExpanded ? { stroke: color, strokeWidth: 2, fill: color } : false,
    activeDot: { stroke: color, strokeWidth: 2, fill: isExpanded ? color : undefined, r: isExpanded ? 4 : 2 },
    type: 'linear',
    strokeDasharray: isExpectation ? '5 5' : undefined,
    color
  };
};

const LineChart = makeChart({
  transformAllSeries: combineSeries,
  className: 'line-chart',
  chartComponent: RCLineChart,
  cartesianComponent: Line,
  makeAppearanceProps: makeLineAppearanceProps,
  passChartDataSeparately: true,
  allowDuplicatedCategory: false
});

export default LineChart;
