import { groupBy, map, omit } from 'lodash';
import React from 'react';
import { Heading } from 'react-bulma-components';
import { Surface, Symbols } from 'recharts';
import { COLOR_THEME } from '../../assets/styles';
import { LanguageContext } from '../../i18n/LanguageProvider';
import { useDisabledSeries } from '../../preferences/widget';
import { useSerializedFilters } from '../../shared/series';
import { createDangerousHTML } from '../../shared/utils';

const Legend = ({ widgetFilters, payload: allCartesians, isEmbedded }) => {
  const { translate } = React.useContext(LanguageContext);
  const serializeFilters = useSerializedFilters();

  const [isSeriesDisabled, setIsSeriesDisabled] = useDisabledSeries();
  const toggleSeries = dataKey => setIsSeriesDisabled(dataKey, !isSeriesDisabled(dataKey));

  const appendExpectationTranslation = value => `${value} ${translate('stat.chart.expectation')}`;

  const legendFilter = getLegendFilter(widgetFilters);
  const filterSets = legendFilter
    ? groupBy(allCartesians, ({ filters }) => serializeFilters(omit(filters, legendFilter.name)))
    : [allCartesians];

  const moreThanOneFilterSet = Object.keys(filterSets).length > 1;

  return (
    <div style={{ paddingTop: isEmbedded ? '1rem' : '2.5rem' }}>
      {map(filterSets, (cartesians, filterSetName) => (
        <div key={filterSetName} style={{ marginBottom: '0.7rem' }}>
          {moreThanOneFilterSet && (
            <Heading size={7} style={{ marginBottom: '0.2rem' }}>
              {filterSetName}
            </Heading>
          )}

          {cartesians.map(({ appearance: { color }, dataKey, name, isExpectation, filters }) => {
            const isDisabled = isSeriesDisabled(dataKey);
            const filterName = filters && legendFilter && filters[legendFilter.name];
            const finalName = filterName
              ? isExpectation
                ? appendExpectationTranslation(filterName)
                : filterName
              : name;

            return (
              <div
                key={dataKey}
                className={`recharts-legend-item ${isDisabled ? 'inactive' : ''}`}
                onClick={() => toggleSeries(dataKey)}
                style={{ color: COLOR_THEME[isDisabled ? 'greyLight' : 'grey'] }}
              >
                <ColoredIcon color={color} isDisabled={isDisabled} isBordered={isExpectation} />
                &nbsp;
                <span className="recharts-legend-item-text" dangerouslySetInnerHTML={createDangerousHTML(finalName)} />
              </div>
            );
          })}
        </div>
      ))}
    </div>
  );
};

const getLegendFilter = (filters = []) => filters.filter(filter => filter.type === 'LEGEND').shift();

export const StaticLegend = ({ payload }) => {
  return (
    <div style={{ paddingTop: '2.5rem' }}>
      {payload.map(({ value, color }) => {
        return (
          <div key={value} className={'recharts-legend-item'} style={{ color: COLOR_THEME['grey'] }}>
            <ColoredIcon color={color} />
            &nbsp;<span className="recharts-legend-item-text">{value}</span>
          </div>
        );
      })}
    </div>
  );
};

export const ColoredIcon = ({ color, isDisabled, isBordered }) => (
  <Surface width={12} height={12} className={isBordered ? 'bordered' : null}>
    <Symbols type="circle" cx={6} cy={6} size={100} fill={color} />
    {isDisabled && <Symbols type="circle" cx={6} cy={6} size={100} fill="#FFF" />}
  </Surface>
);

export default Legend;
