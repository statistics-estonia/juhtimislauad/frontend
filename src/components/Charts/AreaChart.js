import { Area, AreaChart as RCAreaChart } from 'recharts';
import { COLOR_SCALE } from '../../assets/styles';
import { combineSeries } from '../../shared/series';
import { makeChart } from './chart';

const makeAreaAppearanceProps = ({ index, isExpanded }) => {
  const color = COLOR_SCALE[index % COLOR_SCALE.length];

  return {
    stroke: color,
    fill: color,
    stackId: '1',
    dot: isExpanded ? { stroke: color, strokeWidth: 2, fill: color } : false,
    activeDot: { stroke: color, strokeWidth: 2, fill: isExpanded ? color : undefined, r: isExpanded ? 4 : 2 },
    type: 'linear',
    color
  };
};

const AreaChart = makeChart({
  transformAllSeries: combineSeries,
  className: 'area-chart',
  chartComponent: RCAreaChart,
  cartesianComponent: Area,
  makeAppearanceProps: makeAreaAppearanceProps,
  passChartDataSeparately: true,
  allowDuplicatedCategory: false
});

export default AreaChart;
