import { Radar, RadarChart as RCRadarChart } from 'recharts';
import { COLOR_SCALE, COLOR_SCALE_EXPECTED } from '../../../assets/styles';
import { combineSeries } from '../../../shared/series';
import { makeChart } from '../chart';

const makeRadarAppearanceProps = ({ index, isExpanded, isExpectation }) => {
  const colorScale = isExpectation ? COLOR_SCALE_EXPECTED : COLOR_SCALE;
  const color = colorScale[index % colorScale.length];

  return {
    stroke: color,
    fill: 'rgba(255,255,255, 0)',
    activeDot: { stroke: color, strokeWidth: 2, fill: isExpanded ? color : undefined, r: isExpanded ? 4 : 2 },
    color
  };
};

export const RadarChart = makeChart({
  transformAllSeries: combineSeries,
  className: 'radar-chart',
  chartComponent: RCRadarChart,
  cartesianComponent: Radar,
  makeAppearanceProps: makeRadarAppearanceProps,
  axes: 'polar'
});

export default RadarChart;
