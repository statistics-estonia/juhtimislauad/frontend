import React from 'react';
import { Cell, Legend, Pie, PieChart as RCPieChart, Sector, Tooltip as RCTooltip } from 'recharts';
import { colorToRGBA, COLOR_SCALE, DEFAULT_MARGIN } from '../../../assets/styles';
import { useFormatNumber } from '../../../i18n/hooks';
import { useLayoutReady } from '../../../shared/hooks';
import { getFirstSeriesData } from '../../../shared/series';
import { useResponsiveContainer } from '../../ResponsiveContainer';
import Tooltip from '../Tooltip';
import { StaticLegend } from '../Legend';
import './styles.scss';

const PieChart = ({ widget, isExpanded, onLayoutReady = () => {} }) => {
  const formatNumber = useFormatNumber();

  const { diagram } = widget;
  const dataSetForChart = getFirstSeriesData(diagram.series);
  const tooltipPayloadValueFormatter = (value, item, payload, defaultFormatter) =>
    `${defaultFormatter(value)} ${widget.unit ? widget.unit : ``}`;
  const [activeSector, setActiveSector] = React.useState(null);

  const [{ width, height }, setNode] = useResponsiveContainer();
  const ref = React.useCallback(setNode, []);

  useLayoutReady(() => height > 0, onLayoutReady, [height]);

  const onMouseEnter = (data, index) => {
    if (isExpanded) setActiveSector(index);
  };
  const onMouseLeave = () => {
    if (isExpanded) setActiveSector(null);
  };

  return (
    <div className="chart pie-chart is-clipped" ref={ref}>
      <RCPieChart margin={DEFAULT_MARGIN} width={width} height={height}>
        <Pie
          data={dataSetForChart}
          dataKey="value"
          nameKey="abscissa"
          innerRadius="45%"
          startAngle={450}
          endAngle={0}
          isAnimationActive={false}
          label={({ percent }) =>
            formatNumber(percent, { style: 'percent', minimumFractionDigits: 1, maximumFractionDigits: 1 })
          }
          activeIndex={activeSector}
          activeShape={<ActiveShape />}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
        >
          {dataSetForChart.map(({ abscissa }, index) => (
            <Cell key={abscissa} fill={COLOR_SCALE[index % COLOR_SCALE.length]} />
          ))}
        </Pie>
        {isExpanded && <Legend content={<StaticLegend />} align="left" iconType="circle" iconSize={12} />}
        <RCTooltip
          content={isExpanded ? <Tooltip payloadValueFormatter={tooltipPayloadValueFormatter} /> : () => null}
        />
      </RCPieChart>{' '}
    </div>
  );
};

const ActiveShape = ({ cx, cy, innerRadius, outerRadius, startAngle, endAngle, fill }) => {
  const [r, g, b] = colorToRGBA(fill);
  const activeFill = `rgba(${r}, ${g}, ${b}, 0.5)`;

  return (
    <g>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={activeFill}
      />
    </g>
  );
};

export default PieChart;
