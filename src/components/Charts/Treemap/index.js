import { sumBy } from 'lodash';
import React from 'react';
import { Tooltip as RCTooltip, Treemap as RCTreemap } from 'recharts';
import { COLOR_THEME, DEFAULT_MARGIN } from '../../../assets/styles';
import { useFormatNumber } from '../../../i18n/hooks';
import { useLayoutReady } from '../../../shared/hooks';
import { getFirstSeriesData } from '../../../shared/series';
import { useResponsiveContainer } from '../../ResponsiveContainer';
import Tooltip from '../Tooltip';
import './styles.scss';
import * as d3 from 'd3';
import { useActiveLanguageCode } from '../../../i18n/LanguageProvider';

const seriesToChartData = series =>
  getFirstSeriesData(series)
    .map(toNameValuePair)
    .filter(isNotZero);
const toNameValuePair = ({ abscissa, value }) => ({ name: abscissa, value });
const isNotZero = ({ value }) => value !== 0;

const Treemap = ({ widget: { diagram, shortname, unit }, isExpanded, onLayoutReady = () => {} }) => {
  const formatNumber = useFormatNumber();
  const dataSetForChart = React.useMemo(() => seriesToChartData(diagram.series), [diagram]);

  /* for some reason recharts Treemap doesn't behave as other charts, hence the setTimeout */
  const ref = React.useCallback(node => {
    setNode(node);
  }, []);
  const [{ width, height }, setNode] = useResponsiveContainer();
  useLayoutReady(() => height > 0, () => setTimeout(onLayoutReady, 0), [height]);

  const tooltipPayloadValueFormatter = (value, item, payload, defaultFormatter) => {
    return `${defaultFormatter(value)} ${unit ? unit : ``} (${formatNumber(value / total, {
      style: 'percent',
      minimumFractionDigits: 1
    })})`;
  };
  const total = sumBy(dataSetForChart, 'value');
  const isIE = /MSIE|Trident/.test(window.navigator.userAgent);

  return (
    <div className="chart treemap" ref={ref}>
      <RCTreemap
        width={width}
        height={height}
        margin={DEFAULT_MARGIN}
        data={dataSetForChart}
        dataKey="value"
        stroke="#fff"
        fill={COLOR_THEME.primary}
        isAnimationActive={false}
        content={isIE ? undefined : <CustomContent total={total} />}
      >
        <RCTooltip
          labelFormatter={() => shortname.replace(/&shy;/g, '')}
          content={
            isExpanded ? (
              <Tooltip payloadValueFormatter={tooltipPayloadValueFormatter} showRectangle={false} />
            ) : (
              () => null
            )
          }
        />
      </RCTreemap>
    </div>
  );
};

const CustomContent = props => {
  const { depth, x, y, width, height, color, name, total, value } = props;
  const formatNumber = useFormatNumber();
  const lang = useActiveLanguageCode();
  const textRef = React.useRef(null);

  React.useLayoutEffect(() => {
    if (textRef.current && width > 0) d3.select(textRef.current).call(ellipsifyLabel);
  }, [name, lang, width]);

  return (
    <g>
      <rect
        x={x}
        y={y}
        width={width}
        height={height}
        style={{
          fill: color,
          stroke: '#fff',
          strokeWidth: 2 / (depth + 1e-10),
          strokeOpacity: 1 / (depth + 1e-10)
        }}
      />
      {depth === 1 ? (
        <text
          width={width}
          ref={textRef}
          x={x + width / 2}
          y={y + height / 2 + 7}
          textAnchor="middle"
          fill="#fff"
          fontSize={14}
        >
          {`${name} ${formatNumber(value / total, {
            style: 'percent',
            minimumFractionDigits: 1
          })}`}
        </text>
      ) : null}
    </g>
  );
};

const ellipsifyLabel = text => {
  if (text.node() && text.node().textContent) {
    const words = text.text().split(/\s+/);
    const ellipsis = text
      .text('')
      .append('tspan')
      .attr('class', 'elip')
      .text('...');
    const width = parseFloat(text.attr('width')) - ellipsis.node().getComputedTextLength();
    const numWords = words.length;
    const tspan = text.insert('tspan', ':first-child').text(words.join(' '));

    while (tspan.node().getComputedTextLength() > width && words.length) {
      words.pop();
      tspan.text(words.join(' '));
    }

    if (words.length === numWords) {
      ellipsis.remove();
    }
  }
};

export default Treemap;
