import React from 'react';
import { Bar, BarChart, CartesianGrid, Legend, Tooltip as RCTooltip, XAxis, YAxis } from 'recharts';
import { COLOR_SCALE, DEFAULT_MARGIN } from '../../../assets/styles';
import { useFormatNumber } from '../../../i18n/hooks';
import { useLayoutReady, useYAxisWidth, useMedia } from '../../../shared/hooks';
import { getLegendFilter } from '../../../shared/series';
import { YAxisTick, AxisLabel } from '../../Charts/chart';
import Tooltip from '../../Charts/Tooltip';
import { StaticLegend } from '../Legend';
import { useResponsiveContainer } from '../../ResponsiveContainer';

const PopulationPyramid = ({ widget, isExpanded, isEmbedded, disableAnimation, onLayoutReady = () => {} }) => {
  const { diagram } = widget;

  const formatNumber = useFormatNumber();

  const dataSetForChart = React.useMemo(
    () => makePyramidData(diagram.series, getLegendFilter(diagram.filters).name),
    [widget]
  );
  const dataKeys = dataSetForChart[0] ? Object.keys(dataSetForChart[0]).slice(0, -1) : [];

  const tooltipPayloadValueFormatter = (value, item, payload, defaultFormatter) =>
    `${defaultFormatter(Math.abs(value))} ${widget.unit ? widget.unit : ``}`;

  const callbackRef = React.useCallback(node => {
    setContainerNode(node);
    setYAxisNode(node);
  }, []);
  const [{ width, height }, setContainerNode] = useResponsiveContainer(isEmbedded);
  const [yAxisWidth, setYAxisNode] = useYAxisWidth([height, dataSetForChart]);

  useLayoutReady(() => yAxisWidth > 0, onLayoutReady, [height, yAxisWidth]);

  const isMobile = useMedia(['(max-width: 768px)', '(min-width: 769px)'], [true, false], false);

  return (
    <div className={`chart pop-pyramid${isEmbedded ? '' : ' is-clipped'}`} ref={callbackRef}>
      <BarChart
        margin={DEFAULT_MARGIN}
        data={dataSetForChart}
        layout="vertical"
        width={width}
        height={height}
        stackOffset="sign"
      >
        <XAxis
          label={<AxisLabel value={widget.unit} axis="x" />}
          type="number"
          tickFormatter={v => formatNumber(Math.abs(v), { style: 'decimal' })}
        />
        />
        <YAxis
          yAxisId="leftAxis"
          orientation="left"
          type="category"
          dataKey="abscissa"
          reversed={true}
          tick={<YAxisTick type="category" dy="0.3em" />}
          width={yAxisWidth}
          interval={isExpanded && !isMobile ? 0 : 'preserveStartEnd'}
        />
        <YAxis
          yAxisId="rightAxis"
          orientation="right"
          type="category"
          dataKey="abscissa"
          reversed={true}
          tick={<YAxisTick textAnchor="start" type="category" dy="0.3em" />}
          width={yAxisWidth}
          interval={isExpanded && !isMobile ? 0 : 'preserveStartEnd'}
        />
        <CartesianGrid horizontal={false} width={width} />
        {dataKeys.map((key, i) => (
          <Bar
            yAxisId="leftAxis"
            stackId="1"
            key={key}
            dataKey={key}
            fill={COLOR_SCALE[i]}
            isAnimationActive={!disableAnimation}
          />
        ))}
        <RCTooltip
          payloadValueFormatter={tooltipPayloadValueFormatter}
          content={isExpanded ? <Tooltip /> : () => null}
        />
        {isExpanded && <Legend content={<StaticLegend />} align="left" iconType="circle" iconSize={12} />}
      </BarChart>
    </div>
  );
};

const makePyramidData = (series = [], legendName) =>
  series[0]
    ? series[0].series.map((d, i) => ({
        [series[0].filters[legendName]]: -Math.abs(series[0].series[i].value),
        ...(series[1]?.series[i] && { [series[1].filters[legendName]]: Math.abs(series[1].series[i].value) }),
        abscissa: series[0].series[i].abscissa,
      }))
    : [];

export default PopulationPyramid;
