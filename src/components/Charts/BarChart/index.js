import React from 'react';
import { Bar, BarChart as RCBarChart, Rectangle } from 'recharts';
import { COLOR_SCALE, COLOR_SCALE_EXPECTED } from '../../../assets/styles';
import { combineSeries } from '../../../shared/series';
import { makeChart } from '../chart';
import './styles.scss';

const makeBarAppearanceProps = ({ index, isExpectation }) => {
  const colorScale = isExpectation ? COLOR_SCALE_EXPECTED : COLOR_SCALE;
  const color = colorScale[index % colorScale.length];

  return {
    fill: color,
    isAnimationActive: false,
    className: 'expectation',
    shape: CustomBar,
    color,
    stackId: index
  };
};

const CustomBar = ({ source, ...props }) => (
  <Rectangle {...props} className={source === 'excel' ? 'expectation' : null} />
);

const StyledBarChart = props => <RCBarChart barGap={0} {...props} />;

const BarChart = makeChart({
  transformAllSeries: combineSeries,
  className: 'bar-chart',
  chartComponent: StyledBarChart,
  cartesianComponent: Bar,
  makeAppearanceProps: makeBarAppearanceProps
});

export default BarChart;

const makeStackedAppearanceProps = (...args) => ({
  ...makeBarAppearanceProps(...args),
  stackId: 0
});

export const StackedBarChart = makeChart({
  transformAllSeries: combineSeries,
  className: 'stacked-bar-chart',
  chartComponent: StyledBarChart,
  cartesianComponent: Bar,
  makeAppearanceProps: makeStackedAppearanceProps
});
