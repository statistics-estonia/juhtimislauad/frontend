import React from 'react';
import { useDescriptiveWidgetName, useGetWidget, useWidget, useWidgetId } from '../dashboard/WidgetProvider';
import { useScreenshotUrl } from '../embed/hooks';
import { useFormatDate } from '../i18n/hooks';
import { LanguageContext } from '../i18n/LanguageProvider';
import { useDisabledSeries } from '../preferences/widget';
import { hasExpectations } from '../shared/series';
import TableMaker from './TableMaker';
import {
  addHeaderAndFooter,
  and,
  cartesianIsExpectation,
  cartesianIsNotExpectation,
  concatTables,
  downloadAsCsv,
  downloadAsExcelSpreadsheet,
  downloadAsJson,
  hideIfEmptyBody,
  rowIsExcel,
  rowIsStat,
  tableToSheet
} from './utils';

export const ExportContext = React.createContext({});

const ExportProvider = ({ children }) => {
  const [isSeriesDisabled] = useDisabledSeries();
  const { translate } = React.useContext(LanguageContext);
  const widgetId = useWidgetId();
  const getWidget = useGetWidget(widgetId);
  const getDescriptiveName = useDescriptiveWidgetName();
  const widget = useWidget();
  const formatDate = useFormatDate();

  const downloadExcelSpreadsheet = async languageCode => {
    const widget = await getWidgetInLanguage(languageCode);
    const table = await makeTables(widget, languageCode);
    const finalTable = addTitleAndSource(table, widget, languageCode);

    return tableToSheet(finalTable).then(sheet =>
      downloadAsExcelSpreadsheet(sheet, widget.shortname.replace(/&shy;/g, ''))
    );
  };

  const downloadCsv = async languageCode => {
    const widget = await getWidgetInLanguage(languageCode);
    const table = await makeTables(widget, languageCode);

    return tableToSheet(table).then(sheet => downloadAsCsv(sheet, widget.shortname.replace(/&shy;/g, '')));
  };

  const downloadJson = async languageCode => {
    const {
      lang,
      shortname,
      name,
      description,
      note,
      source,
      dataUpdatedAt,
      statCubes = [],
      methodsLinks,
      statisticianJobLinks: statActivityLinks,
      graphType,
      diagram: { series }
    } = await getWidgetInLanguage(languageCode);

    const cleanedUpShortname = shortname.replace(/&shy;/g, '');

    const widget = {
      lang,
      shortname: cleanedUpShortname,
      name,
      description,
      note,
      source,
      dataUpdatedAt,
      cubes: statCubes.map(({ cube }) => cube),
      methodsLinks,
      statActivityLinks,
      graphType,
      diagram: { series }
    };

    return downloadAsJson(widget, cleanedUpShortname);
  };

  const getWidgetInLanguage = async languageCode =>
    languageCode === widget.lang.toLowerCase() ? widget : getWidget(languageCode);

  const makeTables = (widget, languageCode) => {
    const abscissaFormatter = (value, index, row) => (row.date ? formatDateUsingLocale(value) : value);
    const formatDateUsingLocale = value => formatDate(value, { locale: languageCode });
    const valueFormatter = value => (isNaN(value) ? '' : value);
    const filterValueFormatter = (value, index, filter) => (filter.time ? formatDateUsingLocale(value) : value);

    const formatters = { abscissaFormatter, valueFormatter, filterValueFormatter };

    const statTable = makeStatTable(widget, formatters);
    const excelTable = hasExpectations(widget.diagram.series) ? makeExcelTable(widget, languageCode, formatters) : [];

    return concatTables(hideIfEmptyBody(statTable), hideIfEmptyBody(excelTable));
  };

  const makeStatTable = (widget, { abscissaFormatter, valueFormatter, filterValueFormatter }) =>
    new TableMaker(widget, {
      rowFilter: rowIsStat,
      cartesianFilter: and(isCartesianVisible, cartesianIsNotExpectation),
      filterValueFormatter,
      abscissaFormatter,
      valueFormatter
    }).getTable();

  const makeExcelTable = (widget, languageCode, { abscissaFormatter, valueFormatter, filterValueFormatter }) =>
    new TableMaker(widget, {
      rowFilter: rowIsExcel,
      cartesianFilter: and(isCartesianVisible, cartesianIsExpectation),
      filterValueFormatter: (value, index, filter) => {
        const formattedValue = filterValueFormatter(value, index, filter);
        return index === 0 ? appendExpectationTranslation(formattedValue, languageCode) : formattedValue;
      },
      abscissaFormatter,
      valueFormatter
    }).getTable();

  const appendExpectationTranslation = (value, languageCode) =>
    `${value} ${translate('stat.chart.expectation', { languageCode })}`;

  const isCartesianVisible = ({ dataKey }) => !isSeriesDisabled(dataKey);

  const addTitleAndSource = (table, widget, languageCode) =>
    addHeaderAndFooter(
      table,
      getDescriptiveName(widget, languageCode),
      `${translate('stat.chart.source', { languageCode })}: ${widget.source}`
    );

  const value = {
    downloadExcelSpreadsheet,
    downloadCsv,
    downloadJson
  };

  return <ExportContext.Provider value={value}>{children}</ExportContext.Provider>;
};

export default ExportProvider;

const A4_SHORT_SIDE_IN_MM = 210;
const A4_LONG_SIDE_IN_MM = 297;
const INCH_IN_MM = 25.4;

export const usePrint = (dpi = 100) => {
  const screenshotUrl = useScreenshotUrl({
    width: mmToPx(A4_LONG_SIDE_IN_MM, dpi),
    height: mmToPx(A4_SHORT_SIDE_IN_MM, dpi)
  });

  return () => {
    const container = document.createElement('div');
    container.style.width = 0;
    container.style.height = 0;
    container.style.overflow = 'hidden';

    const image = new Image();
    image.src = screenshotUrl;
    image.style.width = '100%';
    image.onload = () => {
      if (isFirefox) iframe.contentWindow.document.write(new XMLSerializer().serializeToString(image));

      iframe.contentWindow.document.execCommand('print', false, null) || iframe.contentWindow.print();
      document.body.removeChild(container);
    };

    const iframe = document.createElement('iframe');

    container.appendChild(iframe);
    document.body.appendChild(container);
    iframe.contentWindow.document.body.appendChild(image);
  };
};

const isFirefox = navigator && navigator.userAgent && navigator.userAgent.includes('Firefox');

const mmToPx = (mm, dpi) => Math.round((mm / INCH_IN_MM) * dpi);
