import { mdiFileExcel, mdiFileImage, mdiFileTable, mdiJson } from '@mdi/js';
import Icon from '@mdi/react';
import React from 'react';
import { Button, Form, Heading } from 'react-bulma-components';
import Popup from 'reactjs-popup';
import Checkbox from '../components/Checkbox';
import { useScreenshotUrl } from '../embed/hooks';
import { LanguageContext, Translate } from '../i18n/LanguageProvider';
import { ExportContext } from './ExportProvider';

const ExportPopup = ({ children }) => {
  const { activeLanguageCode, languages } = React.useContext(LanguageContext);
  const [exportLang, setExportLang] = React.useState(activeLanguageCode);
  const { downloadExcelSpreadsheet, downloadCsv, downloadJson } = React.useContext(ExportContext);
  const screenshotUrl = useScreenshotUrl({ languageCode: exportLang });

  const onExportLangChange = e => setExportLang(e.target.value);

  return (
    <Popup
      trigger={children}
      contentStyle={{ width: '9rem', padding: '1rem' }}
      position="bottom right"
      closeOnDocumentClick
    >
      <React.Fragment>
        <Heading size={4}>
          <Translate>stat.share.download</Translate>
        </Heading>

        {languages.map(languageCode => (
          <LanguageCheckbox
            key={languageCode}
            type="radio"
            value={languageCode}
            label={<Translate>{`stat.lang.${languageCode}`}</Translate>}
            onChange={onExportLangChange}
            checked={languageCode === exportLang}
          />
        ))}

        <ExportButton label="PNG" icon={mdiFileImage} href={screenshotUrl} renderAs="a" />
        <ExportButton label="XLS" icon={mdiFileExcel} onClick={() => downloadExcelSpreadsheet(exportLang)} />
        <ExportButton label="CSV" icon={mdiFileTable} onClick={() => downloadCsv(exportLang)} />
        <ExportButton label="JSON" icon={mdiJson} onClick={() => downloadJson(exportLang)} />
      </React.Fragment>
    </Popup>
  );
};

const LanguageCheckbox = ({ label, ...props }) => (
  <Form.Field>
    <Form.Control>
      <Checkbox {...props}>{label}</Checkbox>
    </Form.Control>
  </Form.Field>
);

const ExportButton = ({ icon, label, ...props }) => (
  <Form.Field>
    <Form.Control>
      <Button fullwidth {...props} className="is-small export-btn">
        <Icon size={0.87} path={icon} />
        <span>{label}</span>
      </Button>
    </Form.Control>
  </Form.Field>
);

export default ExportPopup;
