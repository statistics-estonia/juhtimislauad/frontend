import { combineSeries, makeCartesians } from '../shared/series';

const identity = x => x;

export default class TableMaker {
  constructor(
    widget,
    {
      rowFilter = identity,
      cartesianFilter = identity,
      abscissaFormatter = identity,
      valueFormatter = identity,
      filterValueFormatter = identity
    } = {}
  ) {
    this.widget = widget;
    this.rowFilter = rowFilter;
    this.cartesianFilter = cartesianFilter;
    this.abscissaFormatter = abscissaFormatter;
    this.valueFormatter = valueFormatter;
    this.filterValueFormatter = filterValueFormatter;
  }

  getTable = () => [this.header, ...this.body];

  get header() {
    return [...this.filterNames, ...this.abscissae];
  }

  get filterNames() {
    return this.hasFilters ? this.widgetFilters.map(filter => filter.name) : [''];
  }

  get abscissae() {
    return this.rows.map((row, index) => this.abscissaFormatter(row.date || row.abscissa, index, row));
  }

  get body() {
    return this.cartesians.map(this.mapCartesianToRow);
  }

  get cartesians() {
    return makeCartesians(this.allSeries, this.widgetFilters).filter(this.cartesianFilter);
  }

  mapCartesianToRow = cartesian => {
    const filterValues = this.getFilterValues(cartesian);
    const values = this.rows.map(row => row[cartesian.dataKey]).map(this.valueFormatter);

    return [...filterValues, ...values];
  };

  get rows() {
    return combineSeries(this.allSeries, this.widgetFilters).filter(this.rowFilter);
  }

  getFilterValues = cartesian => {
    const filterValues = this.widgetFilters.map(filter => ({ filter, value: cartesian.filters[filter.name] }));
    const shortnameAsValues = [this.shortname];

    return this.hasFilters
      ? filterValues.map(({ filter, value }, index) => this.filterValueFormatter(value, index, filter))
      : shortnameAsValues.map(this.filterValueFormatter).map(name => name.replace(/&shy;/g, ''));
  };

  get allSeries() {
    return this.widget.diagram.series;
  }

  get widgetFilters() {
    return this.widget.diagram.filters;
  }

  get hasFilters() {
    return this.widgetFilters.length > 0;
  }

  get shortname() {
    return this.widget.shortname;
  }
}
