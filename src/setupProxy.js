const { createProxyMiddleware } = require('http-proxy-middleware');
const pkg = require('../package.json');

module.exports = function (app) {
  app.use(createProxyMiddleware('/api', { target: pkg.proxy, secure: false }));
};
