const PKG_PATH = './package.json';

const fs = require('fs');
const pkg = require(PKG_PATH);

const savePkg = contents => fs.writeFileSync(PKG_PATH, JSON.stringify(contents, null, 2));

const setHomepage = homepage =>
  savePkg({
    ...pkg,
    homepage
  });

setHomepage(process.argv[2]);
